<?php 

$dir = get_template_directory_uri();
// Theme - dependencies

function loadcss(){
	wp_enqueue_style('maincss', get_template_directory_uri(). '/dist/main.css');
	wp_enqueue_style('fontawesome', 'https://use.fontawesome.com/releases/v5.4.1/css/all.css');
}
add_action('wp_head', 'loadcss');

function loadjs(){
	wp_enqueue_script('mainjs', get_template_directory_uri(). '/dist/app.js');

}
add_action('wp_enqueue_scripts', 'loadjs');

if ( function_exists( 'add_theme_support' ) ) { 
	add_theme_support( 'post-thumbnails' );
}

function create_posttypes() {
	$procedureLabels = array(
		'name'               => _x( 'carros', 'post type general name'),
		'singular_name'      => _x( 'carro', 'post type singular name'),
		'menu_name'          => _x( 'Carros', 'admin menu'),
		'name_admin_bar'     => _x( 'Adicionar carro', 'add new on admin bar'),
		'add_new'            => _x( 'Adicionar novo', ''),
		'add_new_item'       => __( 'Adicionar novo carro'),
		'new_item'           => __( 'Novo carro'),
		'edit_item'          => __( 'Editar carro'),
		'view_item'          => __( 'Ver carros'),
		'all_items'          => __( 'Todos os carros'),
		'search_items'       => __( 'Buscar carros'),
		'not_found'          => __( 'Nenhum item encontrado'),
		'not_found_in_trash' => __( 'Nenhum item encontrado na lixeira.')
	);
	register_post_type( 'carros',
		array(
			'labels' => $procedureLabels,
			'public' => true,
			'supports' => array('title', 'editor', 'thumbnail'),
		)
	);
}
add_action( 'init', 'create_posttypes' );




