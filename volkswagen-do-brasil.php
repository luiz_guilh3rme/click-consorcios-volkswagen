<?php require_once './inc/compress-html.php'; ?>
<!DOCTYPE html>
    <html lang="pt-BR">

    <?php
        $title = "Volkswagen do Brasil | Consórcio Nacional Volkswagen";
        $description = '<meta itemprop="description" name="description" content="A Volkswagen tem 62 anos de histórias no Brasil, colecionando inúmeros sucessos. Conheça esta tragetória de segurança e credibilidade. Confira!">';
        $canonicalTag = '<link rel="canonical" href="http://www.consorciovolkswagen.com.br/volkswagen-do-brasil.php">';
        $h1_da_pag = 'Volkswagen do Brasil';
        $texto_after = 'Com foco no presente e olhos no futuro.';
        $img_header = '../../img/header/historia-volkswagen-do-brasil.png';
        $alt = 'Banner História Volkswagen do Brasil';
        $title_alt = 'História Volkswagen do Brasil';
        require_once 'inc/head.php';
    ?>



    <body>

        <?php

            require_once 'inc/header.php';

            require_once 'inc/header-interna.php';

            require_once 'inc/consorcio/content-volkswagen.php';

            require_once 'inc/home/servicos.php';

            require_once 'inc/footer.php';

            require_once 'inc/script.php';

        ?>

    </body>

</html>