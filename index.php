<?php get_header(); ?>
<?php require_once"dados/dados.php"?>
<body>
    <main class="landing">
        <section class="opening-contain-form">
            <div class="center-content">
                <div class="submit-budget-wrapper cleared">
                    <div id="slider">
                        <ul>
                            <li><img src="<?php echo $dir. '/images/slide/carro1.png'; ?>" class="imageSlider"></li>
                            <li><img src="<?php echo $dir. '/images/slide/carro2.png'; ?>" class="imageSlider"></li>
                            <li><img src="<?php echo $dir. '/images/slide/carro3.png'; ?>" class="imageSlider"></li>
                            <li><img src="<?php echo $dir. '/images/slide/carro4.png'; ?>" class="imageSlider"></li>
                            <li><img src="<?php echo $dir. '/images/slide/carro5.png'; ?>" class="imageSlider"></li>
                        </ul>
                    </div>
                    <form id="fomulariotopo" class="submit-budget" data-form="inicial">
                        <input type="hidden" name="identifier" value="Formulário Principal - Primeira Seção">
                        <input type="hidden" name="bulldesk-client" value="">
                        <legend class="form-title">
                            Receba em seu e-mail uma
                            <span class="variant">Simulação</span>
                        </legend>
                        <p class="form-desc">Seu veículo com parcelas acessíveis e até</br> <b>84 meses</b>
                        para pagar </p>
                        <label class="field-wrapper">
                            <input required type="text" class="field" name="nome" placeholder="Nome">
                        </label>
                        <label class="field-wrapper">
                            <input required type="email" class="field" name="email" placeholder="E-mail">
                        </label>

                        <label class="field-wrapper">
                           <input required type="text" minlength=14 class="field whatsapp-field" name="telefone" placeholder="Whatsapp">
                       </label>
                       <p class="range-desc">Valor do Crédito: <b><span class="valueNumber">R$ 100.000,00</span></b></p>
                       <label class="field-wrapper track-wrapper">
                        <input required type="range" name="valor" min="40000" max="200000">
                    </label>
                    <button class="submit-budget-form">QUERO RECEBER AGORA</button>
                </form>
            </div>
        </div>
        <div class="center-content comparativo">

            <h1 style=" display: none;">Consórcio Volkswagen</h1>

        </div>
    </section>
    <section class="money-grubbing">
        <div class="center-content">
            <div class="dib-father">
                <div class="icon-wrapper">
                    <img src="<?php echo $dir. '/images/icon00.png' ?>" alt="" class="icon-instance">
                </div>
                <div class="icon-wrapper">
                    <img src="<?php echo $dir. '/images/icon01.png' ?>" alt="" class="icon-instance">
                </div>
                <div class="icon-wrapper">
                    <img src="<?php echo $dir. '/images/icon02.png' ?>" alt="" class="icon-instance">
                </div>
            </div>
        </div>
    </section>
    <div class="produtos center-content">
        <h3 class="title-several-variants">Confira os <span class="bluey">modelos de carros para consórcio</span></h3>
        <?php 
        $args = array(
         'post_type' => 'carros',
         'order' => 'asc',
         'posts_per_page' => -1
     );
        $carros = get_posts($args);
        $credit = get_field('credit');
        if($carros) : 
            foreach ($carros as $post):
               
                $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail');
                
                ?>

                <div class="quadro">
                    <div class="produtoDados">
                      
                        <img src="<?php
                        if(has_post_thumbnail($post->ID) ):
                        echo $image[0]; 
                        else:
                        $image = $dir.'/images/notfound.png';
                        echo $image;
                        endif;

                        ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
                        <div class="nomeCarro">
                            <p><?php echo the_title();  ?></p>
                        </div>
                        <div class="parcela">
                            <p>Parcelas a partir de:
                                <b class="destaqueQuadro">R$ <?php the_field('installments'); ?></b>
                            </p>
                        </div>
                        <div class="credito">
                            <p>Crédito no valor de:
                                <b class="destaqueQuadro">R$ <?php the_field('credit'); ?></b>
                            </p>
                        </div>
                        <a href="#" class="btnVeiculos">quero uma proposta</a>
                    </div>
                </div>
                <?php 
            endforeach;        
        endif;
        ?>
    </div>
    <div class="proposition" style="display: none;">
        <div class="center-content">
            <form class="proposition-form cleared" data-form="proposta">
                <input type="hidden" name="identifier" value="Proposta">
                <input type="hidden" name="bulldesk-client" value="">
                <legend class="proposition-title">
                    Faça agora uma <span class="variant">PROPOSTA DE ADESÃO</span>
                </legend>
                <label class="field-wrapper one-fourth">
                    <input required type="text" class="field-whitey" placeholder="Nome" name="nome">
                </label>
                <label class="field-wrapper one-fourth">
                    <input required type="text" class="field-whitey whatsapp-field" minlength=14 placeholder="Whatsapp" name="whatsapp">
                </label>
                <label class="field-wrapper one-fourth">
                    <input required type="email" class="field-whitey" placeholder="E-mail" name="email">
                </label>
                <div class="field-wrapper one-fourth">
                    <button class="submit-proposition">
                        <i class="fa fa-paper-plane"></i>
                        ENVIAR
                    </button>
                </div>
            </form>
            <div class="persona" aria-hidden="true"></div>
        </div>
        <div class="uncontained-bg"></div>
    </div>

    <section class="ending">
        <div class="center-content">

            <div class="textoApresentacao">
                <h3 class="title-several-variants"><span class="bluey newLine">experiência</span> da marca VolksWagen </h3>
                <p class="ending-form-desc">Somos os únicos a contar com a garantia da marca Volkswagen e da marca Ducati.</p>
                <p class="ending-form-desc">Mais de 35 anos de tradição e mais de 500 mil veículos entregues! O <strong>Consórcio Nacional Volkswagen</strong> é uma das maiores administradoras de consórcios do país e líder de mercado entre as
                administradoras ligadas a montadoras em número de clientes ativos, segundo o Banco Central do Brasil.</p>
            </div>
            <div class="textoApresentacao">
                <h3 class="title-several-variants"><span class="bluey newLine">como funciona</span> o Consórcio Volkswagen</h3>
                <p class="ending-form-desc"> O <strong>Consórcio de veículos</strong> é uma forma de se adquirir um bem, fazer um investimento ou de proporcionar a você a programação da compra do sonho de consumo, com menor gasto e <strong>parcelado em até 86x</strong>, se comparado à maioria dos financiamentos.</p>
                <p class="ending-form-desc">É similar a um autofinanciamento, em que pessoas com o objetivo de adquirir um
                    bem similar se juntam em um grupo e realizam pagamentos mensais para geração de fundo para compra do
                bem.</p>
            </div>
            <form class="ending-form" data-form="contato">
                <legend class="ending-form-title">
                    Entre em
                    <span class="variant">
                        CONTATO
                    </span>
                </legend>
                <p class="ending-form-desc">
                    Disponibilizamos canais exclusivos para atendimento de nossos clientes. Preencha o formulário, ou
                    se preferir utilize outro canal de contato abaixo.
                </p>
                <div class="cleared">
                    <div class="half-info">
                        <p class="half-title">
                            Tem alguma dúvida?
                            <span class="variant">ENTRE EM CONTATO</span>
                        </p>
                        <img src="<?php echo $dir. '/images/phone.jpg';  ?>" class="phone dib">
                        <p class="phone-number dib">
                            <a href="tel:551121140010" class="tracked phone-body" target="_BLANK" title="Ligue para nós">
                                11 2114.0010
                            </a>
                        </p>
                    </div>
                    <div class="half-info">
                        <p class="half-title">
                            Envie um
                            <span class="variant">WHATSAPP</span>
                        </p>
                        <i class="fab fa-whatsapp huge"></i>
                        <p class="phone-number dib">
                            <a href="wpp" class="tracked cellphone-body" title="Ligue para Nós" target="_BLANK">
                                11 93800.9002
                            </a>
                        </p>
                    </div>
                </div>
                <input type="hidden" name="identifier" value="Formulário Principal - Seção de Contato">
                <input type="hidden" name="bulldesk-client" value="">
                <label class="field-wrapper full">
                    <p class="field-description">Nome:</p>
                    <input required class="field-blue" type="text" name="nome">
                </label>
                <label class="field-wrapper full">
                    <p class="field-description">E-mail:</p>
                    <input required class="field-blue" type="email" name="email">
                </label>
                <label class="field-wrapper full">
                    <p class="field-description">Telefone:</p>
                    <input required class="field-blue whatsapp-field" minlength=14 type="tel" name="telefone">
                </label>

                <label class="field-wrapper full">
                    <p class="field-description">Assunto:</p>
                    <input required class="field-blue" type="text" name="assunto">
                    <!------quais as opções?01-11-2018--------->
                </label>

                <label class="field-wrapper full message">
                    <p class="field-description">
                        Mensagem:
                    </p>
                    <textarea required name="mensagem" class="field-blue bigger textarea"></textarea>
                </label>
                <button class="submit-ending-form">
                    <i class="fa fa-paper-plane"></i>
                    ENVIAR
                </button>
            </form>
        </div>
    </section>
</main>
<?php get_footer(); ?>