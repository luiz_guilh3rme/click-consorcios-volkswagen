<?php include 'inc/compress-html.php'; ?>
<!DOCTYPE html>
    <html lang="pt-BR">

    <?php
        $title = "Erro 404 - Página não encontrada";
        $description = '<meta itemprop="description" name="description" content="Erro 404 - Página não encontrada"><meta name="robots" content="noindex, nofollow">';
        $canonicalTag = '<link rel="canonical" href="http://www.consorciovolkswagen.com.br/404.php">';
        $h1_da_pag = 'Erro 404 - Página não encontrada';
        $texto_after = 'Página não encontrada por favor retorne a pagina ou <a style="color:white;" href="'.home_url().'">clique aqui.</a>';
        $img_header = $dir. '/img/header/frente-amarok-consorcio-volkswagen.png';
        $alt = 'Banner Amarok - Erro 404, Página não encontrada';
        $title_alt = 'Erro 404 - Página não encontrada';
        require_once 'inc/head.php';
    ?>

    <body>
        <?php
            require_once 'inc/header.php';
            require_once 'inc/header-erros.php';
            require_once 'inc/home/servicos.php';
            require_once 'inc/footer.php';
            require_once 'inc/script.php';
        ?>

    </body>

</html>

