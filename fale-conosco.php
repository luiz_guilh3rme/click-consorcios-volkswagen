<?php require_once './inc/compress-html.php'; ?>
<!DOCTYPE html>
    <html lang="pt-BR">

    <?php
        $title = "Fale Conosco | Consórcio Nacional Volkswagen";
        $description = '<meta itemprop="description" name="description" content="Entre em contato com o Consórcio Volkswagen e esclareça suas dúvidas. Nosso atendimento é personalizado para ouvir você. Confira!">';
        $canonicalTag = '<link rel="canonical" href="'. home_url().'/fale-conosco.php">';
        $h1_da_pag = 'Fale Conosco';
        $texto_after = 'Utilize este canal para entrar em contato com uma equipe especializada e pronta para atender suas solicitações, preencha os campos com suas dúvidas ou sugestões e encaminhe para nossa central de atendimento.';
        $img_header = $dir. '/img/header/frente-fox-consorcio-volkswagen.png';
        $alt = 'Banner Fox - Fale Conosco';
        $title_alt = 'Fale Conosco - Atendimento Personalizado';
        require_once 'inc/head.php';
    ?>

    <body>

        <?php

            require_once 'inc/header.php';

            require_once 'inc/header-interna.php';
            
            require_once 'inc/consorcio/content-fale.php';
            
            require_once 'inc/bulldesk.php';

            require_once 'inc/home/servicos.php';

            require_once 'inc/footer.php';

            require_once 'inc/script.php';


        ?>

    </body>

</html>