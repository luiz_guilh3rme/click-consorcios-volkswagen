
function init() {
	// tablePickers();   
	$('.table-picker').on('click', tablePickers); 
    $('.form-pickers').on('click', formPickers);
    $('.tracked').on('click', trackAnalyticsEvents);
	refreshRange();
	closeCta();
    openModal(); 
    closeModal();
    handleForms();
    printNumbers();
    initializeBulldeskAnalytics();
    maskInstancing();
    loadSlider();

    $('.thanks a').on('click', function(){
        window.location = window.location.origin;
    });

    window.onscroll = onMobileScroll;

    if (helpers.isMobile) {

        
    } else {
        // initializeZendesk();
    }
}



// window load binds 
window.onload = init;

function DOMLoaded() {
    // these are not always necessary but sometimes they fuck with ya
    if (helpers.iOS) {
        document.querySelector('html').classList.add('ios');
    } else if (helpers.IE()) {
        document.querySelector('html').classList.add('ie');
    } 
}

// domcontent binds 
document.addEventListener('DOMContentLoaded', DOMLoaded);