// unique scripts go here.

function tablePickers() {
	$('.table-picker').removeClass('active');
	$(this).addClass('active');
	var instance = $(this).data('table');

	$('.table-basic:not([data-table=' + instance + '])').fadeOut(400, function () {
		setTimeout(function () {
			$('.table-basic[data-table=' + instance + ']').fadeIn()
		}, 400)
	});
}

function formPickers() {
	$('.form-pickers').removeClass('active');
	$(this).addClass('active');
	var instance = $(this).data('instance');

	$('.instance:not([data-instance=' + instance + '])').fadeOut(400, function () {
		setTimeout(function () {
			$('.instance[data-instance=' + instance + ']').fadeIn()
		}, 400)
	});
}

function refreshRange() {
	$('input[type="range"]').on('input', function () {
		var value = $(this).val();

		var millions,
		thousands,
		hundreds,
		decimalValue;

		// pre 100k 
		if (value < 100000) {
			thousands = value.substr(0, 2);
			hundreds = value.substr(3, 4);
			// console.log(thousands+ '.' +hundreds);
			decimalValue = thousands + '.' + hundreds;
		}

		// pre 1 million
		else if (value < 1000000) {
			thousands = value.substr(0, 2);
			hundreds = value.substr(3, 5);
			decimalValue = thousands + '0.' + hundreds;

		}
		// else {
		//     millions = value.substr(0, 1);
		//     thousands = value.substr(1, 3);
		//     hundreds = value.substr(4, 5);
		//     decimalValue = millions + '.' + thousands + '.' + hundreds;
		// }

		$('.valueNumber').html('R$ ' + decimalValue + ',00');
	});
}

function closeCta() {
	$('.close-whatsapp').on('click', function () {
		$('.whatsapp-wrapper').addClass('clicked');
	});
}

function openModal() {
	// duhh
	$('.confirm').on('click', function () {
		$('.overlay').fadeIn(400, function () {
			$('.form-wrapper-all').fadeIn();
		});


		// create new date on button click
		var now = new Date(),
		days = ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		day = days[now.getDay()];
		hour = now.getHours(),
		picker = $('.form-pickers[data-instance=00]');


		// check if it is outside business hours
		if (day === 'Domingo' || day === 'Sábado' || hour > 18 || hour <= 9) {
			picker[0].disabled = true;
			picker[0].title = "Disponível apenas em horário comercial";
		}
	});
}

function closeModal() {
	$('.close-modal').on('click', function () {
		$('.form-wrapper-all').fadeOut(400, function () {
			$('.overlay').fadeOut();
		});
	});
}


function handleForms() {

	   $actualurl = window.location.pathname;

	$('body').on('submit', 'form', function (e) {
		e.preventDefault();

		var fields = $(this).serializeObject(),
		instance = $(this).data('form'),
		redirect;

		fields.token = '1299dd4bf91a4d1ce3d6006677993ae1';
		fields.client = $('input[name="bulldesk-client"]').val();

		//alert(fields.valor);
		// button[0].disabled = true; 
		// sort form instance

		switch (instance) {

			case 'inicial':
			redirect = $actualurl + 'obrigado';
			// redirect = $actualurl + 'page-obrigado-inicial';
			// console.log('obrigado-inicial');
			break;

			case 'contato':
			redirect = $actualurl + 'obrigado';
			// redirect = $actualurl + 'page-obrigado-contato';
			// console.log('obrigado-contato');	
			break;

			case 'proposta':
			redirect = $actualurl + 'obrigado';
			// redirect = $actualurl + 'page-obrigado-proposta';
			// console.log('obrigado-proposta');
			break;

			case 'schedule':
			redirect = $actualurl + 'obrigado';
			// redirect = $actualurl + 'page-obrigado-agendar';
			// console.log('obrigado-agendar');
			break;

			case 'leave-message':
			redirect = $actualurl + 'obrigado';
			// redirect = $actualurl + 'page-obrigado-deixar-mensagem';
			// console.log('obrigado-deixar-mensagem');
			break;

			case 'we-call':
			redirect = $actualurl + 'obrigado';
			// redirect = $actualurl + 'page-obrigado-nos-te-ligamos';
			// console.log('obrigado-nos-te-ligamos');
			break;

			default:
			return false;

		}

		if (helpers.isMobile) {
			if (instance === 'inicial' || instance === 'proposta') {
				// console.log(fields);

				// set up variable for queryString

				var queryString = '';

				delete fields.identifier;
				delete fields['bulldesk-client'];
				delete fields.token;
				delete fields.client;

				for (var field in fields) {
					queryString += field + ' : ' + fields[field] + ' | ';
				}

				console.log(queryString);


				window.location = 'https://wa.me/5511938009002?text=' + encodeURI(queryString.slice(0, -1)) + '';
			}
		}

		// ajax to bulldesk
		$.ajax({
			url: 'https://api.bulldesk.com.br/conversion',
			type: 'POST',
			data: fields,
		})
		.done(function (r) {
			
			window.location = window.location.origin + redirect;

		})

		.fail(function () {
			console.log("Error");
			alert('Tente novamente mais tarde ou entre em contato através de nossos telefones.');
		})
		.always(function () {
				console.log(fields);
				button[0].disabled = false;
		});
	});
}

function initializeBulldeskAnalytics() {
	! function (a, b, c) {
		if ('object' != typeof c) {
			var d = function () {
				d.c(arguments)
			};
			d.q = [], d.c = function (a) {
				d.q.push(a)
			}, a.Bulldesk = d;
			var e = b.createElement('script');
			e.type = 'text/javascript', e.async = !0, e.id = 'bulldesk-analytics', e.src =
			'https://static.bulldesk.com.br/analytics.js';
			var f = b.getElementsByTagName('script')[0];
			f.parentNode.insertBefore(e, f)
		}

	}(window, document, window.Bulldesk);

	window.BulldeskSettings = {
		token: '1299dd4bf91a4d1ce3d6006677993ae1'
	};
}

function trackAnalyticsEvents() {
	var category = $(this).data('category'),
	action = $(this).data('action'),
	label = $(this).data('label'),
	value = $(this).data('value');

	ga('gtm1.send', 'event', category, action, label, value);
}

function randomizeRequests(min, max) {
	var random = Math.floor(Math.random() * (max - min + 1)) + min;
	return random;
}

function printNumbers() {
	var numbers = document.querySelectorAll('.number');
	for (var i = 0; i < numbers.length; i++) {
		numbers[i].innerHTML = randomizeRequests(1, 12);
	}
}

function maskInstancing() {
	var SPMaskBehavior = function (val) {
		return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
	},
	spOptions = {
		onKeyPress: function (val, e, field, options) {
			field.mask(SPMaskBehavior.apply({}, arguments), options);
		}
	};

	$('.whatsapp-field').mask(SPMaskBehavior, spOptions);
	$('.dia').mask('00/00/0000');
	$('.hora').mask('00:00');
}


function startSmartLook() {
	window.smartlook || (function (d) {
		var o = smartlook = function () {
			o.api.push(arguments)
		},
		h = d.getElementsByTagName('head')[0];
		var c = d.createElement('script');
		o.api = new Array();
		c.async = true;
		c.type = 'text/javascript';
		c.charset = 'utf-8';
		c.src = 'https://rec.smartlook.com/recorder.js';
		h.appendChild(c);
	})(document);
	smartlook('init', '98c621eba4840a3ff09667697df2a24a7d8c7d7a');
}

function onMobileScroll() {
	if ( helpers.isMobile && window.pageYOffset > 50 ) {
		$('#Topo2').fadeOut(200); 

	} else if (helpers.isMobile && window.pageYOffset < 50) {
		$('#Topo2').fadeIn(); 		
	}
}

function loadSlider(){
    $('#slider ul').slick({
      slidesToShow: 1,
      fade:true,
      dots:false,
      prevArrow:false,
      nextArrow:false,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 5000
    });
}