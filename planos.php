<?php require_once './inc/compress-html.php'; ?>
<!DOCTYPE html>
    <html lang="pt-BR">

    <?php
        $title = "Planos e Preços Especiais | Consórcio Nacional Volkswagen";
        $description = '<meta itemprop="description" name="description" content="Conheça os planos e preços especiais que o Consórcio Nacional Volkswagen oferece. Encontre um plano com o valor que cabe em seu bolso, Confira!">';
        $canonicalTag = '<link rel="canonical" href="http://www.consorciovolkswagen.com.br/planos.php">';
        $h1_da_pag = 'Planos Consórcio Volkswagen';
        $texto_after = 'Conheça os dois Planos de Consórcio Volkswagen: <strong>Especial</strong> e <strong>Gold</strong>. Analise qual atende melhor às suas expectativas e necessidades.';
        $img_header = $dir. '/img/header/frente-fox-blue-motion-consorcio-volkswagen.png';
        $alt = 'Banner Frente do Fox Blue Motion - Planos Volkswagen';
        $title_alt = 'Planos e Preços';
        require_once 'inc/head.php';
    ?>

    <body>
        <?php
            require_once 'inc/header.php';
            require_once 'inc/header-interna.php';
            require_once 'inc/consorcio/content-planos.php';
            require_once 'inc/home/servicos.php';
            require_once 'inc/footer.php';
            ?>
        <script src="../js/scripts.min.js"></script>
        <?php require_once 'inc/bulldesk.php'; ?>
    </body>
</html>