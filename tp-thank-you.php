<?php
// Template Name: Thank You 

	do_action('wp_head','loadcss');
	do_action('wp_enqueue_scripts', 'loadjs');

		echo '<style>#MeuModalBG{display: none;}.pulse-button a {position: relative;top: 8px;}</style>';

get_header(); ?>
<main class="landing">
	<?php if (have_posts()) : while(have_posts()) : the_post();  ?>
		<section class="opening-contain-obrigado">
	
	<div class="tks-text">

			<p class="thanks">
				<br>
				<?php the_content();  ?>
				Clique <a href="<?php echo site_url('/'); ?>">AQUI</a>
				para retornar à página principal.
			</p>
	</div>
		</section>
	<?php endwhile; endif; ?>
</main>
<?php get_footer(); ?>