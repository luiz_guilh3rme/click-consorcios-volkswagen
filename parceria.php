<?php require_once './inc/compress-html.php'; ?>
<!DOCTYPE html>
    <html lang="pt-BR">

    <?php
        $title = "Torne-se um Colaborador | Consórcio Nacional Volkswagen";
        $description = '<meta itemprop="description" name="description" content="Torne-se um colaborador do Consórcio Volkswagen e encontre o caminho do sucesso através de nossos planos de Aliança e Parceria. Venha conhecer!">';
        $canonicalTag = '<link rel="canonical" href="http://www.consorciovolkswagen.com.br/parceria.php">';
        $h1_da_pag = 'Torne-se um Colaborador';
        $texto_after = 'Encontre o caminho do sucesso profissional através das oportunidades reais de negócio.';
        $img_header = $dir. '/img/header/parceria-consorcio-volkswagen.png';
        $alt = 'Banner Pessoas em Reunião - Torne-se um Colaborador';
        $title_alt = 'Torne-se um Colaborador';
        require_once 'inc/head.php';
    ?>

    <body>
        <?php
            require_once 'inc/header.php';
            require_once 'inc/header-interna.php';
            require_once 'inc/consorcio/content-partner.php';
            require_once 'inc/home/servicos.php';
            require_once 'inc/footer.php';
            require_once 'inc/script.php';
            require_once 'inc/bulldesk.php';
        ?>
    </body>

</html>