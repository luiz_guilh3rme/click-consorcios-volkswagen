<?php
if (strpos($_SERVER["REQUEST_URI"], 'obrigado') == false) :
    ?>
    <?php wp_footer(); ?>
<footer class="cloudy">
    <div class="cloudy-bg" aria-hidden="true"></div>
    <?php
    else :
        ?>
    <footer class="cloudy hidden">
        <?php
        endif;
        ?>
        <div class="center-content cleared rodapeDados">
           
            <p class="ending-description just" style="margin-left:	0px; width: 100%;">
                Se você está cansado de andar a pé, pegar um transporte público lotado e ineficiente e quer mais
                conforto e praticidade, conheça o Consórcio Nacional Volkswagen. Adquira seu automóvel com as melhores
                condições do mercado.
                <br>
                <br>
                <span class="variant">
                    VANTAGENS DO CONSÓRCIO NACIONAL VOLKSWAGEN:
                </span>
                <br>
                <br>
                O <strong>Consorcio Nacional Volkswagen</strong> te ajuda a realizar o sonho do seu carro próprio, sem
                juros e com parcelas mensais que cabem no seu bolso. Você participa todos os meses dos sorteios, como
                também pode dar seu lance, ampliando as chances de ser contemplado. É o caminho mais fácil e sem
                burocracia para garantir o carro novo com consorcócio volks. Torne realidade o sonho do carro zero com os planos do <strong>Consorcio
                    Nacional Volkswagen</strong> .

                <div class="row deslocamento" style="position: relative;
    float: left;">

                    <div class=" ending-description four">
                        <h4>Horário de funcionamento</h4>
                        Segunda a Sexta-feira das 08:30 às 18:00
                        Sábado(Plantão) das 09:00 as 16:00
                    </div>

                    <div class=" ending-description four">
                        <h4>Central de Vendas</h4>
                        +55(11) 93800-9002
                        +55(11) 2114-0010
                        contato@clickconsorciovolkswagen.com.br
                    </div>
                    <div class=" ending-description four">
                        <h4>Central de Relacionamento</h4>
                        0800 770 1936
                        SAC:0800 770 1926
                        Deficiente auditivo/fala: 0800 770 1935
                        Ouvidoria: 0800 701 2834
                    </div>
                    <div class="ending-description four">
                        <h4>Click consórcios</h4>
                        Rua Borges de Figueiredo, 303 - Ed. Atrio
                        Giomo - CJ 216 Moóca - São Paulo
                    </div>
                </div>

                <div class="row">
                    <small>
                        * Todas as imagens/fotos de veículos exibidas neste site são de caráter meramente ilustrativas.
                        ** Nos reservamos o direito de correções nas informações dispostas em todo conteúdo deste
                        portal. *** Preços e disponibilidade sujeitos à alterações sem prévio aviso.
                    </small>
                </div>
        </div>
        <div class="copyright">
            <div class="center-content">
               <p class="terms">
                    <b>Consórcio Nacional Volkswagem</b> Rua Volkswagen, 291  Parque Jabaquara, São Paulo - SP, 04344-020 - CNPJ: 47.658.539/0001-04 - <b>Tel: 0800 770 1936</b> 
                </p>
                <div class="signature" style="display: none">
                    <a href="https://www.3xceler.com.br/criacao-de-sites" target="_BLANK" title="Agência 3xceler" class="signature-link transitioned-basic">
                        Criação de Sites
                    </a>
                    <span class="sr-only">
                        3xceler
                    </span>
                    <img src="<?php echo get_template_directory_uri().'/images/3xceler.png';  ?>" alt="Logotipo Agência 3xceler" class="3xceler">
                </div>
           

            </div>
    </div>
    </footer>
 
    <?php require 'includes/modal/modal.php'; ?>


    <?php
    include 'includes/components/overlay.php';
    include 'includes/components/call-cta.php';
    include 'includes/components/whatsapp-cta.php';
    ?>
   
    </body>
    </html>