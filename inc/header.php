<?php
$arrParams = explode("/",$_SERVER["REQUEST_URI"]);
$page = $arrParams[2];
?>
    <div class="faixa-azul">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
            aria-expanded="false">
            <span class="sr-only">Menu Principal</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <div class="telefones">
            <a href="tel:+55112114-0010" class="tel" title="Entre em contato através do telefone: (11) 2114-0010">
                <i class="fa fa-phone"></i> (11) 2114-0010</a>
            <a href="tel:+551193800-09002" class="tel" title="Entre em contato através do Whatssapp: (11) 93800-9002">
                <i class="fa fa-whatsapp"></i> (11) 93800-9002</a>
        </div>
    </div>
    <div class="container">

        <div class="logo-site">
            <a href="<?php home_url(); ?>" title="Consórcio Nacional Volkswagen - Conheça Nossos Planos">

                <img border="0" src="<?php echo $dir. '/img/logo-consorcio-nacional-volksvagen.png'; ?>" alt="Logo Consórcio Nacional Volkswagen - Representante Autorizado"
                    width="353" height="60" class="img-responsive">

            </a>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 pull-right">

            <div class="topo-new">

                <div class="tel-new col-lg-offset-2 col-lg-5">

                    <p class="p-1-topo">Entre em Contato:</p>

                    <p class="p-2-topo">

                        <img class="img-topo-new" src="<?php echo $dir. '/img/icon-telefone-topo.png'; ?>" alt="Ícone Telefone de Contato" width="14" height="14">
                        <a href="tel:+55112114-0010" title="Entre em contato através do telefone: (11) 2114-0010">(11) 2114-0010</a>
                    </p>

                    <p class="p-2-topo">

                        <img class="img-topo-new" src="<?php echo $dir. '/img/icon-whats-topo.png'; ?>" alt="Ícone Whatssapp" width="16" height="17">
                        <a href="tel:+5511938009002" title="Entre em contato através do Whatsapp: (11) 93800-9002">(11) 93800-9002</a>
                    </p>
                </div>

                <div class="meliga-new col-lg-5">

                    <p class="p-1-topo">Ou se preferir, nós te ligamos:</p>


                    <div id="bt-liga-big" class="modalbt" rel="modal" onclick="javascript:location='#janela1';">
                        <i class="fa fa-angle-right"></i>Volks, me liga!</div>

                </div>

            </div>

        </div>

    </div>


    <div class="window" id="janela1">

        <a href="#" class="fechar">
            <img src="<?php echo $dir. '/img/btn-fechar.png'; ?>" alt="Ícone Fechar" />
        </a>

        <span class="title">Nós te ligamos</span>

        <form method="post" id="frmExpresso" action="#" data-modal="nos-te-ligamos">

            <!-- NewsP -->
            <input type="hidden" name="url" value="<?= " http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI] "; ?>">
            <!-- //NewsP -->
            <input type="hidden" name="identifier" value="Nós te Ligamos">

            <label>Nome: </label>
            <input type="text" name="nome" id="nome" placeholder="Digite seu nome" required />

            <label>Telefone: </label>
            <input type="text" name="telefone" class="celular-input" id="telefone" placeholder="Digite seu telefone" required />

            <label>Email: </label>
            <input type="email" name="email" id="email" placeholder="Digite seu email" required />

            <label>Modelo: </label>
            <select name="modelo" id="modelo" required>
                <option disabled selected> Selecione um Modelo </option>
                <option value="------CARROS------" disabled> ------CARROS------ </option>
                <option value="Polo 1.0"> Polo 1.0 </option>
                <option value="Polo MSI 1.6"> Polo MSI 1.6 </option>
                <option value="Polo Highline 200 TSI"> Polo Highline 200 TSI </option>
                <option value="Amarok Cabine Simples"> Amarok Cabine Simples </option>
                <option value="Amarok Cabine Dupla"> Amarok Cabine Dupla </option>
                <option value="Golf Variant Comfortline"> Golf Variant Comfortline </option>
                <option value="Golf Highline 1.4"> Golf Highline 1.4 </option>
                <option value="Golf TSI 1.0"> Golf TSI 1.0 </option>
                <option value="Golf Highline 1.4 Aut"> Golf Highline 1.4 Aut </option>
                <option value="Golf GTI 2.0"> Golf GTI 2.0 </option>
                <option value="Jetta Highline 2.0"> Jetta Highline 2.0 </option>
                <option value="Tiguan 1.4"> Tiguan 1.4 </option>
                <option value="Take UP! - 4 Portas"> Take UP! - 4 Portas </option>
                <option value="Voyage 1.0"> Voyage 1.0 </option>
                <option value="Voyage 1.6 I-Motion"> Voyage 1.6 I-Motion </option>
                <option value="Voyage 1.6"> Voyage 1.6 </option>
                <option value="Gol 1.6 "> Gol 1.6 </option>
                <option value="Gol 1.0 "> Gol 1.0 </option>
                <option value="Virtus 1.6 MSI"> Virtus 1.6 MSI </option>
                <option value="Virtus Highline"> Virtus Highline </option>
                <option value="Delivery Express"> Delivery Express </option>
                <option value="------MOTOS------" disabled> ------MOTOS------ </option>
                <option value="Scrambler"> Scrambler </option>
            </select>

            <label>Opção: </label>
            <select name="opcao">
                <option disabled="disabled" value="" selected>Selecione uma opção</option>
                <option value="Já sou cliente">Já sou cliente</option>
                <option value="Adesão de Consórcio">Adesão de Consórcio</option>
                <option value="Dúvidas sobre o Consorcio">Dúvidas sobre o Consorcio</option>
                <option value="Outros">Outros</option>
            </select>

            <input type="hidden" name="tipo2" id="tipo2" value="expresso" />

            <button type="submit" id="enviar" name="enviar">Enviar</button>

        </form>
    </div>

    <div id="mascara"></div>
    <div class="background-gray-white"></div>
    <div class="backgroung-nav-top">

        <div class="container">

            <nav class="navbar navbar-default">

                <div class="container-fluid">

                    <div class="navbar-header">

                    </div>

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                        <ul class="nav navbar-nav">

                            <li>
                                <a href="<?php echo home_url(); ?>">
                                    <span>Home</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo home_url(); ?>/sobre">
                                    <span>Consórcio Volkswagen</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo home_url(); ?>/planos">
                                    <span>Planos e Preços</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo home_url(); ?>/parceria">
                                    <span>Trabalhe Conosco</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo home_url(); ?>/fale-conosco">
                                    <span>Entre em Contato</span>
                                </a>
                            </li>

                        </ul>

                    </div>

                </div>

            </nav>

        </div>

    </div>