<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://static.bulldesk.com.br/forms.js"></script>
<script>
    ! function (a, b, c) {
        if ('object' != typeof c) {
            var d = function () {
                d.c(arguments)
            };
            d.q = [], d.c = function (a) {
                d.q.push(a)
            }, a.Bulldesk = d;
            var e = b.createElement('script');
            e.type = 'text/javascript', e.async = !0, e.id = 'bulldesk-analytics', e.src =
                'https://static.bulldesk.com.br/analytics.js';
            var f = b.getElementsByTagName('script')[0];
            f.parentNode.insertBefore(e, f)
        }

    }(window, document, window.Bulldesk);

    window.BulldeskSettings = {
        token: '1299dd4bf91a4d1ce3d6006677993ae1'
    };
</script>
<script>
    $("input[name='phone'], input[name='cellphone']")
        .mask("(99) 9999-9999?9")
        .focusout(function (event) {
            var target, phone, element;
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;
            phone = target.value.replace(/\D/g, '');
            element = $(target);
            element.unmask();
            if (phone.length > 10) {
                element.mask("(99) 99999-999?9");
            } else {
                element.mask("(99) 9999-9999?9");
            }
        });
    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    $(document).ready(function () {

        $('body').on('submit', 'form', function (event) {
            event.preventDefault();

            // set generic variables

            var fields = $(this).serializeObject(),
                instance = $(this).data('modal'),
                button = $(this).children('button[type="submit"]'),
                mailInstance,
                redirect;


            // // push token value to field object
            fields.token = '1299dd4bf91a4d1ce3d6006677993ae1';
            fields.client = $('input[name="bulldesk-client"]').val();
            console.log(fields.client);

            // sort form instance
            switch (instance) {
                case 'simular':
                    redirect = '/obrigado/';
                    break;

                case 'modal-whatsapp':
             
                    redirect = '/whatsapp/obrigado/';
                    break;

                case 'cadastro':
              
                    redirect = '/cadastro/obrigado/';
                    break;

                case 'nos-te-ligamos':
                   
                    redirect = '/obrigado/';
                    break;

                case 'fale-conosco':
                  
                    redirect = '/obrigado/';
                    break;

                case 'parceiro':
                  
                    redirect = '/obrigado/';
                    break;

                default:
                    return false;
            }
            // post info to bulldesk
            $.post('https://api.bulldesk.com.br/conversion', fields);

            // ajax to email submit
            $.ajax({
                    url: 'https://api.bulldesk.com.br/conversion',
                    type: 'POST',
                    data: fields,
                })
                .done(function (r) {
                    swal({
                        title: 'Obrigado pelo Contato!',
                        text: 'Nossos Analistas irão responder o mais cedo possível',
                        icon: 'success',
                    });
                    setTimeout(function () {
                        window.location = window.location.origin + redirect;
                    }, 2000);
                })
                .fail(function () {
                    swal({
                        title: 'Algo deu errado...',
                        text: 'Tente novamente mais tarde ou entre em contato através de nossos telefones.',
                        icon: 'error',
                    })
                })
                .always(function () {
                    // console.log(fields);
                });
        });
    });
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-123251837-2"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-123251837-2');
</script>
<!-- Facebook Pixel Code -->
<script>
    ! function (f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function () {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
        'http://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '487714338105541');
    fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none" src="http://www.facebook.com/tr?id=487714338105541&ev=PageView&noscript=1" /></noscript>
<!-- End Facebook Pixel Code -->