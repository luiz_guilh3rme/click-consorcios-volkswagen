<div class="container">

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 content-consorsio">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <p>A Volkswagen tem 62 anos de Brasil. Essa história de sucesso teve início em um pequeno galpão alugado na rua do Manifesto, no bairro do Ipiranga, em São Paulo, com apenas 12 funcionários. Apenas seis anos depois, em 18 de novembro de 1959, a empresa já inaugurava a unidade Anchieta, com a participação do então presidente Juscelino Kubistchek.</p>

            <p>Em 1976, a Volkswagen iniciou a operação da fábrica de Taubaté, erguida com o propósito de produzir o Gol. Vinte anos depois, em 1996, a empresa inaugurou a fábrica de São Carlos, uma das três maiores produtoras de motores do Grupo Volkswagen no mundo. E, em 1999, iniciou a operação da moderna unidade de São José dos Pinhais.</p>

            <p>Em outras palavras: mais tranqüilidade e segurança para quem compra um veículo e para quem o vende.</p>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

            <h2 class="pag-interna-consorcio"><i class="blue-consorcio fa fa-angle-right"></i> Visão</h2>

            <p>Ser líder em qualidade, inovação, vendas e lucratividade da indústria automotiva na América do Sul, com um time de alta performance e focado no desenvolvimento sustentável.</p>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

            <h2 class="pag-interna-consorcio"><i class="blue-consorcio fa fa-angle-right"></i> Nossa Missão</h2>

            <p>A Volkswagen do Brasil é uma fabricante de veículos de alto volume orientada para a qualidade, satisfação do cliente, inovação e responsabilidade socioambiental.<br>Concentramos nossos esforços em agregar valor aos acionistas, colaboradores, clientes, concessionários, fornecedores e à sociedade.</p>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-bottom-none padding-none">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <h2 class="pag-interna-consorcio"><i class="blue-consorcio fa fa-angle-right"></i> História</h2>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 margin-bottom">

                <p>Focada em vencer os desafios do presente e com os olhos voltados para o futuro, a Volkswagen comemorou, em 2015, 62 anos de Brasil.</p>

                <p>Nossa história começou de forma modesta, num galpão alugado no bairro do Ipiranga, em São Paulo, no dia 23 de março de 1953. A empresa foi logo reconhecida pelo mercado por fazer carros duráveis e confiáveis, com preço competitivo, facilidade de manutenção e alto valor de revenda.</p>

                <p>Ao mesmo tempo, a Volkswagen posicionou-se, desde o princípio, como a montadora mais inovadora do Brasil, lançando novas tecnologias e estabelecendo novos padrões de consumo. Um dos exemplos mais emblemáticos deste pioneirismo ocorreu quando a marca lançou a tecnologia Total Flex, que deu ao consumidor a liberdade de escolher o combustível de sua preferência.</p>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 margin-bottom">

                <img width="100%" src="../../img/fotos-internas/volkswagen-do-brasil-anos-50.jpg" alt="Fachada da Volkswagen do Brasil anos 50 com um fusca e uma kombi" />


            </div>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-bottom-none padding-none">

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 margin-bottom">

                <img width="100%" src="../../img/fotos-internas/fuscas-na-porta-da-volkswagen-do-brasil-anos-50.jpg" alt="Diversos fuscas na porta da Volkswagen do Brasil anos 50" />

            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 margin-bottom">

                <h3 class="margin-top-none">Motor da indústria</h3>

                <p>Dentre os fatos marcantes da Volkswagen nos anos 50, destaca-se a fabricação da primeira Kombi com 50% peças nacionais, em 1956. O fato ilustra o papel decisivo da empresa no desenvolvimento da cadeia de fornecedores e da economia do Brasil. Outro marco histórico foi a inauguração da unidade Anchieta, em 18 de novembro 1959, com a participação do ex-presidente da República Juscelino Kubistcheck, que desfilou pela fábrica num Fusca conversível.</p>

                <p>Na década de 60, a história da Volkswagen foi marcada por um crescimento acelerado e por lançamentos como o Karmann-Ghia (1962), a Variant (1969) e o TL (1970). Nos anos 70, nossa Engenharia do Produto mostrou a capacidade de inovação dos profissionais brasileiros e criou os primeiros Volkswagen genuinamente nacionais: a Brasília (1973), o SP1 e o SP2 (1975).</p>

                <p>Ainda na década de 70, a Volkswagen do Brasil lançou o seu primeiro modelo com motor refrigerado a água e tração dianteira, o Passat (1974), uma revolução para a época. Em 1976, implantou a fábrica de Taubaté, com o propósito de fazer outro "brasileiro famoso", o Gol. Lançado em 1980, o modelo logo tornou-se o maior sucesso da indústria automotiva nacional. Ele é líder há 27 anos consecutivos e já soma mais de 7 milhões de unidades produzidas.</p>

            </div>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-bottom-none padding-none">

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 margin-bottom">

                <h3 class="margin-top-none">Vento da inovação</h3>

                <p>Nos anos 80, chegaram os derivados da plataforma Gol: o sedan Voyage, a perua Parati e a picape Saveiro, igualmente campeões de vendas em seus segmentos. Ainda naquela década, a Volkswagen do Brasil entrou no segmento de luxo, com o Santana (1984) e a Quantum (1985). Em 1988, a marca produziu o primeiro carro nacional com injeção eletrônica de combustível e ignição digital com mapeamento eletrônico, o Gol GTI.</p>

                <p>Em 1996, inaugurou a fábrica de motores de São Carlos e, em 1999, a unidade industrial de São José dos Pinhais, no Paraná. Chegando inicialmente como carro importado (1994), o Golf logo se tornou um grande sucesso, introduzindo novos níveis de qualidade e dirigibilidade no mercado.</p>

                <p>Para receber o Polo (2002) e o Polo Sedan (2003), a fábrica Anchieta teve seus meios e processos produtivos completamente modernizados. Em 2003, a Engenharia criou mais um carro revolucionário, o Fox, que tem um melhor aproveitamento do espaço interno como dizia a propaganda "compacto para quem vê, gigante para quem anda". </p>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 margin-bottom">

                <img width="100%" src="../../img/fotos-internas/gol-quadrado-gts-1-8.jpg" alt="Gol Quadrado GTS 1.8" />

            </div>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-bottom-none padding-none">

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 margin-bottom">

                <img width="100%" src="../../img/fotos-internas/fabrica-volkswagen-taubate-produzindo-gols.jpg" alt="Fábrica da Volkswagen em Taubaté produzindo gols" />

            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 margin-bottom">

                <h3 class="margin-top-none">Sustentabilidade</h3>

                <p>Desde 2003, a linha de produtos da Volkswagen do Brasil foi completamente renovada. Foram lançados o Novo Polo, o Novo Polo Sedan e o Novo Golf. A família Gol, Parati e Saveiro entrou na 4ª geração. O próprio Fox ganhou derivados: o CrossFox e o SpaceFox. A Kombi foi equipada com o motor 1.4 litro Total Flex refrigerado a água. Lançados em 2008, o Novo Gol (eleito "Carro do Ano de 2009") e o Voyage inauguraram novos padrões de qualidade, economia, desempenho e dirigibilidade no segmento dos carros de entrada. </p>

                <p>Ao mesmo tempo que lançou produtos, modernizou fábricas e desenvolveu novas tecnologias, na primeira década do século 21, a Volkswagen do Brasil deu outros passos importantes rumo à sustentabilidade. A empresa implantou um eficiente Sistema de Gestão Ambiental e conquistou a ISO 14001 em todas as suas fábricas. A Fundação Volkswagen intensificou e ampliou seu leque de atuação social, trabalhando por uma educação pública de qualidade e pelo bem-estar da comunidade.</p>

            </div>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-bottom-none padding-none">

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 margin-bottom">

                <h3 class="margin-top-none">Mudanças</h3>

                <p>Em dezembro de 2010, a Anchieta inaugurou nova linha de pintura, ampliando sua capacidade produtiva. Em fevereiro, São Carlos aumentou a produção motores e a Taubaté iniciou a construção de uma nova área de Pintura.</p>

                <p>Em 2011, a Volkswagen iniciou uma nova etapa no Brasil, com uma maior conexão tecnológica entre os produtos e processos desenvolvidos no País e o que existe de mais moderno e inovador no Grupo Volkswagen. Até 2014, a empresa investirá R$ 8,7 bilhões em novos produtos e na ampliação da capacidade das fábricas brasileiras.</p>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 margin-bottom">

                <img width="100%" src="../../img/fotos-internas/fabricas-brasileiras-grupo-volkswagen.jpg" alt="Fábricas Brasileiras Grupo Volkswagen Brasil" />

            </div>

        </div>

    </div>

</div>