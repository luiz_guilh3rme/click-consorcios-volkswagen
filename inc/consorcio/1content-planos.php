<div class="container">

    <div  class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-bottom ">

        <h2>Planos Consórcio Volkswagen</h2>

        <p>O Consórcio Volkswagen oferece 2 tipos de planos: <strong>Especial</strong> e <strong>Gold</strong>. Analise qual atende melhor às suas expectativas e necessidades.</p>

    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-bottom">

        <ul class="nav nav-tabs" role="tablist">

            <li role="presentation" class="active"><a href="#especial" aria-controls="home" role="tab" data-toggle="tab">1. Planos Especiais</a></li>

            <li role="presentation"><a href="#gold" aria-controls="profile" role="tab" data-toggle="tab">2. Planos Gold</a></li>

        </ul>

        <div class="tab-content">

            <div role="tabpanel" class="tab-pane active" id="especial">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-bottom padding-none"></div>

                

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none border-azul-bottom">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            <img class="img-carros" src="../../img/carros/up.jpg" />

                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            <h2><i class="blue-consorcio fa fa-angle-right"></i> Conheça o Plano - Up</h2>

                            <p>1 - Selecione o plano o modelo e crédito de interesse;</p>

                            <p>2 - Solicite sua adesão on-line clicando no botão comprar;</p>

                            <p>3 - Preencha o formulário;</p>

                            <p>4 - Aguarde nossa confirmação e pronto;</p>

                            <p>5 - Pronto!</p>

                            <p>Após aderir a um grupo do CNVW você fará parte de um grupo seleto que visa segurança, tranquilidade, transparência e principalmente a garantia de fábrica, investindo de forma inteligente e econômica com o Consórcio Nacional Volkswagen.</p>

                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>TAKE UP! 2 PORTAS</h3> 

                            <p>Crédito - <strong>R$ 30.990,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>MAIS LEVE</p>

                                    <p>LEVE</p>

                                    <p>NORMAL</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 363,68</p>

                                    <p>R$ 474,02</p>

                                    <p>R$ 623,80</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../take-up-2-portas.php">comprar</a></p>

                                    <p><a href="../../take-up-2-portas.php">comprar</a></p>

                                    <p><a href="../../take-up-2-portas.php">comprar</a></p>

                                </div>

                            </div>

                            

                        </div>

                        

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>TAKE UP! 4 PORTAS</h3> 

                            <p>Crédito - <strong>R$ 33.290,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>MAIS LEVE</p>

                                    <p>LEVE</p>

                                    <p>NORMAL</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 390,67</p>

                                    <p>R$ 509,20</p>

                                    <p>R$ 670,10</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../take-up-4-portas.php">comprar</a></p>

                                    <p><a href="../../take-up-4-portas.php">comprar</a></p>

                                    <p><a href="../../take-up-4-portas.php">comprar</a></p>

                                </div>

                            </div>

                            

                        </div>

                        

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>MOVE UP!</h3> 

                            <p>Crédito - <strong>R$ 40.790,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>MAIS LEVE</p>

                                    <p>LEVE</p>

                                    <p>NORMAL</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 478,69</p>

                                    <p>R$ 623,91</p>

                                    <p>R$ 821,07</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../move-up-2-portas.php">comprar</a></p>

                                    <p><a href="../../move-up-2-portas.php">comprar</a></p>

                                    <p><a href="../../move-up-2-portas.php">comprar</a></p>

                                </div>

                            </div>

                            

                        </div>

                        

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>HIGH UP! I -MOTION</h3> 

                            <p>Crédito - <strong>R$ 48.690,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>MAIS LEVE</p>

                                    <p>LEVE</p>

                                    <p>NORMAL</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 571,40</p>

                                    <p>R$ 744,75</p>

                                    <p>R$ 980,09</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../high-up-i-motion.php">comprar</a></p>

                                    <p><a href="../../high-up-i-motion.php">comprar</a></p>

                                    <p><a href="../../high-up-i-motion.php">comprar</a></p>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none border-azul-bottom">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            <img class="img-carros" src="../../img/carros/voyage.jpg" />

                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            <h2><i class="blue-consorcio fa fa-angle-right"></i> Conheça o Plano - Voyage</h2>

                            <p>1 - Selecione o plano o modelo e crédito de interesse;</p>

                            <p>2 - Solicite sua adesão on-line clicando no botão comprar;</p>

                            <p>3 - Preencha o formulário;</p>

                            <p>4 - Aguarde nossa confirmação e pronto;</p>

                            <p>5 - Pronto!</p>

                            <p>Após aderir a um grupo do CNVW você fará parte de um grupo seleto que visa segurança, tranquilidade, transparência e principalmente a garantia de fábrica, investindo de forma inteligente e econômica com o Consórcio Nacional Volkswagen.</p>

                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>VOYAGE 1.0 4P</h3> 

                            <p>Crédito - <strong>R$ 40.990,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>MAIS LEVE</p>

                                    <p>LEVE</p>

                                    <p>NORMAL</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 481,03</p>

                                    <p>R$ 626,97</p>

                                    <p>R$ 825,09</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../voyage-10.php">comprar</a></p>

                                    <p><a href="../../voyage-10.php">comprar</a></p>

                                    <p><a href="../../voyage-10.php">comprar</a></p>

                                </div>

                            </div>

                            

                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>VOYAGE 1.6</h3> 

                            <p>Crédito - <strong>R$ 44.990,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>MAIS LEVE</p>

                                    <p>LEVE</p>

                                    <p>NORMAL</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 527,98</p>

                                    <p>R$ 688,16</p>

                                    <p>R$ 905,61</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../voyage-16.php">comprar</a></p>

                                    <p><a href="../../voyage-16.php">comprar</a></p>

                                    <p><a href="../../voyage-16.php">comprar</a></p>

                                </div>

                            </div>

                            

                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>VOYAGE 1.6 HIGHLINE</h3> 

                            <p>Crédito - <strong>R$ 60.210,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>MAIS LEVE</p>

                                    <p>LEVE</p>

                                    <p>NORMAL</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 706,59</p>

                                    <p>R$ 920,96</p>

                                    <p>R$ 1.211,97</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../voyage-16-highline.php">comprar</a></p>

                                    <p><a href="../../voyage-16-highline.php">comprar</a></p>

                                    <p><a href="../../voyage-16-highline.php">comprar</a></p>

                                </div>

                            </div>

                            

                        </div>

                        

                    </div>

                </div>

                

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none border-azul-bottom">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            <img class="img-carros" src="../../img/carros/gol.jpg" />

                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            <h2><i class="blue-consorcio fa fa-angle-right"></i> Conheça o Plano - Gol</h2>

                            <p>1 - Selecione o plano o modelo e crédito de interesse;</p>

                            <p>2 - Solicite sua adesão on-line clicando no botão comprar;</p>

                            <p>3 - Preencha o formulário;</p>

                            <p>4 - Aguarde nossa confirmação e pronto;</p>

                            <p>5 - Pronto!</p>

                            <p>Após aderir a um grupo do CNVW você fará parte de um grupo seleto que visa segurança, tranquilidade, transparência e principalmente a garantia de fábrica, investindo de forma inteligente e econômica com o Consórcio Nacional Volkswagen.</p>

                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>GOL 1.6</h3> 

                            <p>Crédito - <strong>R$ 37.890,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>MAIS LEVE</p>

                                    <p>LEVE</p>

                                    <p>NORMAL</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 444,65</p>

                                    <p>R$ 579,56</p>

                                    <p>R$ 762,69</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../gol-16.php">comprar</a></p>

                                    <p><a href="../../gol-16.php">comprar</a></p>

                                    <p><a href="../../gol-16.php">comprar</a></p>

                                </div>

                            </div>

                            

                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>GOL 1.6 HIGHLINE</h3> 

                            <p>Crédito - <strong>R$ 52.070,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>MAIS LEVE</p>

                                    <p>LEVE</p>

                                    <p>NORMAL</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 611,06</p>

                                    <p>R$ 796,45</p>

                                    <p>R$ 1.048,12</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../gol-16-highline.php">comprar</a></p>

                                    <p><a href="../../gol-16-highline.php">comprar</a></p>

                                    <p><a href="../../gol-16-highline.php">comprar</a></p>

                                </div>

                            </div>

                            

                        </div>

                        

                    </div>

                </div>

                

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none border-azul-bottom">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            <img class="img-carros" src="../../img/carros/fox.jpg" />

                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            <h2><i class="blue-consorcio fa fa-angle-right"></i> Conheça o Plano - Fox</h2>

                            <p>1 - Selecione o plano o modelo e crédito de interesse;</p>

                            <p>2 - Solicite sua adesão on-line clicando no botão comprar;</p>

                            <p>3 - Preencha o formulário;</p>

                            <p>4 - Aguarde nossa confirmação e pronto;</p>

                            <p>5 - Pronto!</p>

                            <p>Após aderir a um grupo do CNVW você fará parte de um grupo seleto que visa segurança, tranquilidade, transparência e principalmente a garantia de fábrica, investindo de forma inteligente e econômica com o Consórcio Nacional Volkswagen.</p>

                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>NOVO FOX 1.0</h3> 

                            <p>Crédito - <strong>R$ 43.820,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>MAIS LEVE</p>

                                    <p>LEVE</p>

                                    <p>NORMAL</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 514,25</p>

                                    <p>R$ 670,26</p>

                                    <p>R$ 882,06</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../novo-fox-10.php">comprar</a></p>

                                    <p><a href="../../novo-fox-10.php">comprar</a></p>

                                    <p><a href="../../novo-fox-10.php">comprar</a></p>

                                </div>

                            </div>

                            

                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>NOVO FOX 1.6</h3> 

                            <p>Crédito - <strong>R$ 47.390,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>MAIS LEVE</p>

                                    <p>LEVE</p>

                                    <p>NORMAL</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 556,14</p>

                                    <p>R$ 709,57</p>

                                    <p>R$ 953,92</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../novo-fox-16.php">comprar</a></p>

                                    <p><a href="../../novo-fox-16.php">comprar</a></p>

                                    <p><a href="../../novo-fox-16.php">comprar</a></p>

                                </div>

                            </div>

                            

                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>NOVO FOX 1.6 I-MOTION</h3> 

                            <p>Crédito - <strong>R$ 53.170,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>MAIS LEVE</p>

                                    <p>LEVE</p>

                                    <p>NORMAL</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 623,97</p>

                                    <p>R$ 802,87</p>

                                    <p>R$ 1.070,26</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../novo-fox-16-i-motion.php">comprar</a></p>

                                    <p><a href="../../novo-fox-16-i-motion.php">comprar</a></p>

                                    <p><a href="../../novo-fox-16-i-motion.php">comprar</a></p>

                                </div>

                            </div>

                            

                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>NOVO FOX 1.6 HIGHLINE</h3> 

                            <p>Crédito - <strong>R$ 54.700,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>MAIS LEVE</p>

                                    <p>LEVE</p>

                                    <p>NORMAL</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 641,93</p>

                                    <p>R$ 818,17</p>

                                    <p>R$ 1.101,06</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../novo-fox-16-highline.php">comprar</a></p>

                                    <p><a href="../../novo-fox-16-highline.php">comprar</a></p>

                                    <p><a href="../../novo-fox-16-highline.php">comprar</a></p>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none border-azul-bottom">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            <img class="img-carros" src="../../img/carros/saveiro.jpg" />

                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            <h2><i class="blue-consorcio fa fa-angle-right"></i> Conheça o Plano - Saveiro</h2>

                            <p>1 - Selecione o plano o modelo e crédito de interesse;</p>

                            <p>2 - Solicite sua adesão on-line clicando no botão comprar;</p>

                            <p>3 - Preencha o formulário;</p>

                            <p>4 - Aguarde nossa confirmação e pronto;</p>

                            <p>5 - Pronto!</p>

                            <p>Após aderir a um grupo do CNVW você fará parte de um grupo seleto que visa segurança, tranquilidade, transparência e principalmente a garantia de fábrica, investindo de forma inteligente e econômica com o Consórcio Nacional Volkswagen.</p>

                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>SAVEIRO 1.6 CS</h3> 

                            <p>Crédito - <strong>R$ 44.290,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>MAIS LEVE</p>

                                    <p>LEVE</p>

                                    <p>NORMAL</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 519,76</p>

                                    <p>R$ 677,45</p>

                                    <p>R$ 891,52</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../saveiro-cs.php">comprar</a></p>

                                    <p><a href="../../saveiro-cs.php">comprar</a></p>

                                    <p><a href="../../saveiro-cs.php">comprar</a></p>

                                </div>

                            </div>

                            

                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>SAVEIRO 1.6 CE</h3> 

                            <p>Crédito - <strong>R$ 48.590,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>MAIS LEVE</p>

                                    <p>LEVE</p>

                                    <p>NORMAL</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 570,22</p>

                                    <p>R$ 743,22</p>

                                    <p>R$ 978,07</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../saveiro-ce.php">comprar</a></p>

                                    <p><a href="../../saveiro-ce.php">comprar</a></p>

                                    <p><a href="../../saveiro-ce.php">comprar</a></p>

                                </div>

                            </div>

                            

                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>SAVEIRO 1.6 CD</h3> 

                            <p>Crédito - <strong>R$ 53.990,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>MAIS LEVE</p>

                                    <p>LEVE</p>

                                    <p>NORMAL</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 633,59</p>

                                    <p>R$ 817,86</p>

                                    <p>R$ 1.086,77</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../saveiro-cd.php">comprar</a></p>

                                    <p><a href="../../saveiro-cd.php">comprar</a></p>

                                    <p><a href="../../saveiro-cd.php">comprar</a></p>

                                </div>

                            </div>

                            

                        </div>

                        

                    </div>

                </div>

                

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none border-azul-bottom">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            <img class="img-carros" src="../../img/carros/spacefox.jpg" />

                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            <h2><i class="blue-consorcio fa fa-angle-right"></i> Conheça o Plano - Spacefox</h2>

                            <p>1 - Selecione o plano o modelo e crédito de interesse;</p>

                            <p>2 - Solicite sua adesão on-line clicando no botão comprar;</p>

                            <p>3 - Preencha o formulário;</p>

                            <p>4 - Aguarde nossa confirmação e pronto;</p>

                            <p>5 - Pronto!</p>

                            <p>Após aderir a um grupo do CNVW você fará parte de um grupo seleto que visa segurança, tranquilidade, transparência e principalmente a garantia de fábrica, investindo de forma inteligente e econômica com o Consórcio Nacional Volkswagen.</p>

                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>SPACEFOX 1.6 TREND</h3> 

                            <p>Crédito - <strong>R$ 60.310,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>MAIS LEVE</p>

                                    <p>LEVE</p>

                                    <p>NORMAL</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 707,76</p>

                                    <p>R$ 922,49</p>

                                    <p>R$ 1.213,99</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../spacefox-trend.php">comprar</a></p>

                                    <p><a href="../../spacefox-trend.php">comprar</a></p>

                                    <p><a href="../../spacefox-trend.php">comprar</a></p>

                                </div>

                            </div>

                            

                        </div>

                    </div>

                </div>

                

                

            </div>

            

            <div role="tabpanel" class="tab-pane" id="gold">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-bottom padding-none"></div>

                

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none border-azul-bottom">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            <img class="img-carros" src="../../img/carros/jetta.jpg" />

                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            <h2><i class="blue-consorcio fa fa-angle-right"></i> Conheça o Plano - Jetta</h2>

                            <p>1 - Selecione o plano o modelo e crédito de interesse;</p>

                            <p>2 - Solicite sua adesão on-line clicando no botão comprar;</p>

                            <p>3 - Preencha o formulário;</p>

                            <p>4 - Aguarde nossa confirmação e pronto;</p>

                            <p>5 - Pronto!</p>

                            <p>Após aderir a um grupo do CNVW você fará parte de um grupo seleto que visa segurança, tranquilidade, transparência e principalmente a garantia de fábrica, investindo de forma inteligente e econômica com o Consórcio Nacional Volkswagen.</p>

                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                        

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>JETTA TRENDLINE</h3> 

                            <p>Crédito - <strong>R$ 70.990,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>GOLD</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 1.373,53</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../jetta-trendline.php">comprar</a></p>

                                </div>

                            </div>

                            

                        </div>

                        

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>JETTA COMFORTLINE AUT</h3> 

                            <p>Crédito - <strong>R$ 75.490,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>GOLD</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 1.460,60</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../jetta-trendline-automatico.php">comprar</a></p>

                                </div>

                            </div>

                            

                        </div>

                        

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>JETTA HIGHLINE</h3> 

                            <p>Crédito - <strong>R$ 95.990,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>GOLD</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 1.857,24</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../jetta-trendline-highline.php">comprar</a></p>

                                </div>

                            </div>

                            

                        </div>

                        

                    </div>

                </div>

                

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none border-azul-bottom">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            <img class="img-carros" src="../../img/carros/golf.jpg" />

                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            <h2><i class="blue-consorcio fa fa-angle-right"></i> Conheça o Plano - Golf</h2>

                            <p>1 - Selecione o plano o modelo e crédito de interesse;</p>

                            <p>2 - Solicite sua adesão on-line clicando no botão comprar;</p>

                            <p>3 - Preencha o formulário;</p>

                            <p>4 - Aguarde nossa confirmação e pronto;</p>

                            <p>5 - Pronto!</p>

                            <p>Após aderir a um grupo do CNVW você fará parte de um grupo seleto que visa segurança, tranquilidade, transparência e principalmente a garantia de fábrica, investindo de forma inteligente e econômica com o Consórcio Nacional Volkswagen.</p>

                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                        

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>NOVO GOLF COMFORTINE</h3> 

                            <p>Crédito - <strong>R$ 75.790,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>GOLD</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 1.466,40</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../novo-golf-confotline.php">comprar</a></p>

                                </div>

                            </div>

                            

                        </div>

                        

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>NOVO GOLF</h3> 

                            <p>Crédito - <strong>R$ 84.690,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>GOLD</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 1.638,60</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../novo-golf.php">comprar</a></p>

                                </div>

                            </div>

                            

                        </div>

                        

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>NOVO GOLF AUT</h3> 

                            <p>Crédito - <strong>R$ 91.690,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>GOLD</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 1.774,04</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../novo-golf-automatico.php">comprar</a></p>

                                </div>

                            </div>

                            

                        </div>

                        

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>NOVO GOLF GTI</h3> 

                            <p>Crédito - <strong>R$ 112.790,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>GOLD</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 2.182,29</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../novo-golf-gti.php">comprar</a></p>

                                </div>

                            </div>

                            

                        </div>

                        

                    </div>

                </div>

                

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none border-azul-bottom">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            <img class="img-carros" src="../../img/carros/golf-variant.jpg" />

                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            <h2><i class="blue-consorcio fa fa-angle-right"></i> Conheça o Plano - Golf Variant</h2>

                            <p>1 - Selecione o plano o modelo e crédito de interesse;</p>

                            <p>2 - Solicite sua adesão on-line clicando no botão comprar;</p>

                            <p>3 - Preencha o formulário;</p>

                            <p>4 - Aguarde nossa confirmação e pronto;</p>

                            <p>5 - Pronto!</p>

                            <p>Após aderir a um grupo do CNVW você fará parte de um grupo seleto que visa segurança, tranquilidade, transparência e principalmente a garantia de fábrica, investindo de forma inteligente e econômica com o Consórcio Nacional Volkswagen.</p>

                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                        

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>NOVO GOLF VARIANT</h3> 

                            <p>Crédito - <strong>R$ 89.190,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>GOLD</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 1.725,67</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../novo-golf-variant.php">comprar</a></p>

                                </div>

                            </div>

                            

                        </div>

                        

                    </div>

                </div>

                

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none border-azul-bottom">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            <img class="img-carros" src="../../img/carros/fusca.jpg" />

                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            <h2><i class="blue-consorcio fa fa-angle-right"></i> Conheça o Plano - Fusca</h2>

                            <p>1 - Selecione o plano o modelo e crédito de interesse;</p>

                            <p>2 - Solicite sua adesão on-line clicando no botão comprar;</p>

                            <p>3 - Preencha o formulário;</p>

                            <p>4 - Aguarde nossa confirmação e pronto;</p>

                            <p>5 - Pronto!</p>

                            <p>Após aderir a um grupo do CNVW você fará parte de um grupo seleto que visa segurança, tranquilidade, transparência e principalmente a garantia de fábrica, investindo de forma inteligente e econômica com o Consórcio Nacional Volkswagen.</p>

                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                        

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>NOVO FUSCA</h3> 

                            <p>Crédito - <strong>R$ 105.302,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>GOLD</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 2.037,41</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../novo-fusca.php">comprar</a></p>

                                </div>

                            </div>

                            

                        </div>

                        

                    </div>

                </div>

                

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none border-azul-bottom">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            <img class="img-carros" src="../../img/carros/amarok.jpg" />

                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            <h2><i class="blue-consorcio fa fa-angle-right"></i> Conheça o Plano - Amarok</h2>

                            <p>1 - Selecione o plano o modelo e crédito de interesse;</p>

                            <p>2 - Solicite sua adesão on-line clicando no botão comprar;</p>

                            <p>3 - Preencha o formulário;</p>

                            <p>4 - Aguarde nossa confirmação e pronto;</p>

                            <p>5 - Pronto!</p>

                            <p>Após aderir a um grupo do CNVW você fará parte de um grupo seleto que visa segurança, tranquilidade, transparência e principalmente a garantia de fábrica, investindo de forma inteligente e econômica com o Consórcio Nacional Volkswagen.</p>

                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                        

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>AMAROK CAB SIMPLES</h3> 

                            <p>Crédito - <strong>R$ 106.990,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>GOLD</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 2.070,07</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../amarok-cabine-simples.php">comprar</a></p>

                                </div>

                            </div>

                            

                        </div>

                        

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>AMAROK CAB DUP 4X4</h3> 

                            <p>Crédito - <strong>R$ 115.990,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>GOLD</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 2.244,20</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../amarok-cabine-dupla-4x4.php">comprar</a></p>

                                </div>

                            </div>

                            

                        </div>

                        

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>AMAROK TRENDLINE</h3> 

                            <p>Crédito - <strong>R$ 135.990,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>GOLD</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 2631,16</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../amarok-trendline.php">comprar</a></p>

                                </div>

                            </div>

                            

                        </div>

                        

                    </div>

                </div>

                

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none border-azul-bottom">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            <img class="img-carros" src="../../img/carros/passat.jpg" />

                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            <h2><i class="blue-consorcio fa fa-angle-right"></i> Conheça o Plano - Passat</h2>

                            <p>1 - Selecione o plano o modelo e crédito de interesse;</p>

                            <p>2 - Solicite sua adesão on-line clicando no botão comprar;</p>

                            <p>3 - Preencha o formulário;</p>

                            <p>4 - Aguarde nossa confirmação e pronto;</p>

                            <p>5 - Pronto!</p>

                            <p>Após aderir a um grupo do CNVW você fará parte de um grupo seleto que visa segurança, tranquilidade, transparência e principalmente a garantia de fábrica, investindo de forma inteligente e econômica com o Consórcio Nacional Volkswagen.</p>

                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                        

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>PASSAT</h3> 

                            <p>Crédito - <strong>R$ 128.400,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>GOLD</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 2.484,31</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../passat.php">comprar</a></p>

                                </div>

                            </div>

                            

                        </div>

                        

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>PASSAT VARIANT</h3> 

                            <p>Crédito - <strong>R$ 135.000,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>GOLD</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 2.612,01</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../passat-variant.php">comprar</a></p>

                                </div>

                            </div>

                            

                        </div>

                        

                    </div>

                </div>

                

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none border-azul-bottom">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            <img class="img-carros" src="../../img/carros/tiguan.jpg" />

                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            <h2><i class="blue-consorcio fa fa-angle-right"></i> Conheça o Plano - Tiguan</h2>

                            <p>1 - Selecione o plano o modelo e crédito de interesse;</p>

                            <p>2 - Solicite sua adesão on-line clicando no botão comprar;</p>

                            <p>3 - Preencha o formulário;</p>

                            <p>4 - Aguarde nossa confirmação e pronto;</p>

                            <p>5 - Pronto!</p>

                            <p>Após aderir a um grupo do CNVW você fará parte de um grupo seleto que visa segurança, tranquilidade, transparência e principalmente a garantia de fábrica, investindo de forma inteligente e econômica com o Consórcio Nacional Volkswagen.</p>

                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                        

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>TIGUAN</h3> 

                            <p>Crédito - <strong>R$ 131.990,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>GOLD</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 2.553,77</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../tiguan.php">comprar</a></p>

                                </div>

                            </div>

                            

                        </div>

                        

                    </div>

                </div>

                

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none border-azul-bottom">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            <img class="img-carros" src="../../img/carros/multistrada-s-t.jpg" />

                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            <h2><i class="blue-consorcio fa fa-angle-right"></i> Conheça o Plano - Multistrada</h2>

                            <p>1 - Selecione o plano o modelo e crédito de interesse;</p>

                            <p>2 - Solicite sua adesão on-line clicando no botão comprar;</p>

                            <p>3 - Preencha o formulário;</p>

                            <p>4 - Aguarde nossa confirmação e pronto;</p>

                            <p>5 - Pronto!</p>

                            <p>Após aderir a um grupo do CNVW você fará parte de um grupo seleto que visa segurança, tranquilidade, transparência e principalmente a garantia de fábrica, investindo de forma inteligente e econômica com o Consórcio Nacional Volkswagen.</p>

                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                        

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>MULTISTRADA S TOURING</h3> 

                            <p>Crédito - <strong>R$ 71.900,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>GOLD</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 1.391,14</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../multistrada-s-touring.php">comprar</a></p>

                                </div>

                            </div>

                            

                        </div>

                        

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>MULTISTRADA S PIKES PEAK</h3> 

                            <p>Crédito - <strong>R$ 871.900,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>GOLD</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 1.584,62</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../multistrada-s-pikes-peak.php">comprar</a></p>

                                </div>

                            </div>

                            

                        </div>

                        

                    </div>

                </div>

                

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none border-azul-bottom">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            <img class="img-carros" src="../../img/carros/ducati-diavel-carbon.jpg" />

                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            <h2><i class="blue-consorcio fa fa-angle-right"></i> Conheça o Plano - Diavel</h2>

                            <p>1 - Selecione o plano o modelo e crédito de interesse;</p>

                            <p>2 - Solicite sua adesão on-line clicando no botão comprar;</p>

                            <p>3 - Preencha o formulário;</p>

                            <p>4 - Aguarde nossa confirmação e pronto;</p>

                            <p>5 - Pronto!</p>

                            <p>Após aderir a um grupo do CNVW você fará parte de um grupo seleto que visa segurança, tranquilidade, transparência e principalmente a garantia de fábrica, investindo de forma inteligente e econômica com o Consórcio Nacional Volkswagen.</p>

                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                        

                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 a-diferenciado">

                            <h3>DIAVEL CARBON</h3> 

                            <p>Crédito - <strong>R$ 74.900,00</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <p><i class="blue-consorcio fa fa-angle-right"></i> <strong>Selecione o Plano</strong></p>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 border-azul-bottom"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 padding-none">

                                    <p>GOLD</p>

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 padding-none">

                                    <p>R$ 1.410,49</p>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 padding-none">

                                    <p><a href="../../diavel-carbon.php">comprar</a></p>

                                </div>

                            </div>

                            

                        </div>

                        

                    </div>

                </div>

                

            </div>

        </div>

    </div>

    <div  class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-bottom-15x ">

        <p>* Imagens ilustrativas para os modelos - Condições e valores sujeitos a confirmação (consulta/pós compra) - Em caso de dúvidas sobre condições comerciais entrar em contato com a central de vendas on-line sede São Paulo pelo telefone (11) 2114-0010.</p>

    </div>

</div>