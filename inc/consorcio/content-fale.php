<div class="container">

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-up ">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 padding-none">

            <form action="#" method="post" class="fale-conosco" novalidate="novalidate" id="faleconosco" data-modal="fale-conosco">

                <!-- NewsP -->
                <input type="hidden" name="url" value="<?= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">
                <input type="hidden" name="identifier" value="Fale Conosco">
                <!-- //NewsP -->

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">
                        <label>Nome ou Razão:</label>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 padding-none">
                        <input type="text" name="nome" id="nome" required="" aria-required="true" placeholder="Nome">
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">
                        <label>Assunto Tipo:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 padding-none">
                        <select name="assunto">
                            <option value="Dúvidas">Dúvidas</option>
                            <option value="Compra">Compra</option>
                            <option value="Outros">Outros</option>
                        </select>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">
                        <label>Seu e-mail:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 padding-none">
                        <input type="email" name="email" id="email" required="" aria-required="true" placeholder="E-mail">
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">
                        <label>Telefone:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 padding-none">
                        <input type="text" name="telefone" id="telefone" required="" aria-required="true" placeholder="11 99999 9999">
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">
                        <label>Mensagem:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 padding-none">
                        <textarea name="msg" id="msg" required="" aria-required="true" placeholder="Faça aqui uma descrição de qual a sua duvida."></textarea>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                    <div class="col-xs-12 col-sm-12 col-lg-offset-4 col-md-5 col-lg-5 padding-none">
                        <input type="hidden" name="tipo" id="tipo" value="contato"/>
                        <input type="submit" class="buttomcompra" value="Enviar">
                    </div>
                </div>

            </form>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">

            <iframe id="ancor-mapa" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.298405928698!2d-46.60930228491693!3d-23.557723867343306!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5938a7939833%3A0x6f7a6aba6b66c6e0!2sR.+Borges+de+Figueiredo%2C+303+-+Mooca%2C+S%C3%A3o+Paulo+-+SP%2C+03110-010%2C+Brasil!5e0!3m2!1spt-BR!2sbr!4v1447253994900" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

            <h3 class="h3-fale-conosco">Horário de Funcionamento</h3>

            <p class="padding-none">Segunda à Sexta-Feira das 08:00 às 18:00<br>Sábados (Plantão) das 09:00 às 16:00</p>

            <p class="margin-bottom"><b>Consórcio Nacional Volkswagen</b><br>Rua Borges de Figueiredo, 303 Ed. Atrio Giorno Conj 216<br>Móoca - São Paulo / SP<br>CEP: 03110-010<br>CNPJ: 07.456.203/0001-36</p>

        </div>

    </div>

</div>