<?php
setlocale(LC_MONETARY,"pt_BR", "ptb");
require 'admin/include/config.php';
?>

    <div class="container">

        <div class="margin-bottom margin-up">

            <!-- Nav -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#especial" aria-controls="home" role="tab" data-toggle="tab">1. Planos Especiais</a>
                </li>
                <li role="presentation">
                    <a href="#gold" aria-controls="profile" role="tab" data-toggle="tab">2. Planos Gold</a>
                </li>
            </ul>

            <div class="tab-content">

                <div role="tabpanel" class="tab-pane active" id="especial">

                    <!-- TODO: Remover isso -->
                    <div class="margin-bottom"></div>

                    <?php
$sqlSel = "SELECT DISTINCT volks_planos_auto.* FROM volks_planos_auto
INNER JOIN volks_auto ON volks_auto.auto_plano_modelo = volks_planos_auto.auto_modelo_id AND volks_auto.auto_parcela_leve != 0";
$resultSel = mysql_query($sqlSel);
while($rowSel = mysql_fetch_assoc($resultSel)){
?>


                        <div class="border-azul-bottom">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                                    <img class="img-carros" src="../../img/carros/<?=$rowSel[" auto_modelo_imagem "] ?>" title="<?=$rowSel[" auto_modelo_imagem_title
                                        "] ?>" alt="<?=$rowSel[" auto_modelo_imagem_alt "] ?>" />
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                                    <h2>
                                        <i class="blue-consorcio fa fa-angle-right"></i>
                                        <?=utf8_encode($rowSel["auto_modelo_nome"]) ?>
                                    </h2>

                                    <table class="table-consorcio footable" cellspacing="0" cellpadding="0" border="0">
                                        <thead>
                                            <tr>
                                                <th>Modelo</th>
                                                <th data-hide="phone">Crédito</th>
                                                <th data-hide="phone">Plano Leve</th>
                                                <th data-hide="phone">Plano Normal</th>
                                                <th></th>
                                            </tr>
                                        </thead>

                                        <tbody>

                                            <?php
$sqlSelSub = "SELECT * FROM volks_auto WHERE auto_plano_modelo = ".$rowSel["auto_modelo_id"]." AND auto_parcela_leve != 0";
$resultSelSub = mysql_query($sqlSelSub);
while($rowSelSub = mysql_fetch_assoc($resultSelSub)){
?>
                                                <tr>
                                                    <td>
                                                        <?=utf8_encode($rowSelSub["auto_nome"]) ?>
                                                    </td>
                                                    <td>
                                                        <?=money_format('%n', $rowSelSub["auto_credit"]) ?>
                                                    </td>
                                                    <td>
                                                        <?=money_format('%n', $rowSelSub["auto_parcela_leve"]) ?>
                                                    </td>
                                                    <td>
                                                        <?=money_format('%n', $rowSelSub["auto_parcela_normal"]) ?>
                                                    </td>
                                                    <td>
                                                        <a href="http://www.consorciovolkswagen.com.br" title="Ver modelo <?=utf8_encode($rowSelSub[" auto_nome "]) ?>" class="btn btn-success btn-sm">Ver modelo</a>
                                                    </td>
                                                </tr>
                                                <?php
}
?>

                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                        <?php
}
?>
                </div>


                <div role="tabpanel" class="tab-pane" id="gold">

                    <!-- TODO: Remover isso -->
                    <div class="margin-bottom"></div>

                    <?php
                    $sqlSel = "SELECT DISTINCT volks_planos_auto.* FROM volks_planos_auto
                    INNER JOIN volks_auto ON volks_auto.auto_plano_modelo = volks_planos_auto.auto_modelo_id AND volks_auto.auto_parcela_leve = 0";
                    $resultSel = mysql_query($sqlSel);
                    while($rowSel1 = mysql_fetch_assoc($resultSel)){
                ?>


                        <div class="border-azul-bottom">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                                    <img class="img-carros" src="../../img/carros/<?=$rowSel1[" auto_modelo_imagem "] ?>" title="<?=$rowSel1[" auto_modelo_imagem_title
                                        "] ?>" alt="<?=$rowSel1[" auto_modelo_imagem_alt "] ?>"/>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                                    <h2>
                                        <i class="blue-consorcio fa fa-angle-right"></i>
                                        <?=utf8_encode($rowSel1["auto_modelo_nome"]) ?>
                                    </h2>

                                    <table class="table-consorcio footable2" cellspacing="0" cellpadding="0" border="0">
                                        <thead>
                                            <tr>
                                                <th>Modelo</th>
                                                <th data-hide="phone">Crédito</th>
                                                <th data-hide="phone">Plano Gold</th>
                                                <th></th>
                                            </tr>
                                        </thead>

                                        <tbody>

                                            <?php
                                    $sqlSelSub = "SELECT * FROM volks_auto WHERE auto_plano_modelo = ".$rowSel1["auto_modelo_id"]." AND auto_parcela_leve = 0";


                                    $resultSelSub = mysql_query($sqlSelSub);
                                    while($rowSelSub = mysql_fetch_assoc($resultSelSub)){
                                        ?>
                                                <tr>
                                                    <td>
                                                        <?=utf8_encode($rowSelSub["auto_nome"]) ?>
                                                    </td>
                                                    <td>
                                                        <?=money_format('%n', $rowSelSub["auto_credit"]) ?>
                                                    </td>
                                                    <td>
                                                        <?=money_format('%n', $rowSelSub["auto_parcela_gold"]) ?>
                                                    </td>
                                                    <td>
                                                        <a href="http://www.consorciovolkswagen.com.br" title="Ver modelo <?=utf8_encode($rowSelSub[" auto_nome "]) ?>"
                                                            class="btn btn-success btn-sm">Ver modelo</a>
                                                    </td>
                                                </tr>
                                                <?php
                                    }
                                    ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php
                }
                ?>
                </div>

            </div>

        </div>

        <div class="row margin-bottom-15x">
            <p>O Consórcio Nacional Volkswagen é o único garantido pela própria Volkswagen. Frete incluso. Pintura metálica
                e opcionais não inclusos. Planos sem taxa de adesão. Seguro de vida incluso no valor da prestação. Os modelos,
                códigos e valores estão sujeitos a alterações conforme a política de comercialização da fábrica. Crédito
                sujeito à aprovação na contemplação. Imagens meramente ilustrativas.</p>
        </div>


    </div>