<div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 content-consorsio">
        <p>Conheça algumas das principais questões abordadas por participantes do Consórcio Nacional Volkswagen durante o processo de adesão, esclareça de forma rápida e objetiva todas as principais dúvidas com relação aos serviços, bens, planos, valores, entre outros tópicos.</p>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 linha-azul-bottom padding-none">
            <a class="btn btn-primary btn-ggggg" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><i class="blue-consorcio fa fa-angle-right"></i>Como posso obter uma cota do Consórcio Nacional Volkswagen?</a>
            <div class="collapse" id="collapseExample">
                <div class="well">
                    <p>É muito fácil. Basta clicar aqui e preencher a Proposta de Adesão com os seus dados, assinar e efetuar o pagamento da 1ª parcela. Assim que o seu grupo estiver formado, você receberá o comunicado de constituição e adesão ao grupo de consórcio confirmando sua participação, com os dados do seu grupo, do plano escolhido, a sua senha e o acesso eletrônico, além do calendário das assembléias. Um de nossos consultores entrará em contato para maiores detalhes e infomação em como proceder. Lembre-se que todo e qualquer pagamento deve ser efetuado sempre em favor do Consórcio Nacional Volkswagen.</p>
                    <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Fechar</button>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 linha-azul-bottom padding-none">
            <a class="btn btn-primary btn-ggggg" role="button" data-toggle="collapse" href="#collapseExample1" aria-expanded="false" aria-controls="collapseExample"><i class="blue-consorcio fa fa-angle-right"></i>Quais são as condições para participar do Consórcio Nacional Volkswagen?</a>
            <div class="collapse" id="collapseExample1">
                <div class="well">
                    <p>Basta que você seja maior de 18 anos ou comprovadamente emancipado.</p>
                    <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample1" aria-expanded="false" aria-controls="collapseExample">Fechar</button>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 linha-azul-bottom padding-none">
            <a class="btn btn-primary btn-ggggg" role="button" data-toggle="collapse" href="#collapseExample2" aria-expanded="false" aria-controls="collapseExample"><i class="blue-consorcio fa fa-angle-right"></i>Tendo a qualificação acima, quais são os outros requisitos para que eu possa ingressar no plano?</a>
            <div class="collapse" id="collapseExample2">
                <div class="well">
                    <p>Sua prestação mensal do consórcio não poderá ultrapassar 30% do seu salário bruto, acrescido da renda familiar, se preciso for, para atingir esse patamar. Caso a opção tenha sido pelo Consórcio Flexível Volkswagen, será considerado para análise da capacidade econômico-financeira três vezes o valor da parcela que corresponder ao penúltimo período do plano.</p>
                    <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample2" aria-expanded="false" aria-controls="collapseExample">Fechar</button>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 linha-azul-bottom padding-none">
            <a class="btn btn-primary btn-ggggg" role="button" data-toggle="collapse" href="#collapseExample3" aria-expanded="false" aria-controls="collapseExample"><i class="blue-consorcio fa fa-angle-right"></i>Quais são as vantagens do Consórcio Nacional Volkswagen?</a>
            <div class="collapse" id="collapseExample3">
                <div class="well">
                    <p>São várias. Além de possuir planos com os mais variados prazos para praticamente toda a linha Volkswagen (inclusive para caminhões e ônibus), atendimento personalizado, atuação em todo o território nacional, assembléias transmitidas via satélite para a Rede de Concessionários, o Consórcio Nacional Volkswagen é o único garantido pela própria Volkswagen.</p>
                    <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample3" aria-expanded="false" aria-controls="collapseExample">Fechar</button>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 linha-azul-bottom padding-none">
            <a class="btn btn-primary btn-ggggg" role="button" data-toggle="collapse" href="#collapseExample4" aria-expanded="false" aria-controls="collapseExample"><i class="blue-consorcio fa fa-angle-right"></i>Quantas cotas de consórcio posso adquirir?</a>
            <div class="collapse" id="collapseExample4">
                <div class="well">
                    <p>Não existe restrições à quantidade de cotas adiquiridas.</p>
                    <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample4" aria-expanded="false" aria-controls="collapseExample">Fechar</button>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 linha-azul-bottom padding-none">
            <a class="btn btn-primary btn-ggggg" role="button" data-toggle="collapse" href="#collapseExample5" aria-expanded="false" aria-controls="collapseExample"><i class="blue-consorcio fa fa-angle-right"></i>Como são efetuados os pagamentos do consórcio?</a>
            <div class="collapse" id="collapseExample5">
                <div class="well">
                    <p>Os pagamentos podem ser realizados em qualquer banco participante do sistema integrado de compensação até a data de vencimento, utilizando o Extrato Mensal que enviaremos para o seu endereço de correspondência. Após a data de vencimento (até 30 dias após), o pagamento poderá ser efetuado em uma das agências do banco cobrador. A impressão do boleto bancário poderá ser feita através do nosso Atendimento Eletrônico via Internet.</p>
                    <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample5" aria-expanded="false" aria-controls="collapseExample">Fechar</button>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 linha-azul-bottom padding-none">
            <a class="btn btn-primary btn-ggggg" role="button" data-toggle="collapse" href="#collapseExample6" aria-expanded="false" aria-controls="collapseExample"><i class="blue-consorcio fa fa-angle-right"></i>O que acontece quando a prestação mensal for paga após o vencimento?</a>
            <div class="collapse" id="collapseExample6">
                <div class="well">
                    <p>Sempre que houver atraso do pagamento da sua prestação mensal, sua cota não participará do sorteio e não será possível ofertar lance para a assembléia em que ocorreu o atraso. As prestações vencidas ficarão sujeitas a juros de mora e multa contratual, bem como a eventuais reajustes ocorridos no preço do Veículo Básico do Plano.</p>
                    <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample6" aria-expanded="false" aria-controls="collapseExample">Fechar</button>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 linha-azul-bottom padding-none">
            <a class="btn btn-primary btn-ggggg" role="button" data-toggle="collapse" href="#collapseExample13" aria-expanded="false" aria-controls="collapseExample"><i class="blue-consorcio fa fa-angle-right"></i>Há reajuste nas prestações do consórcio?</a>
            <div class="collapse" id="collapseExample13">
                <div class="well">
                    <p>As prestações mensais do consórcio têm como base o valor do Veículo Básico do Plano, sofrendo alterações somente nos casos de aumento ou redução do preço do veículo.</p>
                    <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample13" aria-expanded="false" aria-controls="collapseExample">Fechar</button>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 linha-azul-bottom padding-none">
            <a class="btn btn-primary btn-ggggg" role="button" data-toggle="collapse" href="#collapseExample7" aria-expanded="false" aria-controls="collapseExample"><i class="blue-consorcio fa fa-angle-right"></i>É possível antecipar o pagamento das prestações mensais?</a>
            <div class="collapse" id="collapseExample7">
                <div class="well">
                    <p>Sim. Basta somar ao valor do Extrato Mensal a quantia que deseja antecipar, preenchendo o campo "valor cobrado" com o total a pagar. O valor pago a mais será considerado para a amortização de suas prestações mensais na ordem inversa , a contar da última.</p>
                    <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample7" aria-expanded="false" aria-controls="collapseExample">Fechar</button>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 linha-azul-bottom padding-none">
            <a class="btn btn-primary btn-ggggg" role="button" data-toggle="collapse" href="#collapseExample8" aria-expanded="false" aria-controls="collapseExample"><i class="blue-consorcio fa fa-angle-right"></i>É possível quitar antecipadamente a cota?</a>
            <div class="collapse" id="collapseExample8">
                <div class="well">
                    <p>Sim. No entanto, caso a cota não esteja contemplada, não será mais possível ofertar lance, devendo o consorciado aguardar a contemplação por sorteio. Se o pagamento for efetuado fora da data da assembléia, a cota estará sujeita a reajustes de acordo com o contrato.</p>
                    <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample8" aria-expanded="false" aria-controls="collapseExample">Fechar</button>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 linha-azul-bottom padding-none">
            <a class="btn btn-primary btn-ggggg" role="button" data-toggle="collapse" href="#collapseExample9" aria-expanded="false" aria-controls="collapseExample"><i class="blue-consorcio fa fa-angle-right"></i>Como devo proceder caso não receba o Extrato Mensal?</a>
            <div class="collapse" id="collapseExample9">
                <div class="well">
                    <p>Caso isso aconteça, você poderá obter a 2a via do seu Extrato Mensal pelo Atendimento Eletrônico, no site (canto superior direito), pela concessionária Volkswagen mais próxima de você ou por uma de nossas Centrais de Atendimento à clientes.</p>
                    <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample9" aria-expanded="false" aria-controls="collapseExample">Fechar</button>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 linha-azul-bottom padding-none">
            <a class="btn btn-primary btn-ggggg" role="button" data-toggle="collapse" href="#collapseExample12" aria-expanded="false" aria-controls="collapseExample"><i class="blue-consorcio fa fa-angle-right"></i>Quantos veículos são entregues por mês?</a>
            <div class="collapse" id="collapseExample12">
                <div class="well">
                    <p>Depende do plano escolhido. No Plano Normal, são dois veículos, sendo um por sorteio e outro por lance. No Plano Leve de Automóveis e Caminhões, são três veículos, sendo um por sorteio e dois por lance. Já no Plano Flexível, as quantidades de veículos entregues por mês são crescentes ao longo do prazo de duração do plano, variando de 3 a 7 veículos, sendo 1 por sorteio e os demais por Lance. Vale lembrar que, independente da modalidade do plano, as contemplações estão condicionadas à existência de recursos financeiros no fundo comum dos grupos.</p>
                    <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample12" aria-expanded="false" aria-controls="collapseExample">Fechar</button>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 linha-azul-bottom padding-none">
            <a class="btn btn-primary btn-ggggg" role="button" data-toggle="collapse" href="#collapseExample10" aria-expanded="false" aria-controls="collapseExample"><i class="blue-consorcio fa fa-angle-right"></i>Como funciona o Plano Normal do Consórcio Nacional Volkswagen?</a>
            <div class="collapse" id="collapseExample10">
                <div class="well">
                    <p>No Plano Normal do Consórcio Nacional Volkswagen, você paga uma prestação proporcional ao valor do veículo dividido pelo prazo do grupo. Além disso, ainda existe o custo das taxas de administração, fundo de reserva e seguro de vida. Ao ser contemplado, você tem à disposição o valor integral referente ao Veículo Básico do seu plano.</p>
                    <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample10" aria-expanded="false" aria-controls="collapseExample">Fechar</button>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 linha-azul-bottom padding-none">
            <a class="btn btn-primary btn-ggggg" role="button" data-toggle="collapse" href="#collapseExample11" aria-expanded="false" aria-controls="collapseExample"><i class="blue-consorcio fa fa-angle-right"></i>E o Consórcio Leve Volkswagen?</a>
            <div class="collapse" id="collapseExample11">
                <div class="well">
                    <p>No Consórcio Leve Volkswagen, você paga prestações 25% menores que as de um plano normal até ser contemplado. Depois, essa diferença pode ser ajustada através das seguintes opções: A-) receber 100% do valor do crédito, dividindo os 25% devidos nas prestações restantes; B-) receber 75% do valor do crédito e continuar pagando prestações reduzidas. Observação: No Consórcio Leve Volkswagen, caso a contemplação ocorra na última assembléia, o consorciado não terá a opção de dividir a diferença dos 25%.</p>
                    <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample11" aria-expanded="false" aria-controls="collapseExample">Fechar</button>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 linha-azul-bottom padding-none">
            <a class="btn btn-primary btn-ggggg" role="button" data-toggle="collapse" href="#collapseExample14" aria-expanded="false" aria-controls="collapseExample"><i class="blue-consorcio fa fa-angle-right"></i>E o Consórcio Mais Leve VolksWagen ?</a>
            <div class="collapse" id="collapseExample14">
                <div class="well">
                    <p>No Consórcio Mais Leve Volkswagen, você paga prestações 33,3333% menores que as de um plano normal até ser contemplado. Depois, essa diferença pode ser ajustada através das seguintes opções: A-) receber 100% do valor do crédito, dividindo os 33,3333% devidos nas prestações restantes, ou abater do lance ofertado, B-) receber 66,6667% do valor do crédito e continuar pagando prestações reduzidas. Observação: No Consórcio Mais Leve Volkswagen, caso a contemplação ocorra na última assembléia, o consorciado não terá a opção de dividir a diferença dos 33,3333%.</p>
                    <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample14" aria-expanded="false" aria-controls="collapseExample">Fechar</button>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 linha-azul-bottom padding-none">
            <a class="btn btn-primary btn-ggggg" role="button" data-toggle="collapse" href="#collapseExample15" aria-expanded="false" aria-controls="collapseExample"><i class="blue-consorcio fa fa-angle-right"></i>Como são feitas as contemplações dos planos do Consórcio Nacional Volkswagen?</a>
            <div class="collapse" id="collapseExample15">
                <div class="well">
                    <p>As contemplações ocorrem por sorteio e por lance. Os sorteios são realizados com base nos números da Loteria Federal.</p>
                    <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample15" aria-expanded="false" aria-controls="collapseExample">Fechar</button>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 linha-azul-bottom padding-none">
            <a class="btn btn-primary btn-ggggg" role="button" data-toggle="collapse" href="#collapseExample16" aria-expanded="false" aria-controls="collapseExample"><i class="blue-consorcio fa fa-angle-right"></i>Como posso ofertar um lance?</a>
            <div class="collapse" id="collapseExample16">
                <div class="well">
                    <p>Até 1 (um) dia antes da data da sua assembléia: você poderá ofertar o lance pela Internet, acessando nosso site http://www.cnvw.com.br/, ou então pelo Talker – Central de Atendimento Eletrônico. No dia da assembléia: você poderá ofertar o lance em qualquer uma de nossas filiais ou em nossa matriz, localizada na Rua Volkswagen, 291, Parque Jabaquara -São Paulo.</p>
                    <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample16" aria-expanded="false" aria-controls="collapseExample">Fechar</button>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 linha-azul-bottom padding-none">
            <a class="btn btn-primary btn-ggggg" role="button" data-toggle="collapse" href="#collapseExample17" aria-expanded="false" aria-controls="collapseExample"><i class="blue-consorcio fa fa-angle-right"></i>Como é feita a apuração do lance vencedor?</a>
            <div class="collapse" id="collapseExample17">
                <div class="well">
                    <p>É considerado vencedor o lance que amortizar o maior número de prestações mensais do grupo. No caso de grupo misto, o lance vencedor não será necessariamente o de maior valor, mas sim aquele que amortizar o maior número de prestações. Em casos de empate, a seqüência de elegibilidade prevista no Sistema de Sorteio do contrato é que servirá como critério de desempate.</p>
                    <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample17" aria-expanded="false" aria-controls="collapseExample">Fechar</button>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 linha-azul-bottom padding-none">
            <a class="btn btn-primary btn-ggggg" role="button" data-toggle="collapse" href="#collapseExample18" aria-expanded="false" aria-controls="collapseExample"><i class="blue-consorcio fa fa-angle-right"></i>Quais são as formas de utilização do valor do lance?</a>
            <div class="collapse" id="collapseExample18">
                <div class="well">
                    <p>Plano Normal: o seu lance amortizará parcelas na Ordem Inversa, a contar da última, permitindo uma redução no prazo do seu plano. Consórcio Leve Volkswagen: caso a sua opção seja por 100% do valor do crédito, o seu lance amortizará prioritariamente os 25% devidos e o restante será utilizado na Ordem Inversa, a contar da última. Se a sua opção for 75% do valor do crédito, o seu lance amortizará parcelas na Ordem Inversa a contar da última, permitindo uma redução no prazo do seu plano. Consórcio Flexível Volkswagen: neste plano, você tem várias formas de utilização do valor do lance. São elas: <br>• Ordem Direta: o seu lance amortizará as três parcelas seguintes à contemplação. O restante amortizará parcelas na ordem inversa, a contar da última; <br>• Ordem Linear: o seu lance amortizará parcialmente e proporcionalmente as parcelas seguintes à contemplação, respeitando o limite mínimo do plano escolhido. Caso haja algum valor restante, ele será utilizado na Ordem Inversa; <br>• Ordem Inversa: o seu lance amortizará parcelas na Ordem Inversa, a contar da última; <br>• Ordem Mista: o seu lance amortizará parcelas nas Ordens Direta, Linear e Inversa, seguindo esta ordem de prioridade.</p>
                    <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample18" aria-expanded="false" aria-controls="collapseExample">Fechar</button>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 linha-azul-bottom padding-none">
            <a class="btn btn-primary btn-ggggg" role="button" data-toggle="collapse" href="#collapseExample19" aria-expanded="false" aria-controls="collapseExample"><i class="blue-consorcio fa fa-angle-right"></i>Meu veículo usado pode ser aceito como lance?</a>
            <div class="collapse" id="collapseExample19">
                <div class="well">
                    <p>Esta alternativa deve ser negociada junto ao seu concessionário, já que o lance tem de ser feito em dinheiro ou em cheque administrativo.</p>
                    <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample19" aria-expanded="false" aria-controls="collapseExample">Fechar</button>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 linha-azul-bottom padding-none">
            <a class="btn btn-primary btn-ggggg" role="button" data-toggle="collapse" href="#collapseExample20" aria-expanded="false" aria-controls="collapseExample"><i class="blue-consorcio fa fa-angle-right"></i>Quanto tempo terei para cobrir o lance?</a>
            <div class="collapse" id="collapseExample20">
                <div class="well">
                    <p>O prazo para cobertura do lance é de três dias úteis, contados a partir da data da assembléia de contemplação.</p>
                    <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample20" aria-expanded="false" aria-controls="collapseExample">Fechar</button>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 linha-azul-bottom padding-none">
            <a class="btn btn-primary btn-ggggg" role="button" data-toggle="collapse" href="#collapseExample21" aria-expanded="false" aria-controls="collapseExample"><i class="blue-consorcio fa fa-angle-right"></i>Quando for contemplado, poderei optar por outro veículo ou outras cores?</a>
            <div class="collapse" id="collapseExample21">
                <div class="well">
                    <p>Sim, desde que você se responsabilize pela diferença de preço, se houver, pagando-a diretamente para a concessionária no momento do faturamento.</p>
                    <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample21" aria-expanded="false" aria-controls="collapseExample">Fechar</button>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 linha-azul-bottom padding-none">
            <a class="btn btn-primary btn-ggggg" role="button" data-toggle="collapse" href="#collapseExample22" aria-expanded="false" aria-controls="collapseExample"><i class="blue-consorcio fa fa-angle-right"></i>Após ter sido contemplado, quanto tempo terei para efetuar o pedido do veículo?</a>
            <div class="collapse" id="collapseExample22">
                <div class="well">
                    <p>Recomendamos que você faça o pedido do veículo o quanto antes na sua concessionária Volkswagen, pois caso ocorra aumento no preço do veículo entre a data da Assembléia de Contemplação e o faturamento, a diferença deverá ser paga por você diretamente à concessionária. O seu crédito ficará disponível a partir do dia seguinte à contemplação, não tendo prazo máximo estabelecido para sua utilização.</p>
                    <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample22" aria-expanded="false" aria-controls="collapseExample">Fechar</button>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 linha-azul-bottom padding-none">
            <a class="btn btn-primary btn-ggggg" role="button" data-toggle="collapse" href="#collapseExample23" aria-expanded="false" aria-controls="collapseExample"><i class="blue-consorcio fa fa-angle-right"></i>É possível transferir a minha cota para terceiros, repassando as prestações mensais restantes?</a>
            <div class="collapse" id="collapseExample23">
                <div class="well">
                    <p>Sim. No entanto, o cadastro do novo consorciado deverá ser previamente aprovado pelo CONSÓRCIO NACIONAL VOLKSWAGEN. É necessário também estar em dia com os pagamentos mensais. Pelo Atendimento Eletrônico, em nosso site (<a target="_blank" href="http://www.cnvw.com.br/">http://www.cnvw.com.br/</a>), você poderá consultar a relação de documentos e as condições necessárias para realizar essa operação. Se preferir, informe-se em sua concessionária Volkswagen.</p>
                    <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample23" aria-expanded="false" aria-controls="collapseExample">Fechar</button>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 linha-azul-bottom padding-none">
            <a class="btn btn-primary btn-ggggg" role="button" data-toggle="collapse" href="#collapseExample24" aria-expanded="false" aria-controls="collapseExample"><i class="blue-consorcio fa fa-angle-right"></i>Onde, quando e em que horários são realizadas as assembléias?</a>
            <div class="collapse" id="collapseExample24">
                <div class="well">
                    <p>Para que você possa se planejar, as Assembléias de Contemplação ocorrem mensalmente, sempre às terças-feiras (exceto feriados), a partir das 9 horas, na sede do Consórcio Nacional Volkswagen, situada na rua Volkswagen, 291, no bairro do Jabaquara em São Paulo. No entanto, você pode acessar o resultado das assembléias por meio do nosso site <a target="_blank" href="http://www.cnvw.com.br/">http://www.cnvw.com.br/</a>, acionando o Talker (Central de Atendimento Eletrônico) ou ainda se preferir, em qualquer uma das Concessionárias Volkswagen, a partir das 11:00 horas do dia em que a assembléia for realizada. Não esqueça as datas das assembléias est ão assinaladas no Slip de pagamento mensal.</p>
                    <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample24" aria-expanded="false" aria-controls="collapseExample">Fechar</button>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 linha-azul-bottom padding-none">
            <a class="btn btn-primary btn-ggggg" role="button" data-toggle="collapse" href="#collapseExample25" aria-expanded="false" aria-controls="collapseExample"><i class="blue-consorcio fa fa-angle-right"></i>Preciso comparecer às Assembléias de Contemplação?</a>
            <div class="collapse" id="collapseExample25">
                <div class="well">
                    <p>Não se preocupe. Caso seja contemplado, você será comunicado por carta ou telegrama. O resultado das Assembléias é disponibilizado também nas páginas do Atendimento Eletrônico via Internet. No entanto, sua presença nas assembléias é sempre bem-vinda.</p>
                    <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample25" aria-expanded="false" aria-controls="collapseExample">Fechar</button>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 linha-azul-bottom padding-none">
            <a class="btn btn-primary btn-ggggg" role="button" data-toggle="collapse" href="#collapseExample26" aria-expanded="false" aria-controls="collapseExample"><i class="blue-consorcio fa fa-angle-right"></i>Como é calculado o valor da minha prestação mensal?</a>
            <div class="collapse" id="collapseExample26">
                <div class="well">
                    <p>O valor da prestação mensal é obtido aplicando-se sobre o valor do veículo do plano as taxas de administração e fundo de reserva. Depois, aplica-se o percentual de Contribuição Mensal do seu Plano e soma-se 0,0734% (calculado sobre o saldo devedor) referente ao seguro de vida.</p>
                    <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample26" aria-expanded="false" aria-controls="collapseExample">Fechar</button>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 linha-azul-bottom padding-none">
            <a class="btn btn-primary btn-ggggg" role="button" data-toggle="collapse" href="#collapseExample27" aria-expanded="false" aria-controls="collapseExample"><i class="blue-consorcio fa fa-angle-right"></i>Quando compro uma cota de consórcio tenho algum seguro?</a>
            <div class="collapse" id="collapseExample27">
                <div class="well">
                    <p>Sim. Existe um Seguro de Vida em Grupo para, em caso de óbito, garantir aos beneficiários do consorciado uma indenização que corresponderá à quitação do saldo devedor a vencer, tenha o consorciado sido contemplado ou não.</p>
                    <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample27" aria-expanded="false" aria-controls="collapseExample">Fechar</button>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 linha-azul-bottom padding-none">
            <a class="btn btn-primary btn-ggggg" role="button" data-toggle="collapse" href="#collapseExample28" aria-expanded="false" aria-controls="collapseExample"><i class="blue-consorcio fa fa-angle-right"></i>O que acontece se eu desistir antes do prazo de duração do meu grupo?</a>
            <div class="collapse" id="collapseExample28">
                <div class="well">
                    <p>Se realmente não lhe restar outra alternativa a não ser desistir do plano, o dinheiro será devolvido corrigido, descontadas as parcelas referentes às multas, penalidades e taxa de administração, conforme descrito no seu Contrato de Adesão e de acordo com a legislação vigente. A devolução ocorrerá após o encerramento do seu grupo. É importante ressaltar que a desistência só poderá ocorrer antes da sua contemplação</p>
                    <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample28" aria-expanded="false" aria-controls="collapseExample">Fechar</button>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 linha-azul-bottom padding-none">
            <a class="btn btn-primary btn-ggggg" role="button" data-toggle="collapse" href="#collapseExample29" aria-expanded="false" aria-controls="collapseExample"><i class="blue-consorcio fa fa-angle-right"></i>Posso solicitar exclusão de sorteio?</a>
            <div class="collapse" id="collapseExample29">
                <div class="well">
                    <p>A partir da segunda assembléia de contemplação, é possível solicitar exclusão do sorteio. O prazo mínimo de exclusão será de três assembléias, podendo o consorciado retornar a participar dos sorteios a qualquer momento mediante a solicitação formal. Será revertida de forma automática e independente de comunicação se os únicos consorciados a serem contemplados estiverem na situação de exclusão de sorteio.</p>
                    <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample29" aria-expanded="false" aria-controls="collapseExample">Fechar</button>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 linha-azul-bottom padding-none">
            <a class="btn btn-primary btn-ggggg" role="button" data-toggle="collapse" href="#collapseExample30" aria-expanded="false" aria-controls="collapseExample"><i class="blue-consorcio fa fa-angle-right"></i>É possível substituir o veículo com cláusula de alienação fiduciária em favor do Consórcio Nacional Volkswagen?</a>
            <div class="collapse" id="collapseExample30">
                <div class="well">
                    <p>Sim. No entanto, é necessária a prévia anuência do CONSÓRCIO NACIONAL VOLKSWAGEN, e a cota deverá estar na situação "em dia". Para os contratos com saldo devedor inferior a 50%, o veículo substituto deverá ter no máximo 5 (cinco) anos de fabricação. Já nos contratos com saldo superior a 50%, o veículo deverá ter no máximo 3 (três) anos de fabricação. Pelo Atendimento Eletrônico, em nosso site <a target="_blank" href="http://www.cnvw.com.br/">http://www.cnvw.com.br/</a>, você poderá consultar a relação de documentos e as condições necessárias para realizar essa operação. Se preferir, informe-se em sua concessionária Volkswagen.</p>
                    <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample30" aria-expanded="false" aria-controls="collapseExample">Fechar</button>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 linha-azul-bottom padding-none">
            <a class="btn btn-primary btn-ggggg" role="button" data-toggle="collapse" href="#collapseExample31" aria-expanded="false" aria-controls="collapseExample"><i class="blue-consorcio fa fa-angle-right"></i>Caso eu mude de endereço, como devo informar o Consórcio Nacional Volkswagen?</a>
            <div class="collapse" id="collapseExample31">
                <div class="well">
                    <p>O novo endereço poderá ser informado pelo nosso site na Internet, pela carta- resposta presente no contrato ou por uma de nossas Centrais de Atendimento a clientes. É muito importante que você mantenha seus dados atualizados para que possamos manter um contato permanente.</p>
                    <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample31" aria-expanded="false" aria-controls="collapseExample">Fechar</button>
                </div>
            </div>
        </div>
        
        <p>Para saber mais sobre o Consórcio Nacional Volkswagen, entre em contato com a Central de Relacionamento pelo telefone (11) 2114-0010, ou se preferir receba o contato de um de nossos Consultores. Leia o Contrato, click aqui!</p>
    </div>
</div>