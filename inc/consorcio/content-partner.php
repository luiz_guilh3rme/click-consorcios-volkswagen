<div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 content-consorsio">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <p>Encontre o caminho do sucesso através de nossos planos de Aliança e Parceria, ou tornando-se um colaborador direto de nossa equipe. Conheça as diferentes oportunidades através do canal abaixo.</p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-bottom-15x">
            <!--<h2 class="pag-interna-consorcio"><i class="blue-consorcio fa fa-angle-right"></i> Visão</h2>-->
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <h2 class="margin-top-none">Oportunidades</h2>
                <p>Este canal é dedicado a profissionais que estão buscando uma oportunidade em um mercado promissor e de constante crescimento. Se você possui este perfil, encaminhe hoje mesmo seu currículo para <a href="mailto:info@consorciovolkswagen.com.br">info@consorciovolkswagen.com.br</a> com o assunto "Recursos Humanos" e aguarde o processo de triagem e seleção, talvez você possa fazer parte desta grande família.</p>
                <h2 class="margin-top-none">Parcerias e Alianças</h2>
                <p>Se você é um profissional dedicado a venda e comercialização de planos de consórcio, e busca uma oportunidade exclusiva de trabalho, com total liberdade e com os respaldo uma grande marca, aproveite este meio inovador e venha já para nosso time.</p>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <h2 class="margin-top-none">Vantagens</h2>
                <ul class="ul-consorcio">
                    <li>
                        <p class="margin-bottom-none">Possibilidade de início imediato (após análise e treinamento);</p>
                    </li>
                    <li>
                        <p class="margin-bottom-none"> Uso das principais ferramentas de forma personalizada;</p>
                    </li>
                    <li>
                        <p class="margin-bottom-none"> Ganhos imediatos desde a primeira venda;</p>
                    </li>
                    <li>
                        <p class="margin-bottom-none"> Maior profissionalismo para o atendimento e prospecção;</p>
                    </li>
                    <li>
                        <p> Operação junto a um dos maiores Portais da Internet;</p>
                    </li>
                </ul>
                <h2 class="margin-top-none">Formulário de Cadastramento</h2>
                <p>Para dar o primeiro passo de uma possível parceria, realize o pré-cadastramento através do formulário abaixo e aguarde o contato de nossos gestores.</p>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none linha-azul">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                <form action="#" method="post" class="partner" novalidate="novalidate" data-modal="parceiro">

                    <!-- NEWSP -->
                    <input type="hidden" name="url" value="<?= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none margin-bottom">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                                <label>Nome ou Razão:</label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                                <input type="hidden" name="identifier" value="Formulário de Parceiro">
                                <input type="text" name="nome" id="nome" required="" aria-required="true" placeholder="Nome">
                            </div>
                        </div>
						  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                                <label>cep:</label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                                <input type="text" name="cep" id="cep" required="" aria-required="true" placeholder="CEP">
                            </div>
                        </div>
						   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                                <label>Endereço:</label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                                <input type="text" name="endereco" id="endereco" required="" aria-required="true" placeholder="Endereço">
                            </div>
                        </div>
						    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                                <label>Número:</label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                                <input type="text" name="numero" id="numero" required="" aria-required="true" placeholder="Numero">
                            </div>
                        </div>
						   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                                <label>Complemento:</label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                                <input type="text" name="complemento" id="complemento" required="" aria-required="true" placeholder="Complemento">
                            </div>
                        </div>
						  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                                <label>Bairro:</label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                                <input type="text" name="bairro" id="bairro" required="" aria-required="true" placeholder="Bairro">
                            </div>
                        </div>

						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                                <label>Cidade:</label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                                <input type="text" name="cidade" id="cidade" required="" aria-required="true" placeholder="Cidade">
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                                <label>Estado:</label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                                <select name="uf" class="frm_view">
                                    <option selected="">Selecione</option>
                                    <option value="AC">Acre</option>
                                    <option value="AL">Alagoas</option>
                                    <option value="AM">Amazonas</option>
                                    <option value="AP">Amapá</option>
                                    <option value="BA">Bahia</option>
                                    <option value="CE">Ceará</option>
                                    <option value="DF">Distrito Federal</option>
                                    <option value="ES">Espírito Santo</option>
                                    <option value="GO">Goiás</option>
                                    <option value="MA">Maranhão</option>
                                    <option value="MG">Minas Gerais</option>
                                    <option value="MS">Mato Grosso do Sul</option>
                                    <option value="MT">Mato Grosso</option>
                                    <option value="PA">Pará</option>
                                    <option value="PB">paraíba</option>
                                    <option value="PE">Pernambuco</option>
                                    <option value="PI">Piauí</option>
                                    <option value="PR">Paraná</option>
                                    <option value="RJ">Rio de Janeiro</option>
                                    <option value="RN">Rio Grande do Norte</option>
                                    <option value="RO">Rondônia</option>
                                    <option value="RR">Roraima</option>
                                    <option value="RS">Rio Grande do Sul</option>
                                    <option value="SC">Santa Catarina</option>
                                    <option value="SE">Sergipe</option>
                                    <option value="SP">São Paulo</option>
                                    <option value="TO">Tocantins</option>
                                </select>
                            </div>
                        </div>


                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none margin-bottom">

					  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                                <label>Nascimento:</label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                                <input type="text" name="nascimento" id="nascimento" required="" aria-required="true" placeholder="Nascimento">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                                <label>Telefone(RES):</label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                                <input type="text" name="telefone" id="telefone" required="" aria-required="true" placeholder="22 55555 4444">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                                <label>Celular:</label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                                <input type="text" name="celular" id="celular" required="" aria-required="true" placeholder="22 55555 4444">
                            </div>
                        </div>
						  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                                <label>Seu e-mail:</label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                                <input type="email" name="email" id="email" required="" aria-required="true" placeholder="E-mail">
                            </div>
                        </div>


                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                                <label>Mensagem:</label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                                <textarea name="msg" id="msg" required="" aria-required="true" placeholder="Faça aqui uma descrição de qual a sua duvida."></textarea>
                            </div>
                        </div>
                      
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none margin-bottom">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                            <p>Através da proposta manifesto a intenção de participar de grupo de consórcio, declarando serem verdadeiras todas as informações prestadas.</p>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none margin-bottom">
                        <input type="submit" class="buttomcompra" value="Enviar">
                    </div>
                </form>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-up">
            <p>* Formulário de intenção de adesão será encaminhado para central de análise e gestão em info@consorciovolkswagen.com.br.</p>
        </div>
    </div>
</div>