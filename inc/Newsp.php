<?php

class Newsp {
    protected $private;

    public function __construct($private){
        $this->private = $private;
    }

    public function saveLead($leadData, $unsetKeys = array()) {

        // Get data
        $data = $leadData;

        foreach($unsetKeys as $key) {
            unset($data[$key]);
        }

        if(empty($leadData['url']))
            $leadData['url'] = "";

        $data = array(
            'url' => $leadData['url'],
            'cp_token' => $this->private,
            'data' => $data
        );


        // Setup cURL
        $ch = curl_init('http://www.3xceler.com.br/api/v1/newsp/saveLead');
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode($data)
        ));

        //execute post
        $resultado = curl_exec($ch);

        // Check for errors
        if($resultado === FALSE){
            die(curl_error($ch));
        }

        if($resultado !== false)
            return true;

        else return false;
    }
}