<div class="container">

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-bottom ">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                <img class="img-carros" src="../../img/carros/golf-gti.jpg" />

            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                <h2><i class="blue-consorcio fa fa-angle-right"></i> Novo Golf GTI - 4 Portas</h2>

                <p>Se esta é sua opção ideal agora falta muito pouco para que você torne seu sonho uma realidade, conheça os detalhes do modelo e prossiga preenchendo o formulário com seus dados para iniciar a primeira fase de sua adesão ao grupo.</p>

                <p><img class="img-esquerda-carro" src="../../img/consorcio-volks-help.png" style="float: left;" />Se você possuir alguma dúvida poderá ainda consultar nosso <a href="../../faq.php" target="_blank">FAQ</a> ou utilizar uma consulta através dos meios de atendimento disponíveis, como: chat, e-mail ou telefone.</p>

                <p><i class="blue-consorcio fa fa-angle-left"></i> <a href="../../planos.php">Retornar aos planos</a></p>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                        <a class="btn-orcamento" href="#puxada-orcamento">

                            <span class="color-btn-orcamento1"><p>Compre esse plano</p></span>

                            <span class="color-btn-orcamento2"><p>Parcelas de</p></span>

                            <span class="color-btn-orcamento3"><p><span>R$</span>2.182,29</p></span>

                        </a>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                        <a class="btn-orcamento2" rel="modal" onclick="javascript:location='#janela1';">

                            <img src="../../img/consorcio-volkswagen-fale-conosco.png" />

                            <span class="color-btn-meliga1"><p>Nos te ligamos</p></span>

                            <span class="color-btn-meliga2"><p>O Consórcio Volkswagen entra em contato com você, basta um click!</p></span>

                            <p class="p-btn-orcamento"><span class="color-btn-meliga3">Volks, me liga!</span></p>

                        </a>

                    </div>

                </div>

            </div>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none"> 

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                <h2><i class="blue-consorcio fa fa-angle-right"></i> Informações sobre o plano</h2>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">PRAZO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>60 Meses</strong></p>

                    <p class="linha-azul-bottom">GRUPO DE CONSÓRCIO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>GOLD</strong></p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">CATEGORIA</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>CARROS326</strong></p>

                    <p class="linha-azul-bottom">CRÉDITO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>R$ 112.790,00</strong></p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">TIPO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>Pessoa Física</strong></p>

                    <p class="linha-azul-bottom">PARCELAS MENSAIS</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>R$ 2.182,29</strong></p>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none linha-azul-bottom-2x">

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                    <h3>Dados do Plano</h3>

                    <p>Adquira seu volkswagen com segurança e tranquilidade de quem tem mais de 30 anos de tradição no mercado.</p>

                    <p>Plano Gold:</p>

                    <p>Prazo de Duração: 60 meses</p>

                    <p>% Contribuição Mensal: Série M / Normal - 1,6667%</p>

                    <p>% Taxa de Administração Total: Antecipada: - / Deferida: 11,0%</p>

                    <p>Plano Gold: 120 Participantes</p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                    <h3>O melhor da Volkswagen </h3>

                    <p>em um único carro</p>

                    <p>Mitos são eternos. Afinal, todo mito é reinterpretado para diferentes épocas.</p>

                    <p>Volkswagen você conheçe você confia!</p>

                    <p>Consórcio Nacional Volkswagen - O Consórcio da Fábrica.</p>

                    <p>www.consorciovolkswagen.com.br</p>

                </div>

            </div>

        </div>

    </div>

    

</div>