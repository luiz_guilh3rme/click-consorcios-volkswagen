<div class="container">

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-bottom ">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                <img class="img-carros" src="../../img/carros/gol.jpg" />

            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                <h2><i class="blue-consorcio fa fa-angle-right"></i> Gol 1.6 - 4 Portas</h2>

                <p>Se esta é sua opção ideal agora falta muito pouco para que você torne seu sonho uma realidade, conheça os detalhes do modelo e prossiga preenchendo o formulário com seus dados para iniciar a primeira fase de sua adesão ao grupo.</p>

                <p><img class="img-esquerda-carro" src="../../img/consorcio-volks-help.png" style="float: left;" />Se você possuir alguma dúvida poderá ainda consultar nosso <a href="../../faq.php" target="_blank">FAQ</a> ou utilizar uma consulta através dos meios de atendimento disponíveis, como: chat, e-mail ou telefone.</p>

                <p><i class="blue-consorcio fa fa-angle-left"></i> <a href="../../planos.php">Retornar aos planos</a></p>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                        <a class="btn-orcamento" href="#puxada-orcamento">

                            <span class="color-btn-orcamento1"><p>Compre esse plano</p></span>

                            <span class="color-btn-orcamento2"><p>Parcelas de a partir de</p></span>

                            <span class="color-btn-orcamento3"><p><span>R$</span>444,65</p></span>

                        </a>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                        <a class="btn-orcamento2" rel="modal" onclick="javascript:location='#janela1';">

                            <img src="../../img/consorcio-volkswagen-fale-conosco.png" />

                            <span class="color-btn-meliga1"><p>Nos te ligamos</p></span>

                            <span class="color-btn-meliga2"><p>O Consórcio Volkswagen entra em contato com você, basta um click!</p></span>

                            <p class="p-btn-orcamento"><span class="color-btn-meliga3">Volks, me liga!</span></p>

                        </a>

                    </div>

                </div>

            </div>

        </div>

        

        

        

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none"> 

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                <h2><i class="blue-consorcio fa fa-angle-right"></i> Informações sobre o plano Mais Leve</h2>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">PRAZO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>72 Meses</strong></p>

                    <p class="linha-azul-bottom">GRUPO DE CONSÓRCIO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>MAIS LEVE</strong></p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">CATEGORIA</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>CARROS268</strong></p>

                    <p class="linha-azul-bottom">CRÉDITO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>R$ 37.890,00</strong></p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">TIPO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>Pessoa Física</strong></p>

                    <p class="linha-azul-bottom">PARCELAS MENSAIS</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>R$ 444,65</strong></p>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none linha-azul-bottom-2x">

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                    <h3>Dados do Plano</h3>

                    <p>Adquira seu volkswagen com segurança e tranquilidade de quem tem mais de 30 anos de tradição no mercado.</p>

                    <p>Plano Gold:</p>

                    <p>Prazo de Duração: 72 meses</p>

                    <p>% Contribuição Mensal: Série U / Mais Leve - 0,9259%</p>

                    <p>% Taxa de Administração Total: Antecipada: - / Deferida: 17,5%</p>

                    <p>Plano Mais Leve (Série U): 432 Participantes</p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                    <h3>Conheça alguns itens de série e opcionais disponíveis para o Novo Gol - Consulte a versão</h3>

                    <h4>Vista frontal</h4>

                    <p>A grade de tomada de ar, mais afilada e com três lâminas, dá destaque à logomarca Volkswagen e se une de maneira harmoniosa ao conjunto. Opcionalmente pode vir pintada na cor preto brilhante.</p>

                    <h4>Vista traseira</h4>

                    <p>O lado jovem do Gol é valorizado pelo opcional aerofólio traseiro pintado na cor do carro, ao qual o brake light é integrado.</p>

                    <h4>O Gol</h4>

                    <p>A boa dirigibilidade do Gol está presente até em sua versão mais econômica, a 1.0. À economia do motor 1.0 se aliam elementos de estilo que enaltecem o espírito esportivo que este carro oferece aos que o elegeram o automóvel mais querido do Brasil.</p>

                    <h4>Rodas</h4>

                    <p>Design de novas rodas para todas as versões. Rodas escurecidas aro 16 para versões Power. Design de logotipo VW novo e sofisticado.</p>

                    <p>www.consorciovolkswagen.com.br</p>

                </div>

            </div>

        </div>

        

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none"> 

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                <h2><i class="blue-consorcio fa fa-angle-right"></i> Informações sobre o plano Leve</h2>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">PRAZO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>60 Meses</strong></p>

                    <p class="linha-azul-bottom">GRUPO DE CONSÓRCIO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>LEVE</strong></p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">CATEGORIA</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>CARROS288</strong></p>

                    <p class="linha-azul-bottom">CRÉDITO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>R$ 37.890,00</strong></p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">TIPO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>Pessoa Física</strong></p>

                    <p class="linha-azul-bottom">PARCELAS MENSAIS</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>R$ 579,56</strong></p>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none linha-azul-bottom-2x">

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                    <h3>Dados do Plano</h3>

                    <p>Adquira seu volkswagen com segurança e tranquilidade de quem tem mais de 30 anos de tradição no mercado.</p>

                    <p>Plano Gold:</p>

                    <p>Prazo de Duração: 60 meses</p>

                    <p>% Contribuição Mensal: Série O / Leve - 1,2500%</p>

                    <p>% Taxa de Administração Total: Antecipada: - / Deferida: 15,5%</p>

                    <p>Plano Leve (Série O): 240 Participantes</p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                    <h3>Conheça alguns itens de série e opcionais disponíveis para o Novo Gol - Consulte a versão</h3>

                    <h4>Vista frontal</h4>

                    <p>A grade de tomada de ar, mais afilada e com três lâminas, dá destaque à logomarca Volkswagen e se une de maneira harmoniosa ao conjunto. Opcionalmente pode vir pintada na cor preto brilhante.</p>

                    <h4>Vista traseira</h4>

                    <p>O lado jovem do Gol é valorizado pelo opcional aerofólio traseiro pintado na cor do carro, ao qual o brake light é integrado.</p>

                    <h4>O Gol</h4>

                    <p>A boa dirigibilidade do Gol está presente até em sua versão mais econômica, a 1.0. À economia do motor 1.0 se aliam elementos de estilo que enaltecem o espírito esportivo que este carro oferece aos que o elegeram o automóvel mais querido do Brasil.</p>

                    <h4>Rodas</h4>

                    <p>Design de novas rodas para todas as versões. Rodas escurecidas aro 16 para versões Power. Design de logotipo VW novo e sofisticado.</p>

                    <p>www.consorciovolkswagen.com.br</p>

                </div>

            </div>

        </div>

        

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none"> 

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                <h2><i class="blue-consorcio fa fa-angle-right"></i> Informações sobre o plano Normal</h2>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">PRAZO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>60 Meses</strong></p>

                    <p class="linha-azul-bottom">GRUPO DE CONSÓRCIO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>NORMAL</strong></p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">CATEGORIA</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>CARROS312</strong></p>

                    <p class="linha-azul-bottom">CRÉDITO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>R$ 37.890,00</strong></p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">TIPO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>Pessoa Física</strong></p>

                    <p class="linha-azul-bottom">PARCELAS MENSAIS</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>R$ 762,69</strong></p>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none linha-azul-bottom-2x">

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                    <h3>Dados do Plano</h3>

                    <p>Adquira seu volkswagen com segurança e tranquilidade de quem tem mais de 30 anos de tradição no mercado.</p>

                    <p>Plano Gold:</p>

                    <p>Prazo de Duração: 60 meses</p>

                    <p>% Contribuição Mensal: Série M / Normal - 1,6667%</p>

                    <p>% Taxa de Administração Total: Antecipada: - / Deferida: 15,5%</p>

                    <p>Plano Normal (Série M): 120 Participantes</p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                    <h3>Conheça alguns itens de série e opcionais disponíveis para o Novo Gol - Consulte a versão</h3>

                    <h4>Vista frontal</h4>

                    <p>A grade de tomada de ar, mais afilada e com três lâminas, dá destaque à logomarca Volkswagen e se une de maneira harmoniosa ao conjunto. Opcionalmente pode vir pintada na cor preto brilhante.</p>

                    <h4>Vista traseira</h4>

                    <p>O lado jovem do Gol é valorizado pelo opcional aerofólio traseiro pintado na cor do carro, ao qual o brake light é integrado.</p>

                    <h4>O Gol</h4>

                    <p>A boa dirigibilidade do Gol está presente até em sua versão mais econômica, a 1.0. À economia do motor 1.0 se aliam elementos de estilo que enaltecem o espírito esportivo que este carro oferece aos que o elegeram o automóvel mais querido do Brasil.</p>

                    <h4>Rodas</h4>

                    <p>Design de novas rodas para todas as versões. Rodas escurecidas aro 16 para versões Power. Design de logotipo VW novo e sofisticado.</p>

                    <p>www.consorciovolkswagen.com.br</p>

                </div>

            </div>

        </div>

    </div>

    

</div>