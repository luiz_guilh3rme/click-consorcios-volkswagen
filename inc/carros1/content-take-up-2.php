<div class="container">

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-bottom ">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                <img class="img-carros" src="../../img/carros/up.jpg" />

            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                <h2><i class="blue-consorcio fa fa-angle-right"></i> Take UP! - 2 Portas</h2>

                <p>Se esta é sua opção ideal agora falta muito pouco para que você torne seu sonho uma realidade, conheça os detalhes do modelo e prossiga preenchendo o formulário com seus dados para iniciar a primeira fase de sua adesão ao grupo.</p>

                <p><img class="img-esquerda-carro" src="../../img/consorcio-volks-help.png" style="float: left;" />Se você possuir alguma dúvida poderá ainda consultar nosso <a href="../../faq.php" target="_blank">FAQ</a> ou utilizar uma consulta através dos meios de atendimento disponíveis, como: chat, e-mail ou telefone.</p>

                <p><i class="blue-consorcio fa fa-angle-left"></i> <a href="../../planos.php">Retornar aos planos</a></p>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                        <a class="btn-orcamento" href="#puxada-orcamento">

                            <span class="color-btn-orcamento1"><p>Compre esse plano</p></span>

                            <span class="color-btn-orcamento2"><p>Parcelas de a partir de</p></span>

                            <span class="color-btn-orcamento3"><p><span>R$</span>363,68</p></span>

                        </a>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                        <a class="btn-orcamento2" rel="modal" onclick="javascript:location='#janela1';">

                            <img src="../../img/consorcio-volkswagen-fale-conosco.png" />

                            <span class="color-btn-meliga1"><p>Nos te ligamos</p></span>

                            <span class="color-btn-meliga2"><p>O Consórcio Volkswagen entra em contato com você, basta um click!</p></span>

                            <p class="p-btn-orcamento"><span class="color-btn-meliga3">Volks, me liga!</span></p>

                        </a>

                    </div>

                </div>

            </div>

        </div>

        

        

        

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none"> 

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                <h2><i class="blue-consorcio fa fa-angle-right"></i> Informações sobre o plano Mais Leve</h2>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">PRAZO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>72 Meses</strong></p>

                    <p class="linha-azul-bottom">GRUPO DE CONSÓRCIO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>MAIS LEVE</strong></p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">CATEGORIA</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>CARROS260</strong></p>

                    <p class="linha-azul-bottom">CRÉDITO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>R$ 30.990,00</strong></p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">TIPO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>Pessoa Física</strong></p>

                    <p class="linha-azul-bottom">PARCELAS MENSAIS</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>R$ 363,68</strong></p>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none linha-azul-bottom-2x">

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                    <h3>Dados do Plano</h3>

                    <p>Adquira seu volkswagen com segurança e tranquilidade de quem tem mais de 30 anos de tradição no mercado.</p>

                    <p>Plano Gold:</p>

                    <p>Prazo de Duração: 72 meses</p>

                    <p>% Contribuição Mensal: Série U / Mais Leve - 0,9259%</p>

                    <p>% Taxa de Administração Total: Antecipada: - / Deferida: 17,5%</p>

                    <p>Plano Mais Leve (Série U): 432 Participantes</p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                    <h3>Novo Volkswagen UP!</h3>

                    <p>O nome já diz tudo: com o novo up!, você terá muitos motivos para sorrir porque ele é diferente de tudo o que você já viu. Um carro democrático, com visual moderno e divertido, acessível a todos e, acima de tudo, um autêntico Volkswagen, com toda a segurança e robustez que só a marca pode oferecer. Nunca o DNA da marca foi reinterpretado de uma forma tão descontraída.</p>

                    <p>Com design clean e moderno, o up! esbanja simpatia, e o belo sorriso na parte frontal é só o ponto de partida para atrair o que a vida tem de melhor. E se a aparência chama a atenção, o interior não fica atrás: aqui, tudo foi projetado para o seu conforto com ergonomia.</p>

                    <p>A diversão é garantida por um conjunto de tecnologias que vão dar um up na sua rotina, trazendo a bordo o que existe de melhor em entretenimento. Além de tudo, o up! vem equipado com uma nova geração de motor que o torna o 1.0 mais leve, potente e econômico da categoria. É também o primeiro veículo produzido no Brasil a atingir 5 estrelas (nota máxima) em segurança para adultos e 4 estrelas para crianças pelo Latin NCAP, entidade especializada em segurança automotiva da América Latina.</p>

                    <p>Prepare-se para ficar com a energia em alta, ganhar a cidade e dar um up na sua vida. Afinal, atrás de um up!, sempre vêm outros ups. Divirta-se escolhendo o seu!</p>

                </div>

            </div>

        </div>

        

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none"> 

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                <h2><i class="blue-consorcio fa fa-angle-right"></i> Informações sobre o plano Leve</h2>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">PRAZO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>60 Meses</strong></p>

                    <p class="linha-azul-bottom">GRUPO DE CONSÓRCIO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>LEVE</strong></p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">CATEGORIA</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>CARROS281</strong></p>

                    <p class="linha-azul-bottom">CRÉDITO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>R$ 30.990,00</strong></p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">TIPO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>Pessoa Física</strong></p>

                    <p class="linha-azul-bottom">PARCELAS MENSAIS</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>R$ 474,02</strong></p>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none linha-azul-bottom-2x">

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                    <h3>Dados do Plano</h3>

                    <p>Adquira seu volkswagen com segurança e tranquilidade de quem tem mais de 30 anos de tradição no mercado.</p>

                    <p>Plano Gold:</p>

                    <p>Prazo de Duração: 60 meses</p>

                    <p>% Contribuição Mensal: Série O / Leve - 1,2500%</p>

                    <p>% Taxa de Administração Total: Antecipada: - / Deferida: 15,5%</p>

                    <p>Plano Leve (Série O): 240 Participantes</p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                    <h3>Novo Volkswagen UP!</h3>

                    <p>O nome já diz tudo: com o novo up!, você terá muitos motivos para sorrir porque ele é diferente de tudo o que você já viu. Um carro democrático, com visual moderno e divertido, acessível a todos e, acima de tudo, um autêntico Volkswagen, com toda a segurança e robustez que só a marca pode oferecer. Nunca o DNA da marca foi reinterpretado de uma forma tão descontraída.</p>

                    <p>Com design clean e moderno, o up! esbanja simpatia, e o belo sorriso na parte frontal é só o ponto de partida para atrair o que a vida tem de melhor. E se a aparência chama a atenção, o interior não fica atrás: aqui, tudo foi projetado para o seu conforto com ergonomia.</p>

                    <p>A diversão é garantida por um conjunto de tecnologias que vão dar um up na sua rotina, trazendo a bordo o que existe de melhor em entretenimento. Além de tudo, o up! vem equipado com uma nova geração de motor que o torna o 1.0 mais leve, potente e econômico da categoria. É também o primeiro veículo produzido no Brasil a atingir 5 estrelas (nota máxima) em segurança para adultos e 4 estrelas para crianças pelo Latin NCAP, entidade especializada em segurança automotiva da América Latina.</p>

                    <p>Prepare-se para ficar com a energia em alta, ganhar a cidade e dar um up na sua vida. Afinal, atrás de um up!, sempre vêm outros ups. Divirta-se escolhendo o seu!</p>

                </div>

            </div>

        </div>

        

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none"> 

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                <h2><i class="blue-consorcio fa fa-angle-right"></i> Informações sobre o plano Normal</h2>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">PRAZO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>60 Meses</strong></p>

                    <p class="linha-azul-bottom">GRUPO DE CONSÓRCIO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>NORMAL</strong></p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">CATEGORIA</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>CARROS305</strong></p>

                    <p class="linha-azul-bottom">CRÉDITO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>R$ 30.990,00</strong></p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">TIPO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>Pessoa Física</strong></p>

                    <p class="linha-azul-bottom">PARCELAS MENSAIS</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>R$ 623,80</strong></p>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none linha-azul-bottom-2x">

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                    <h3>Dados do Plano</h3>

                    <p>Adquira seu volkswagen com segurança e tranquilidade de quem tem mais de 30 anos de tradição no mercado.</p>

                    <p>Plano Gold:</p>

                    <p>Prazo de Duração: 60 meses</p>

                    <p>% Contribuição Mensal: Série M / Normal - 1,6667%</p>

                    <p>% Taxa de Administração Total: Antecipada: - / Deferida: 15,5%</p>

                    <p>Plano Normal (Série M): 120 Participantes</p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                    <h3>Novo Volkswagen UP!</h3>

                    <p>O nome já diz tudo: com o novo up!, você terá muitos motivos para sorrir porque ele é diferente de tudo o que você já viu. Um carro democrático, com visual moderno e divertido, acessível a todos e, acima de tudo, um autêntico Volkswagen, com toda a segurança e robustez que só a marca pode oferecer. Nunca o DNA da marca foi reinterpretado de uma forma tão descontraída.</p>

                    <p>Com design clean e moderno, o up! esbanja simpatia, e o belo sorriso na parte frontal é só o ponto de partida para atrair o que a vida tem de melhor. E se a aparência chama a atenção, o interior não fica atrás: aqui, tudo foi projetado para o seu conforto com ergonomia.</p>

                    <p>A diversão é garantida por um conjunto de tecnologias que vão dar um up na sua rotina, trazendo a bordo o que existe de melhor em entretenimento. Além de tudo, o up! vem equipado com uma nova geração de motor que o torna o 1.0 mais leve, potente e econômico da categoria. É também o primeiro veículo produzido no Brasil a atingir 5 estrelas (nota máxima) em segurança para adultos e 4 estrelas para crianças pelo Latin NCAP, entidade especializada em segurança automotiva da América Latina.</p>

                    <p>Prepare-se para ficar com a energia em alta, ganhar a cidade e dar um up na sua vida. Afinal, atrás de um up!, sempre vêm outros ups. Divirta-se escolhendo o seu!</p>

                </div>

            </div>

        </div>

    </div>

    

</div>