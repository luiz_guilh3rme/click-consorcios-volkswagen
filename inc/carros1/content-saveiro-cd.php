<div class="container">

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-bottom ">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                <img class="img-carros" src="../../img/carros/saveiro.jpg" />

            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                <h2><i class="blue-consorcio fa fa-angle-right"></i> SAVEIRO 1.6 CD</h2>

                <p>Se esta é sua opção ideal agora falta muito pouco para que você torne seu sonho uma realidade, conheça os detalhes do modelo e prossiga preenchendo o formulário com seus dados para iniciar a primeira fase de sua adesão ao grupo.</p>

                <p><img class="img-esquerda-carro" src="../../img/consorcio-volks-help.png" style="float: left;" />Se você possuir alguma dúvida poderá ainda consultar nosso <a href="../../faq.php" target="_blank">FAQ</a> ou utilizar uma consulta através dos meios de atendimento disponíveis, como: chat, e-mail ou telefone.</p>

                <p><i class="blue-consorcio fa fa-angle-left"></i> <a href="../../planos.php">Retornar aos planos</a></p>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                        <a class="btn-orcamento" href="#puxada-orcamento">

                            <span class="color-btn-orcamento1"><p>Compre esse plano</p></span>

                            <span class="color-btn-orcamento2"><p>Parcelas de a partir de</p></span>

                            <span class="color-btn-orcamento3"><p><span>R$</span>633,59</p></span>

                        </a>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                        <a class="btn-orcamento2" rel="modal" onclick="javascript:location='#janela1';">

                            <img src="../../img/consorcio-volkswagen-fale-conosco.png" />

                            <span class="color-btn-meliga1"><p>Nos te ligamos</p></span>

                            <span class="color-btn-meliga2"><p>O Consórcio Volkswagen entra em contato com você, basta um click!</p></span>

                            <p class="p-btn-orcamento"><span class="color-btn-meliga3">Volks, me liga!</span></p>

                        </a>

                    </div>

                </div>

            </div>

        </div>

        

        

        

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none"> 

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                <h2><i class="blue-consorcio fa fa-angle-right"></i> Informações sobre o plano Mais Leve</h2>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">PRAZO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>72 Meses</strong></p>

                    <p class="linha-azul-bottom">GRUPO DE CONSÓRCIO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>MAIS LEVE</strong></p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">CATEGORIA</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>CARROS277</strong></p>

                    <p class="linha-azul-bottom">CRÉDITO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>R$ 53.990,00</strong></p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">TIPO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>Pessoa Física</strong></p>

                    <p class="linha-azul-bottom">PARCELAS MENSAIS</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>R$ 633,59</strong></p>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none linha-azul-bottom-2x">

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                    <h3>Dados do Plano</h3>

                    <p>Adquira seu volkswagen com segurança e tranquilidade de quem tem mais de 30 anos de tradição no mercado.</p>

                    <p>Plano Gold:</p>

                    <p>Prazo de Duração: 72 meses</p>

                    <p>% Contribuição Mensal: Série U / Mais Leve - 0,9259%</p>

                    <p>% Taxa de Administração Total: Antecipada: - / Deferida: 17,5%</p>

                    <p>Plano Mais Leve (Série U): 432 Participantes</p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                    <h3>Saveiro 2015</h3>

                    <p>Tudo foi pensado visando o desempenho, sem deixar de lado o conforto e estilo. Desde os faróis com máscara negra até a lanterna traseira de neblina, reúne funcionalidade com um visual de quem está pronto para encarar todos os desafios. Nela cabe a aventura que você imaginar.</p>

                    <h4>Logotipo cromado "Cross"</h4>

                    <p>No lado direito da grade do radiador, o logotipo “CROSS” cromado reforça sua valentia.</p>

                    <p>Lanternas traseiras com lente escurecida e lanterna de neblina .Destacam sua presença e proporcionam mais segurança.</p>

                    <h4>Maçanetas</h4>

                    <p>Com as maçanetas na cor da pick-up, as laterais da Saveiro Cross ficaram ainda mais bonitas e suas linhas mais fluentes</p>

                    <h4>Rodas de liga-leve 15” e pneus 205/60 R15 – “All Terrain”</h4>

                    <p>Molduras alargadas das caixas de roda destacam mais esportividade e robustez. Santantonio com função de aerofólio e braços estendidos sobre as laterais da caçamba. Braços estendidos sobre a lateral da caçamba. Proteção, conforto e um visual moderno.</p>

                    <p>(*) Consulte as versões - informações institucionais para alguns modelos.</p>

                </div>

            </div>

        </div>

        

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none"> 

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                <h2><i class="blue-consorcio fa fa-angle-right"></i> Informações sobre o plano Leve</h2>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">PRAZO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>60 Meses</strong></p>

                    <p class="linha-azul-bottom">GRUPO DE CONSÓRCIO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>LEVE</strong></p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">CATEGORIA</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>CARROS297</strong></p>

                    <p class="linha-azul-bottom">CRÉDITO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>R$ 53.990,00</strong></p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">TIPO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>Pessoa Física</strong></p>

                    <p class="linha-azul-bottom">PARCELAS MENSAIS</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>R$ 817,86</strong></p>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none linha-azul-bottom-2x">

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                    <h3>Dados do Plano</h3>

                    <p>Adquira seu volkswagen com segurança e tranquilidade de quem tem mais de 30 anos de tradição no mercado.</p>

                    <p>Plano Gold:</p>

                    <p>Prazo de Duração: 60 meses</p>

                    <p>% Contribuição Mensal: Série O / Leve - 1,2500%</p>

                    <p>% Taxa de Administração Total: Antecipada: - / Deferida: 15,5%</p>

                    <p>Plano Leve (Série O): 240 Participantes</p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                    <h3>Saveiro 2015</h3>

                    <p>Tudo foi pensado visando o desempenho, sem deixar de lado o conforto e estilo. Desde os faróis com máscara negra até a lanterna traseira de neblina, reúne funcionalidade com um visual de quem está pronto para encarar todos os desafios. Nela cabe a aventura que você imaginar.</p>

                    <h4>Logotipo cromado "Cross"</h4>

                    <p>No lado direito da grade do radiador, o logotipo “CROSS” cromado reforça sua valentia.</p>

                    <p>Lanternas traseiras com lente escurecida e lanterna de neblina .Destacam sua presença e proporcionam mais segurança.</p>

                    <h4>Maçanetas</h4>

                    <p>Com as maçanetas na cor da pick-up, as laterais da Saveiro Cross ficaram ainda mais bonitas e suas linhas mais fluentes</p>

                    <h4>Rodas de liga-leve 15” e pneus 205/60 R15 – “All Terrain”</h4>

                    <p>Molduras alargadas das caixas de roda destacam mais esportividade e robustez. Santantonio com função de aerofólio e braços estendidos sobre as laterais da caçamba. Braços estendidos sobre a lateral da caçamba. Proteção, conforto e um visual moderno.</p>

                    <p>(*) Consulte as versões - informações institucionais para alguns modelos.</p>

                </div>

            </div>

        </div>

        

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none"> 

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                <h2><i class="blue-consorcio fa fa-angle-right"></i> Informações sobre o plano Normal</h2>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">PRAZO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>60 Meses</strong></p>

                    <p class="linha-azul-bottom">GRUPO DE CONSÓRCIO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>NORMAL</strong></p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">CATEGORIA</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>CARROS302</strong></p>

                    <p class="linha-azul-bottom">CRÉDITO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>R$ 53.990,00</strong></p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">TIPO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>Pessoa Física</strong></p>

                    <p class="linha-azul-bottom">PARCELAS MENSAIS</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>R$ 1.086,77</strong></p>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none linha-azul-bottom-2x">

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                    <h3>Dados do Plano</h3>

                    <p>Adquira seu volkswagen com segurança e tranquilidade de quem tem mais de 30 anos de tradição no mercado.</p>

                    <p>Plano Gold:</p>

                    <p>Prazo de Duração: 60 meses</p>

                    <p>% Contribuição Mensal: Série M / Normal - 1,6667%</p>

                    <p>% Taxa de Administração Total: Antecipada: - / Deferida: 15,5%</p>

                    <p>Plano Normal (Série M): 120 Participantes</p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                    <h3>Saveiro 2015</h3>

                    <p>Tudo foi pensado visando o desempenho, sem deixar de lado o conforto e estilo. Desde os faróis com máscara negra até a lanterna traseira de neblina, reúne funcionalidade com um visual de quem está pronto para encarar todos os desafios. Nela cabe a aventura que você imaginar.</p>

                    <h4>Logotipo cromado "Cross"</h4>

                    <p>No lado direito da grade do radiador, o logotipo “CROSS” cromado reforça sua valentia.</p>

                    <p>Lanternas traseiras com lente escurecida e lanterna de neblina .Destacam sua presença e proporcionam mais segurança.</p>

                    <h4>Maçanetas</h4>

                    <p>Com as maçanetas na cor da pick-up, as laterais da Saveiro Cross ficaram ainda mais bonitas e suas linhas mais fluentes</p>

                    <h4>Rodas de liga-leve 15” e pneus 205/60 R15 – “All Terrain”</h4>

                    <p>Molduras alargadas das caixas de roda destacam mais esportividade e robustez. Santantonio com função de aerofólio e braços estendidos sobre as laterais da caçamba. Braços estendidos sobre a lateral da caçamba. Proteção, conforto e um visual moderno.</p>

                    <p>(*) Consulte as versões - informações institucionais para alguns modelos.</p>

                </div>

            </div>

        </div>

    </div>

    

</div>