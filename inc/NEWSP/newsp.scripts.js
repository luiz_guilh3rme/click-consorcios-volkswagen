/* ============================================== *
 * Name: NewsP Front End Plugin                   *
 * Description: Plugin de integração com o NewsP  *
 * Data: 2015 / 08 / 15                           *
 * ============================================== */

if(!jQuery) throw 'Por favor tenha certeza de ter inserido o jQuery antes de chamar o NewsP';

// Carrega formulários
function nWatchForm(form, options) {

    // Apply EventListeners
    if(form.length < 1) return; //throw 'Erro ao decodificar formulário.';

    // Set Event Lister on Submit
    jQuery(form).on('submit', prepareForm);

    if(options.validate) {
        jQuery(form).on('submited', function (data) {
            var result = data.originalEvent.detail;

            if(result.status !== '200'){
                jQuery('.newsp-label').remove();
                jQuery('<span class="newsp-label newsp-error-message">'+result.message+'</span>').insertAfter(form);
            }else{
                jQuery('.newsp-label').remove();
                jQuery('<span class="newsp-label newsp-success-message">Seu email foi cadastrado!</span>').insertAfter(form);
            }
        });
    }

}

function prepareForm (formEvt) {
    formEvt.preventDefault();

    var current = formEvt.currentTarget;
    var parsedForm = jQuery(current).serialize();

    jQuery.ajax({
        // DEPENDENDO DA VERSÃO DO JQUERY PODE VARIAR O TYPE ou o METHOD
        type: 'POST',
        url: globalPath + '/Newsp.send.php',
        data: parsedForm
    }).done(function(data) {
        var result = JSON.parse(data);

        // App
        var event = new CustomEvent("submited", {
            detail: result,
            bubbles: true,
            cancelable: true
        });

        current.dispatchEvent(event);
    });
}