<?php

    // Define sua PRIVATE KEY
    $priv = '6512bd43d9caa6e02c990b0a82652dca';

    if(isset($_POST) && !empty($_POST)){
        if(!isset($_POST['email'])) return;

        $data = convertToDTO($_POST);
        $data['tipo99'] = $priv;

        // SetUp Post
        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data),
            ),
        );

        $context  = stream_context_create($options);
        $result = file_get_contents('http://newsp.3xceler.com.br/index.php', false, $context);

        die($result);
    }

    function convertToDTO($obj) {

        // Prepara dados
        $dados = array(
            'nome99' 	=> null,
            'email99' 	=>  null,
            'telefone99'=> null
        );

        // Set It
        if(isset($obj['nome'])) $dados['nome99'] = $obj['nome'];
        if(isset($obj['email'])) $dados['email99'] = $obj['email'];
        if(isset($obj['telefone'])) $dados['telefone99'] = $obj['telefone'];

        return $dados;
    }
?>