<form action="" class="submit-main-cta form_01 cleared" data-modal="simular">
    <label class="field-wrapper half">
    <input type="hidden" name="identifier" value="Adquira seu Voyage">
        <input type="text" class="field_01" name="name" placeholder="Nome Completo">
        <input type="hidden" name="bulldesk-client" value="">
    </label>
    <label class="field-wrapper half">
        <input type="email" class="field_01" name="email" placeholder="E-mail">
    </label>
    <label class="field-wrapper half">
        <input type="text" class="field_01" name="cellphone" placeholder="(DDD) Celular">
    </label>
    <label class="field-wrapper half">
        <input type="text" class="field_01" name="phone" placeholder="(DDD) Telefone">
    </label>
    <label class="field-wrapper half">
        <select class="field_01" name="user-select">
            <option disabled selected> Selecione um Modelo </option>
            <option value="Voyage 1.0">Voyage 1.0</option>
            <option value="Voyage 1.6">Voyage 1.6</option>
            <option value="Voyage 1.6 confortline">Voyage 1.6 Confortline</option>
            <option value="Voyage 1.6 hihgline">Voyage 1.6 Highline</option>
     </select>
    </label>
    <label class="field-wrapper half">
        <input type="number" class="field_01" name="user-credit" placeholder="Crédito desejado">
    </label>
    <button class="submit_01_form">ENVIAR</button>
</form>