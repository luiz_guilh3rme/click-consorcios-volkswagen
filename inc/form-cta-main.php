<form action="" class="submit-main-cta form_01 cleared" data-modal="simular">
    <label class="field-wrapper half">
        <input type="hidden" name="identifier" value="Adquira seu Volks">
        <input required type="text" class="field_01" name="name" placeholder="Nome Completo">
        <input type="hidden" name="bulldesk-client" value="">
    </label>
    <label class="field-wrapper half">
        <input required type="email" class="field_01" name="email" placeholder="E-mail">
    </label>
    <label class="field-wrapper half">
        <input required type="text" class="field_01" name="cellphone" placeholder="(DDD) Celular">
    </label>
    <label class="field-wrapper half">
        <input required type="text" class="field_01" name="phone" placeholder="(DDD) Telefone">
    </label>
    <label class="field-wrapper half">
        <select required class="field_01" name="user-select">
            <option disabled selected> Selecione um Modelo </option>
            <option value="------CARROS------" disabled> ------CARROS------ </option>
            <option value="Polo 1.0"> Polo 1.0 </option>
            <option value="Polo MSI 1.6"> Polo MSI 1.6 </option>
            <option value="Polo Highline 200 TSI"> Polo Highline 200 TSI </option>
            <option value="Amarok Cabine Simples"> Amarok Cabine Simples </option>
            <option value="Amarok Cabine Dupla"> Amarok Cabine Dupla </option>
            <option value="Golf Variant Comfortline"> Golf Variant Comfortline </option>
            <option value="Golf Highline 1.4"> Golf Highline 1.4 </option>
            <option value="Golf TSI 1.0"> Golf TSI 1.0 </option>
            <option value="Golf Highline 1.4 Aut"> Golf Highline 1.4 Aut </option>
            <option value="Golf GTI 2.0"> Golf GTI 2.0 </option>
            <option value="Jetta Highline 2.0"> Jetta Highline 2.0 </option>
            <option value="Tiguan 1.4"> Tiguan 1.4 </option>
            <option value="Take UP! - 4 Portas"> Take UP! - 4 Portas </option>
            <option value="Voyage 1.0"> Voyage 1.0 </option>
            <option value="Voyage 1.6 I-Motion"> Voyage 1.6 I-Motion </option>
            <option value="Voyage 1.6"> Voyage 1.6 </option>
            <option value="Gol 1.6 "> Gol 1.6 </option>
            <option value="Gol 1.0 "> Gol 1.0 </option>
            <option value="Virtus 1.6 MSI"> Virtus 1.6 MSI </option>
            <option value="Virtus Highline"> Virtus Highline </option>
            <option value="Delivery Express"> Delivery Express </option>
            <option value="------MOTOS------" disabled> ------MOTOS------ </option>
            <option value="Scrambler"> Scrambler </option>
        </select>
    </label>
    <label class="field-wrapper half">
        <input type="number" required class="field_01" name="user-credit" placeholder="Crédito desejado">
    </label>
    <button class="submit_01_form" type="submit">ENVIAR</button>
</form>