<?php

$arrParams = explode("/",$_SERVER["REQUEST_URI"]);

$page = $arrParams[2];

//$page = str_replace(".php","",$page);

?>
<div id="puxada-orcamento" class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-bottom-15x linha-azul-bottom-2x">
        <h2 class="orcamento-title"><i class="blue-consorcio fa fa-angle-right"></i>Clique para preencher o formulário de adesão</h2>
        <form action="#" method="post" id="orcamento" class="orcamento" novalidate="novalidate">

            <!-- NewsP -->
            <input type="hidden" name="url" value="<?= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">
            <!-- //NewsP -->

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none margin-bottom">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <label>Nome ou Razão:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <input type="text" oninvalid="this.setCustomValidity('Digite o nome'); console.log('Erro')" name="nome" id="nome" required="" aria-required="true" placeholder="Nome">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <label>CPF/CNPJ:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <input type="text" name="cpnj" id="cpnj" required="" aria-required="true" placeholder="CPF / CNPJ">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <label>RG ou IE:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <input type="text" name="rgie" id="rgie" required="" aria-required="true" placeholder="RG / IE">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <label>Orgão Emissor:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <input type="text" name="orgao" id="orgao" required="" aria-required="true" placeholder="Orgão Emissor">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <label>Nascimento:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <input type="text" name="nascimento" id="nascimento" required="" aria-required="true" placeholder="Nascimento">
                    </div>
                </div>
				   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <label>Sexo:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <select name="sexo" class="frm_view"> 
                            <option value="" selected disabled>Selecione</option>
                            <option value="MASCULINO">MASCULINO</option>
                            <option value="FEMININO">FEMININO</option>
                        </select>
                    </div>
                </div>
				   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <label>Estado Civil:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <select name="estado_civil" class="frm_view">
                            <option value="" selected disabled>Selecione</option>
                            <option value="SOLTEIRO">SOLTEIRO</option>
                            <option value="CASADO">CASADO</option>
                            <option value="UNIÃO ESTÁVEL">UNIÃO ESTÁVEL</option>
                            <option value="VIUVO">VIUVO</option>
                            <option value="DIVORCIADO">DIVORCIADO</option>
                            <option value="OUTROS">OUTROS</option>
                        </select>
                    </div>
                </div>
				
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <label>Telefone(RES):</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <input type="text" name="telefone" id="telefone" aria-required="true" placeholder="22 55555 4444">
                    </div>
                </div>
   
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <label>Telefone(COM):</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <input type="text" name="telefonecom" id="telefonecom" aria-required="true" placeholder="22 55555 4444">
                    </div>
                </div>
				             <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <label>Celular:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <input type="text" name="celular" id="celular" aria-required="true" placeholder="22 55555 4444">
                    </div>
                </div>
				
				    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <label>Seu e-mail:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <input type="email" name="email" id="email" required="" aria-required="true" placeholder="E-mail">
                    </div>
                </div>
				
				  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <label>Validação:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <?php
                            $min = 1111111; 
                            $max = 9999999; 
                            $gera = rand($min,$max); 
                        ?>
                        <label class="label-grande"><?php echo $gera ?></label>
                    </div>
                </div>
           
             
         

            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none margin-bottom">
                     <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <label>CEP:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <input type="text" name="cep" id="cep" required="" aria-required="true" placeholder="CEP">
                    </div>
                </div>
				
				   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <label>Endereço:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <input type="text" name="endereco" id="endereco" required="" aria-required="true" placeholder="Endereço">
                    </div>
                </div>
				
				       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <label>Número:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <input type="text" name="numero" id="numero" required="" aria-required="true" placeholder="Numero">
                    </div>
                </div>
				 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <label>Complemento:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <input type="text" name="complemento" id="complemento" aria-required="true" placeholder="Complemento">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <label>Bairro:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <input type="text" name="bairro" id="bairro" aria-required="true" placeholder="Bairro">
                    </div>
                </div>
				
				
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <label>Cidade:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <input type="text" name="cidade" id="cidade" aria-required="true" placeholder="Cidade">
                    </div>
                </div>
               
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <label>Estado:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <select name="uf" class="frm_view"> 
                            <option selected disabled>Selecione</option>
                            <option value="AC">Acre</option>
                            <option value="AL">Alagoas</option>
                            <option value="AM">Amazonas</option>
                            <option value="AP">Amapá</option>
                            <option value="BA">Bahia</option>
                            <option value="CE">Ceará</option>
                            <option value="DF">Distrito Federal</option>
                            <option value="ES">Espírito Santo</option>
                            <option value="GO">Goiás</option>
                            <option value="MA">Maranhão</option>
                            <option value="MG">Minas Gerais</option>
                            <option value="MS">Mato Grosso do Sul</option>
                            <option value="MT">Mato Grosso</option>
                            <option value="PA">Pará</option>
                            <option value="PB">paraíba</option>
                            <option value="PE">Pernambuco</option>
                            <option value="PI">Piauí</option>
                            <option value="PR">Paraná</option>
                            <option value="RJ">Rio de Janeiro</option>
                            <option value="RN">Rio Grande do Norte</option>
                            <option value="RO">Rondônia</option>
                            <option value="RR">Roraima</option>
                            <option value="RS">Rio Grande do Sul</option>
                            <option value="SC">Santa Catarina</option>
                            <option value="SE">Sergipe</option>
                            <option value="SP">São Paulo</option>
                            <option value="TO">Tocantins</option>
                        </select>
                    </div>
                </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <label>Profissão:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">

						<input type="text" name="formacao" id="formacao" required="" aria-required="true" placeholder="Profissão">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <label>Renda Mensal:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <input type="text" name="renda" id="renda" required="" aria-required="true" placeholder="Renda Mensal">
                    </div>
                </div>
             
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <label>Plano:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <select name="plano" class="frm_view">
                            <option value="" selected disabled>Selecione um plano</option>
                            <option value="PLANO LEVE">PLANO LEVE</option>
                            <option value="PLANO NORMAL">PLANO NORMAL</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <label>Modelo:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <select name="modelo" id="modelo">
							<option value="" selected disabled> Selecione o modelo </option>
							
							<?php
							$sqlSelect = "SELECT * FROM volks_auto";
							$resultSelect = mysql_query($sqlSelect);
							while($rowSelect = mysql_fetch_assoc($resultSelect)){
								if($rowSelect["auto_slug"] == $page){
									$sel = 'selected="true"';
								}else{
									$sel = "";
								}
							?>
                            <option value="<?=utf8_encode($rowSelect["auto_nome"]) ?>" <?=$sel ?>><?=utf8_encode($rowSelect["auto_nome"]) ?></option>
							
							<?php
							}
							?>
                        

                        </select>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none margin-bottom">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                    <p>Através da proposta manifesto a intenção de participar de grupo de consórcio, declarando serem verdadeiras todas as informações prestadas.</p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <label>Digite o número que está vendo acima:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-none">
                        <input type="text" name="codigo">
                        <input type="hidden" name="cod-h" id="cod-h" value="<?php echo $gera ?>"/>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none margin-bottom">
                <input type="hidden" name="tipo4" id="tipo4" value="orcamento"/>
                <input type="submit" class="buttomcompra" value="Enviar proposta de Adesão">
            </div>
        </form>

<!-- Target Mais Publicidade -->
<script src="https://static.bulldesk.com.br/forms.js"></script>
<script>
  new BulldeskFormIntegration({
    token: 'a9c3fe612299f9cad1ab2a8dfef76e46',
    identifier: 'ConsorcioVolks'
  });
</script>


    </div>
    <div  class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-bottom-15x ">
        <p>O Consórcio Nacional Volkswagen é o único garantido pela própria Volkswagen. Frete incluso. Pintura metálica e opcionais não inclusos. Planos sem taxa de adesão. Seguro de vida incluso no valor da prestação. Os modelos, códigos e valores estão sujeitos a alterações conforme a política de comercialização da fábrica. Crédito sujeito à aprovação na contemplação. Imagens meramente ilustrativas.</p>
    </div>
</div>