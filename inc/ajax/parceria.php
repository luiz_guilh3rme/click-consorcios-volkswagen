<?php 

require '../PHPMailer-master/PHPMailerAutoload.php';

if ( !empty($_POST) ) {

    $identifier = trim ($_POST["identifier"]);
    $name = trim( $_POST["nome"] );
    $cep = trim($_POST["cep"]); 
    $endereco = trim($_POST["endereco"]);
    $numero = trim($_POST["numero"]);
    $complemento = trim($_POST["complemento"]);
    $bairro = trim($_POST["bairro"]);
    $cidade = trim($_POST["cidade"]);
    $estado = trim($_POST["estado"]);
    $nascimento = trim($_POST["nascimento"]);
    $telefone = trim($_POST["telefone"]);
    $celular = trim($_POST["celular"]);
    $email = trim( $_POST["email"] );
	$msg = trim( $_POST["msg"] );
	

	$mail = new PHPMailer(true); 
	$mail->CharSet = 'UTF-8';
	$mail->Subject = ("Nós te Ligamos - Consórcio VW");
	$mail->msgHTML("<html>
		<body>
		<h1 style='font-family: Helvetica, sans-serif; font-size: 32px; line-height: 36px;'> {$identifier} </h1>
		<p style='font-family: Helvetica, sans-serif; font-size: 18px; line-height: 22px;'>  
		Nome: {$name}
		<br/>
		Email: {$email}
		<br/>
		Telefone: {$telefone}
        <br/> 
        Celular : {$celular}
        <br/>>
        Data de Nascimento : {$nascimento}  
        <br/>
        Mensagem : {$msg}
        <br/>
        Endereço : {$endereco}
        <br/>
        Bairro : {$bairro}
        <br/>
        Número : {$numero}
        <br/>
        Complemento : {$complemento}
        <br/>
        Cidade : {$cidade}
        <br/>
        Estado : {$estado}
        <br/> 
		</p>
		</body>
		</html>");
	$mail->SetFrom('consorciovw@serv3xceler.com.br', 'Simulação de Orçamento - Consórcio VW');
	$mail->addCC( 'andre.facchini@3xceler.com.br', 'André Facchini');


	if ($mail->send()) {
		echo 'Mensagem enviada com sucesso!';
	}  else {
		echo 'Erro o enviar a mensagem' . $mail->ErrorInfo;
	}
}
else {
	echo 'Ops!';
}
die;