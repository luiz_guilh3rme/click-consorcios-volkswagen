<?php 

require '../PHPMailer-master/PHPMailerAutoload.php';

if ( !empty($_POST) ) {

    $identifier = trim ($_POST["identifier"]);
	$name = trim( $_POST["nome"] );
    $email = trim( $_POST["email"] );
    $phone = trim( $_POST["telefone"] );
    $subj = trim($_POST["assunto"]);
	$msg = trim( $_POST["msg"] );
	

	$mail = new PHPMailer(true); 
	$mail->CharSet = 'UTF-8';
	$mail->Subject = ("Nós te Ligamos - Consórcio VW");
	$mail->msgHTML("<html>
		<body>
		<h1 style='font-family: Helvetica, sans-serif; font-size: 32px; line-height: 36px;'> {$identifier} </h1>
		<p style='font-family: Helvetica, sans-serif; font-size: 18px; line-height: 22px;'>  
		Nome: {$name}
		<br/>
		Email: {$email}
		<br/>
		Telefone: {$phone}
		<br/>
		Modelo: {$model} 
        <br/> 
        Opção : {$opt}
		</p>
		</body>
		</html>");
	$mail->SetFrom('consorciovw@serv3xceler.com.br', 'Simulação de Orçamento - Consórcio VW');
	$mail->addCC( 'andre.facchini@3xceler.com.br', 'André Facchini');


	if ($mail->send()) {
		echo 'Mensagem enviada com sucesso!';
	}  else {
		echo 'Erro o enviar a mensagem' . $mail->ErrorInfo;
	}
}
else {
	echo 'Ops!';
}
die;