<div class="container">

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-bottom ">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
 <p><i class="blue-consorcio fa fa-angle-left"></i> <a href="../../planos.php">Retornar aos planos</a></p>
                <img class="img-carros" src="../../img/carros/up.jpg" />

                
                
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
                      
                       <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                        <a class="btn-orcamento2" rel="modal" onclick="javascript:location='#janela1';">

                            <img src="../../img/consorcio-volkswagen-fale-conosco.png" />

                            <span class="color-btn-meliga1"><p>Nos te ligamos</p></span>

                            <span class="color-btn-meliga2"><p>O Consórcio Volkswagen entra em contato com você, basta um click!</p></span>

                            <p class="p-btn-orcamento"><span class="color-btn-meliga3">Volks, me liga!</span></p>

                        </a>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                        <a class="btn-orcamento" href="#puxada-orcamento">

                            <!--<span class="color-btn-orcamento1"><p>Compre esse plano</p></span>-->

                            <span class="color-btn-orcamento2"><p>Parcelas de a partir de</p></span>

                            <span class="color-btn-orcamento3"><p><span>R$</span>623,91</p></span>
							
							<p class="p-btn-orcamento"><span class="color-btn-meliga4">comprar</span></p>

                        </a>

                    </div>

                   

                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                <h2><i class="blue-consorcio fa fa-angle-right"></i> Move UP - 2 Portas</h2>

                <p>O nome já diz tudo: com o novo up!, você terá muitos motivos para sorrir porque ele é diferente de tudo o que você já viu. Um carro democrático, com visual moderno e divertido, acessível a todos e, acima de tudo, um autêntico Volkswagen, com toda a segurança e robustez que só a marca pode oferecer. Nunca o DNA da marca foi reinterpretado de uma forma tão descontraída.</p>

                    <p>Com design clean e moderno, o up! esbanja simpatia, e o belo sorriso na parte frontal é só o ponto de partida para atrair o que a vida tem de melhor. E se a aparência chama a atenção, o interior não fica atrás: aqui, tudo foi projetado para o seu conforto com ergonomia.</p>

                    <p>A diversão é garantida por um conjunto de tecnologias que vão dar um up na sua rotina, trazendo a bordo o que existe de melhor em entretenimento. Além de tudo, o up! vem equipado com uma nova geração de motor que o torna o 1.0 mais leve, potente e econômico da categoria. É também o primeiro veículo produzido no Brasil a atingir 5 estrelas (nota máxima) em segurança para adultos e 4 estrelas para crianças pelo Latin NCAP, entidade especializada em segurança automotiva da América Latina.</p>

                    <p>Prepare-se para ficar com a energia em alta, ganhar a cidade e dar um up na sua vida. Afinal, atrás de um up!, sempre vêm outros ups. Divirta-se escolhendo o seu!</p>

        <!--        <p><i class="blue-consorcio fa fa-angle-left"></i> <a href="../../planos.php">Retornar aos planos</a></p>-->

              

            </div>

        </div>

        

        

        

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none"> 

            <!--<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                <h2><i class="blue-consorcio fa fa-angle-right"></i> Informações sobre o plano Mais Leve</h2>

            </div>-->

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                 <table class="table-consorcio table-single" cellspacing="0" cellpadding="0" border="0">
                <thead>
                    <tr>
                        <th>Grupo de Consórcio</th>
                        <th>Prazo</th>
                        <th>Série</th>
                        <th>Categoria</th>
                        <th>Tipo</th>
                        <th>Crédito</th>
                        <th>Parcelas Mensais</th>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <td>Leve</td>
                        <td>84 Meses</td>
                        <td>O</td>
                        <td>CARROS283</td>
                        <td>Pessoa Física</td>
                        <td>R$ 40.790,00</td>
                        <td>R$ 623,91</td>
                    </tr>
                    <tr>
                        <td>Normal</td>
                        <td>84 Meses</td>
                        <td>R</td>
                        <td>CARROS307</td>
                        <td>Pessoa Física</td>
                        <td>R$ 40.790,00</td>
                        <td>R$ 821,07</td>
                    </tr>

                </tbody>
            </table>
                
                <!--
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">PRAZO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>72 Meses</strong></p>

                    <p class="linha-azul-bottom">GRUPO DE CONSÓRCIO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>MAIS LEVE</strong></p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">CATEGORIA</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>CARROS262</strong></p>

                    <p class="linha-azul-bottom">CRÉDITO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>R$ 40.790,00</strong></p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">TIPO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>Pessoa Física</strong></p>

                    <p class="linha-azul-bottom">PARCELAS MENSAIS</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>R$ 478,69</strong></p>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none linha-azul-bottom-2x">

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                    <h3>Dados do Plano</h3>

                    <p>Adquira seu volkswagen com segurança e tranquilidade de quem tem mais de 30 anos de tradição no mercado.</p>

                    <p>Plano Gold:</p>

                    <p>Prazo de Duração: 72 meses</p>

                    <p>% Contribuição Mensal: Série U / Mais Leve - 0,9259%</p>

                    <p>% Taxa de Administração Total: Antecipada: - / Deferida: 17,5%</p>

                    <p>Plano Mais Leve (Série U): 432 Participantes</p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                    <h3>Novo Volkswagen UP!</h3>

                    <p>O nome já diz tudo: com o novo up!, você terá muitos motivos para sorrir porque ele é diferente de tudo o que você já viu. Um carro democrático, com visual moderno e divertido, acessível a todos e, acima de tudo, um autêntico Volkswagen, com toda a segurança e robustez que só a marca pode oferecer. Nunca o DNA da marca foi reinterpretado de uma forma tão descontraída.</p>

                    <p>Com design clean e moderno, o up! esbanja simpatia, e o belo sorriso na parte frontal é só o ponto de partida para atrair o que a vida tem de melhor. E se a aparência chama a atenção, o interior não fica atrás: aqui, tudo foi projetado para o seu conforto com ergonomia.</p>

                    <p>A diversão é garantida por um conjunto de tecnologias que vão dar um up na sua rotina, trazendo a bordo o que existe de melhor em entretenimento. Além de tudo, o up! vem equipado com uma nova geração de motor que o torna o 1.0 mais leve, potente e econômico da categoria. É também o primeiro veículo produzido no Brasil a atingir 5 estrelas (nota máxima) em segurança para adultos e 4 estrelas para crianças pelo Latin NCAP, entidade especializada em segurança automotiva da América Latina.</p>

                    <p>Prepare-se para ficar com a energia em alta, ganhar a cidade e dar um up na sua vida. Afinal, atrás de um up!, sempre vêm outros ups. Divirta-se escolhendo o seu!</p>

                </div>

            </div>-->

        </div>

        <!--

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none"> 

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                <h2><i class="blue-consorcio fa fa-angle-right"></i> Informações sobre o plano Leve</h2>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">PRAZO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>60 Meses</strong></p>

                    <p class="linha-azul-bottom">GRUPO DE CONSÓRCIO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>LEVE</strong></p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">CATEGORIA</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>CARROS283</strong></p>

                    <p class="linha-azul-bottom">CRÉDITO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>R$ 40.790,00</strong></p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">TIPO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>Pessoa Física</strong></p>

                    <p class="linha-azul-bottom">PARCELAS MENSAIS</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>R$ 623,91</strong></p>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none linha-azul-bottom-2x">

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                    <h3>Dados do Plano</h3>

                    <p>Adquira seu volkswagen com segurança e tranquilidade de quem tem mais de 30 anos de tradição no mercado.</p>

                    <p>Plano Gold:</p>

                    <p>Prazo de Duração: 60 meses</p>

                    <p>% Contribuição Mensal: Série O / Leve - 1,2500%</p>

                    <p>% Taxa de Administração Total: Antecipada: - / Deferida: 15,5%</p>

                    <p>Plano Leve (Série O): 240 Participantes</p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                    <h3>Novo Volkswagen UP!</h3>

                    <p>O nome já diz tudo: com o novo up!, você terá muitos motivos para sorrir porque ele é diferente de tudo o que você já viu. Um carro democrático, com visual moderno e divertido, acessível a todos e, acima de tudo, um autêntico Volkswagen, com toda a segurança e robustez que só a marca pode oferecer. Nunca o DNA da marca foi reinterpretado de uma forma tão descontraída.</p>

                    <p>Com design clean e moderno, o up! esbanja simpatia, e o belo sorriso na parte frontal é só o ponto de partida para atrair o que a vida tem de melhor. E se a aparência chama a atenção, o interior não fica atrás: aqui, tudo foi projetado para o seu conforto com ergonomia.</p>

                    <p>A diversão é garantida por um conjunto de tecnologias que vão dar um up na sua rotina, trazendo a bordo o que existe de melhor em entretenimento. Além de tudo, o up! vem equipado com uma nova geração de motor que o torna o 1.0 mais leve, potente e econômico da categoria. É também o primeiro veículo produzido no Brasil a atingir 5 estrelas (nota máxima) em segurança para adultos e 4 estrelas para crianças pelo Latin NCAP, entidade especializada em segurança automotiva da América Latina.</p>

                    <p>Prepare-se para ficar com a energia em alta, ganhar a cidade e dar um up na sua vida. Afinal, atrás de um up!, sempre vêm outros ups. Divirta-se escolhendo o seu!</p>

                </div>

            </div>

        </div>

        

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none"> 

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                <h2><i class="blue-consorcio fa fa-angle-right"></i> Informações sobre o plano Normal</h2>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">PRAZO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>60 Meses</strong></p>

                    <p class="linha-azul-bottom">GRUPO DE CONSÓRCIO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>NORMAL</strong></p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">CATEGORIA</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>CARROS307</strong></p>

                    <p class="linha-azul-bottom">CRÉDITO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>R$ 40.790,00</strong></p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-none">

                    <p class="linha-azul-bottom">TIPO</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>Pessoa Física</strong></p>

                    <p class="linha-azul-bottom">PARCELAS MENSAIS</p>

                    <p class="linha-azul-bottom-2x"><i class="blue-consorcio fa fa-angle-right"></i><strong>R$ 821,07</strong></p>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none linha-azul-bottom-2x">

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                    <h3>Dados do Plano</h3>

                    <p>Adquira seu volkswagen com segurança e tranquilidade de quem tem mais de 30 anos de tradição no mercado.</p>

                    <p>Plano Gold:</p>

                    <p>Prazo de Duração: 60 meses</p>

                    <p>% Contribuição Mensal: Série M / Normal - 1,6667%</p>

                    <p>% Taxa de Administração Total: Antecipada: - / Deferida: 15,5%</p>

                    <p>Plano Normal (Série M): 120 Participantes</p>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                    <h3>Novo Volkswagen UP!</h3>

                    <p>O nome já diz tudo: com o novo up!, você terá muitos motivos para sorrir porque ele é diferente de tudo o que você já viu. Um carro democrático, com visual moderno e divertido, acessível a todos e, acima de tudo, um autêntico Volkswagen, com toda a segurança e robustez que só a marca pode oferecer. Nunca o DNA da marca foi reinterpretado de uma forma tão descontraída.</p>

                    <p>Com design clean e moderno, o up! esbanja simpatia, e o belo sorriso na parte frontal é só o ponto de partida para atrair o que a vida tem de melhor. E se a aparência chama a atenção, o interior não fica atrás: aqui, tudo foi projetado para o seu conforto com ergonomia.</p>

                    <p>A diversão é garantida por um conjunto de tecnologias que vão dar um up na sua rotina, trazendo a bordo o que existe de melhor em entretenimento. Além de tudo, o up! vem equipado com uma nova geração de motor que o torna o 1.0 mais leve, potente e econômico da categoria. É também o primeiro veículo produzido no Brasil a atingir 5 estrelas (nota máxima) em segurança para adultos e 4 estrelas para crianças pelo Latin NCAP, entidade especializada em segurança automotiva da América Latina.</p>

                    <p>Prepare-se para ficar com a energia em alta, ganhar a cidade e dar um up na sua vida. Afinal, atrás de um up!, sempre vêm outros ups. Divirta-se escolhendo o seu!</p>

                </div>

            </div>

        </div>-->

    </div>

    

</div>