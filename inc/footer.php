<div class="container rodape-all">
    <div itemscope itemtype="http://schema.org/Organization" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress" class="col-xs-12 col-sm-12 col-md-6 col-lg-6 rodape-new">
            <p>
                <b>Horário de Funcionamento</b><br/>
                Segunda à Sexta-Feira das 08:00 às 18:00<br/>
                Sábados (Plantão) das 09:00 às 16:00
            </p>
            
            <br/>
            <p>
                <span itemprop="name">
                    <b>Consórcio Nacional Volkswagen</b>
                </span>
            </p>
            <p>
               <!--  <span itemprop="streetAddress">Rua Borges de Figueiredo, 303 Ed. Atrio Giorno Conj 216</span><br/>
                <span itemprop="addressLocality">Móoca - São Paulo </span> / <span itemprop="addressRegion">SP</span><br/>
                <span itemprop="postalCode">CEP: 03110-010</span><br/> -->
                <span>Click Consórcio de Autos e Imóveis LTDA</span><br/>
                <span>CNPJ: 07.456.203/0001-36</span><br/>
            </p>
            <br/>
            <p>
                <a target="_blank" href="http://www.3xceler.com.br">
                    Créditos - 3xceler
                </a>
            </p>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 rodape-new">
            <p>
                <b>Central de Atendimento</b><br/>
                <i class="fa fa-envelope-o"></i> <span itemprop="email">info@consorciovolkswagen.com.br </span><br/>
                <i class="fa fa-phone"></i> <span itemprop="telephone">+55 (11) 2114-0010</span><br/>
                <i class="fa fa-phone"></i> <span itemprop="telephone">+55 (11) 93800-9002</span>
            </p>
            <p>
                <b>Banco Volkswagen</b><br/>
                Central de Relacionamento:<br/>
                <i class="fa fa-phone"></i> <span itemprop="telephone">0800 770 1936</span>
            </p>
            <p>SAC:<br/>
                <i class="fa fa-phone"></i> <span itemprop="telephone">0800 770 1926</span>
            </p>
            <p>Deficiente auditivo/fala:<br/>
                <i class="fa fa-phone"></i> <span itemprop="telephone">0800 770 1935</span>
            </p>
            <p>Ouvidoria:<br/>
                <i class="fa fa-phone"></i> <span itemprop="telephone">0800 701 2834</span>
            </p>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <!-- <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4e57b56267e875f5" async="async"></script>                        <div class="addthis_sharing_toolbox"></div> -->
            </div>
        </div>
    </div>
</div>