<form action="" class="submit-main-cta form_01 cleared" data-modal="simular">
    <label class="field-wrapper half">
    <input type="hidden" name="identifier" value="Adquira seu UP">
        <input required type="text" class="field_01" name="name" placeholder="Nome Completo">
        <input type="hidden" name="bulldesk-client" value="">
    </label>
    <label class="field-wrapper half">
        <input required type="email" class="field_01" name="email" placeholder="E-mail">
    </label>
    <label class="field-wrapper half">
        <input required type="text" class="field_01" name="cellphone" placeholder="(DDD) Celular">
    </label>
    <label class="field-wrapper half">
        <input required type="text" class="field_01" name="phone" placeholder="(DDD) Telefone">
    </label>
    <label class="field-wrapper half">
        <select class="field_01" name="user-select">
            <option disabled selected> Selecione um Modelo </option>
            <option value="Take UP! 4 Portas">Take UP! 4 Portas</option>
            <option value="Move UP! 4 Portas">Move UP! 4 Portas</option>
            <option value="Move UP! TSI">Move UP! TSI</option>
            <option value="High UP! TSI">High UP! TSI</option>
            <option value="Cross UP! TSI">Cross UP! TSI</option>
     </select>
    </label>
    <label class="field-wrapper half">
        <input type="number" class="field_01" name="user-credit" placeholder="Crédito desejado">
    </label>
    <button class="submit_01_form">ENVIAR</button>
</form>