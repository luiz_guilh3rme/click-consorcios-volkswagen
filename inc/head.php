<head>
    <meta charset="UTF-8">
    <title><?php echo $title; ?></title>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <?php echo $description; ?>
    <?php echo $canonicalTag; ?>
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(). '/favicon/favicon.ico'; ?>">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(). '/css/slick.min.css'; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(). '/css/slick-theme.min.css'; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(). '/css/style.css'; ?> ">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(). '/css/style2.min.css'; ?> ">
</head>