<?php 
setlocale(LC_MONETARY,"pt_BR", "ptb");
require_once 'compress-html.php';
?>
<!DOCTYPE html>
    <html lang="pt-BR">

    <?php

        //$teste_include = include './inc/orcamento.php';

		
		 $title = utf8_encode($rowSel["auto_seo_title"]);
		 
        $description = '<meta itemprop="description" name="description" content="'.utf8_encode($rowSel["auto_seo_description"]).'">';
		
        $canonicalTag = '<link rel="canonical" href="'.$rowSel["auto_seo_cannonical"].'">';
		
		
     //   $title = "Consórcio Take UP! - 2 Portas | Consórcio Volkswagen";

      //  $description = '<meta itemprop="description" name="description" content="Consórcio Take UP! 2 portas com até 72 meses para pagar e planos sem juros, sem entrada ou taxa de adesão. Visual moderno e divertido, com preço acessível!">';

      //  $canonicalTag = '<link rel="canonical" href="http://www.consorciovolkswagen.com.br/take-up-2-portas.php">';

        $h1_da_pag =  str_replace(" | Consórcio Volkswagen","",$title);

        $texto_after = 'Agora falta pouco para você fazer parte de um seleto grupo que visa economia e tranquilidade';

        $img_header = '../../img/header/interior-do-volkswagen.png';

        $alt = 'Interior do Volkswagen - Consórcio Take UP! - 2 Portas';

        $title_alt = 'Consórcio Take UP! - 2 Portas';

        require_once 'inc/head.php';

    ?>



    <body>

        <?php

            require_once 'header.php';

            require_once 'header-planos.php';
			
			?>
			
			
			<div class="container">

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-bottom ">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
<p><i class="blue-consorcio fa fa-angle-left"></i> <a href="../../planos.php">Retornar aos planos</a></p>
                <img class="img-carros" src="../../img/carros/<?php echo (!empty($rowSel['auto_imagem']) ? $rowSel['auto_imagem'] : $rowSel["auto_modelo_imagem"]); ?>" title="<?=$rowSel["auto_imagem_title"] ?>"  alt="<?=$rowSel["auto_imagem_alt"] ?>"/>


                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">


                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                        <a class="btn-orcamento2" rel="modal" onclick="javascript:location = '#janela1';">

                            <img src="../../img/consorcio-volkswagen-fale-conosco.png" />

                            <span class="color-btn-meliga1"><p>Nos te ligamos</p></span>

                            <span class="color-btn-meliga2"><p>O Consórcio Volkswagen entra em contato com você, basta um click!</p></span>

                            <p class="p-btn-orcamento"><span class="color-btn-meliga3">Volks, me liga!</span></p>

                        </a>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                        <a class="btn-orcamento" href="#puxada-orcamento">

                            <!--<span class="color-btn-orcamento1"><p>Compre esse plano</p></span>-->

                            <span class="color-btn-orcamento2"><p>Parcelas de a partir de</p></span>

                            <span class="color-btn-orcamento3"><p><?php echo ($rowSel['auto_parcela_leve'] == 0 ? money_format('%n', $rowSel['auto_parcela_gold']) : money_format('%n', $rowSel["auto_parcela_leve"])); ?></p></span>

							<p class="p-btn-orcamento"><span class="color-btn-meliga4">comprar</span></p>
                        </a>

                    </div>



                </div>


            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                <h2><i class="blue-consorcio fa fa-angle-right"></i><?=str_replace("Consórcio ","",$h1_da_pag); ?></h2>

             <p> <?=utf8_encode($rowSel['auto_descricao']); ?></p>
            </div>

        </div>


        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none"> 

	
 

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none">
<table class="table-consorcio table-single" cellspacing="0" cellpadding="0" border="0">
                <thead>
                    <tr>
                        <th>Tipo de Plano</th>
						<th>Série</th>
                        <th>Prazo</th>
                        <th>Categoria</th>
                        <th>Tipo</th>
                        <th>Crédito</th>
                        <th>Parcelas Mensais</th>
                        <th>Contribuição Mensal</th>
                        <th>Taxa de Administração</th>
                        <th>Participantes</th>
                    </tr>
                </thead>

                <tbody>
				
				
				 			<?php
		if($rowSel['auto_parcela_leve'] != 0){
			
			$sqlNormal = "SELECT * FROM volks_plano WHERE plano_plano_sigla  = 'L'";
			$resultNormal = mysql_query($sqlNormal);
			$rowNormal = mysql_fetch_assoc($resultNormal);
			?>
                    <tr>
                        <td>Leve</td>
						 <td><?=$rowNormal["plano_serie"] ?></td>
                        <td><?=$rowNormal["plano_prazo"] ?></td>
                        <td><?=$rowSel['auto_cat_normal']; ?></td>
                        <td><?=utf8_encode($rowSel['auto_tipo']); ?></td>
                        <td><?=money_format('%n', $rowSel['auto_credit']); ?></td>
                          <td><?=money_format('%n', $rowSel['auto_parcela_leve']); ?></td>
						  <td><?=$rowNormal["plano_contrib_mensal"] ?>%</td>
                        <td><?=$rowNormal["plano_taxaadm"] ?>%</td>
                        <td><?=$rowNormal["plano_participantes"] ?></td>
                    </tr>
		<?php
		}
		?>
             
			 			<?php
		if($rowSel['auto_parcela_normal'] != 0){
			
			$sqlNormal = "SELECT * FROM volks_plano WHERE plano_plano_sigla  = 'N'";
			$resultNormal = mysql_query($sqlNormal);
			$rowNormal = mysql_fetch_assoc($resultNormal);
			?>
                    <tr>
                        <td>Normal</td>
						 <td><?=$rowNormal["plano_serie"] ?></td>
                        <td><?=$rowNormal["plano_prazo"] ?></td>
                        <td><?=$rowSel['auto_cat_normal']; ?></td>
                        <td><?=utf8_encode($rowSel['auto_tipo']); ?></td>
                        <td><?=money_format('%n', $rowSel['auto_credit']); ?></td>
                          <td><?=money_format('%n', $rowSel['auto_parcela_normal']); ?></td>
						  <td><?=$rowNormal["plano_contrib_mensal"] ?>%</td>
                        <td><?=$rowNormal["plano_taxaadm"] ?>%</td>
                        <td><?=$rowNormal["plano_participantes"] ?></td>
                    </tr>
		<?php
		}
		?>
		
		
				 	 			<?php
		if($rowSel['auto_parcela_gold'] != 0){
			
			$sqlNormal = "SELECT * FROM volks_plano WHERE plano_plano_sigla  = 'G'";
			$resultNormal = mysql_query($sqlNormal);
			$rowNormal = mysql_fetch_assoc($resultNormal);
			?>
                    <tr>
                        <td>Gold</td>
						 <td><?=$rowNormal["plano_serie"] ?></td>
                        <td><?=$rowNormal["plano_prazo"] ?></td>
                        <td><?=$rowSel['auto_cat_normal']; ?></td>
                        <td><?=utf8_encode($rowSel['auto_tipo']); ?></td>
                        <td><?=money_format('%n', $rowSel['auto_credit']); ?></td>
                          <td><?=money_format('%n', $rowSel['auto_parcela_gold']); ?></td>
						  <td><?=$rowNormal["plano_contrib_mensal"] ?>%</td>
                        <td><?=$rowNormal["plano_taxaadm"] ?>%</td>
                        <td><?=$rowNormal["plano_participantes"] ?></td>
                    </tr>
		<?php
		}
		?>
                </tbody>
            </table>

            </div>


        </div>



    </div>



</div>
			
			<?php

          //  require_once 'inc/carros/content-take-up-2.php';

            require_once 'orcamento.php';

            require_once 'home/servicos.php';

            require_once 'footer.php';

            require_once 'script.php';

        ?>
    </body>
</html>