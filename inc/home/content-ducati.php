<div class="background-ducati">

    <div class="container">

        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 texto-ducati">

            <h2><i class="fa fa-angle-right"></i> Experiência da Marca</h2>

            <p>Somos os únicos a contar com a garantia da marca Volkswagen e da marca Ducati.</p>
            <p>Mais de 35 anos de tradição e 500 mil veículos entregues! O Consórcio Nacional Volkswagen é uma das maiores administradoras de consórcios do país e líder de mercado entre as administradoras ligadas a montadoras em número de clientes ativos, segundo o Banco Central do Brasil.</p>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 texto-ducati">

            <h2><i class="fa fa-angle-right"></i> Como funciona o Consórcio</h2>

            <p>O Consórcio é uma forma de se adquirir um bem, fazer um investimento ou de proporcionar a você a programação da compra do sonho de consumo, com menor gasto, se comparado à maioria dos financiamentos.</p>

            <p>É similar a um autofinanciamento, em que pessoas com o objetivo de adquirir um bem similar se juntam em um grupo e realizam pagamentos mensais para geração de fundo para compra do bem. <a href="sobre.php">Saiba mais</a></p>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 img-ducati">

            <p>

                <img border="0" src="img/logo-ducati-motor-holding.png" alt="Logo Ducati Motor Holding" width="180" height="180">

            </p>

        </div>

    </div>

</div>