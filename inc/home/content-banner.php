<div id="myCarousel" class="carousel slide" data-ride="carousel">

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">

        <div class="item active">

            <a href="/consorcio/take-up-2-portas">
                <img src="../../img/banners/banner1.jpg" alt="Consórcio Volkswagen Novo UP!">
            </a>

        </div>

        <div class="item">

            <a href="/consorcio/novo-fox-10-trendline">
                <img src="../../img/banners/banner2.jpg" alt="Consórcio Volkswagen Novo Fox!">
            </a>

        </div>

        <div class="item">
            <a href="/consorcio/gol-16">
                <img src="../../img/banners/banner3.jpg" alt="Consórcio Volkswagen Novo Gol" >
            </a>
        </div>

        <div class="item">
            <a href="/consorcio/saveiro-16-cs">
                <img src="../../img/banners/banner4.jpg" alt="Consórcio Volkswagen Saveiro" >
            </a>
        </div>

        <div class="item">
            <a href="/consorcio/golf-comfortline-16">
                <img src="../../img/banners/banner5.jpg" alt="Consórcio Volkswagen  Novo Golf" >
            </a>
        </div>



        <div class="item">
            <a href="/consorcio/voyage-10">
                <img src="../../img/banners/banner6.jpg" alt="Consórcio Volkswagen Voyage">
            </a>

        </div>

       

    </div>



    <!-- Left and right controls -->

    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">

        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>

        <span class="sr-only">Previous</span>

    </a>

    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">

        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>

        <span class="sr-only">Next</span>

    </a>

</div>