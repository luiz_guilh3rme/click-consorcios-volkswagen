<div class="backgroung-image-home">

    <div class="container">

        <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">

            <a href="<?php echo home_url(). '/sobre'; ?>">

                <img border="0" src="<?php echo $dir. '/img/icone-lider-consorcio-nacional.png'; ?>" width="70" height="70" class="imgfoto" alt="Ícone Líder - Consórcio Nacional Volkswagen" title="Conheça o Consórcio Nacional Volkswagen">

            </a>

            <p class="conteudotab3item"><a href="<?php echo home_url(). '/sobre'; ?>">Consórcio Volkswagen</a></p>

            <p class="conteudotab3item-sub"><a href="<?php echo home_url(). '/sobre'; ?>">Garantia da Fábrica</a></p>

        </div>

        <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">

            <a href="<?php echo home_url(). '/volkswagen-do-brasil'; ?>">

                <img border="0" src="<?php echo $dir. '/img/icone-engrenagem-volkswagen-do-brasil.png'; ?>" width="70" height="70" class="imgfoto" onlclick="limpadiv();" alt="Ícone Líder - Volkswagen do Brasil" title="Conheça a Volkswagen do Brasil - Tradição e Confiança">

            </a>

            <p class="conteudotab3item"><a href="<?php echo home_url(). '/volkswagen-do-brasil'; ?>">Volkswagen do Brasil</a></p>

            <p class="conteudotab3item-sub"><a href="<?php echo home_url(). '/volkswagen-do-brasil'; ?>">Tradição e Confiança</a></p>

        </div>

        <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">

            <a href="<?php echo home_url(). '/faq'; ?>">

                <img border="0" src="<?php echo $dir. '/img/icone-nuvem-perguntas-frequentes.png'; ?>" width="70" height="70" class="imgfoto" alt="Ícone Nuvem - Perguntas Frequentes" title="Confira as Perguntas Frequentes">

            </a>

            <p class="conteudotab3item"><a href="<?php echo home_url(). '/faq'; ?>">Perguntas Frequentes</a></p>

            <p class="conteudotab3item-sub"><a href="<?php echo home_url(). '/faq'; ?>">Consulte nosso FAQ</a></p>

        </div>

        <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">

            <a href="<?php echo home_url(). '/fale-conosco'; ?>">

                <img border="0" src="<?php echo $dir. '/img/icone-alvo-fale-conosco.png'; ?>" width="70" height="70" class="imgfoto" alt="Ícone Alvo - Fale Conosco" title="Fale Conosco e Tire Suas Dúvidas">

            </a>

            <p class="conteudotab3item"><a href="<?php echo home_url(). '/fale-conosco'; ?>">Fale<br>Conosco</a></p>

            <p class="conteudotab3item-sub"><a href="<?php echo home_url(). '/fale-conosco'; ?>">Tire Suas Dúvidas</a></p>

        </div>

        <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">

            <a target="_blank" href="http://www.bancovw.com.br/br/home.html"> 

                <img border="0" src="<?php echo $dir. '/img/icone-sifrao-banco-volkswagen.png'; ?>" width="70" height="70" class="imgfoto" alt="Ícone Sifrão - Banco Volkswagen" title="Acesso ao Site do Banco Volkswagen">

            </a>

            <p class="conteudotab3item"><a target="_blank" href="http://www.bancovw.com.br/br/home.html">Banco<br>Volkswagen</a></p>

            <p class="conteudotab3item-sub"><a target="_blank" href="http://www.bancovw.com.br/br/home.html">Leia as Condições</a></p>

        </div>

        <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">

            <a target="_blank" href="http://atendimento-eletronico.bancovw.com.br/logon/"> 

                <img src="<?php echo $dir. '/img/icone-cadeado-atendimento-eletronico.png'; ?>" width="70" height="70" border="0" class="imgfoto" alt="Ícone Cadeado - Atendimento Eletrônico" title="Visite o Atendimento Eletrônico do Banco Volkswagen">

            </a>

            <p class="conteudotab3item"><a target="_blank" href="http://atendimento-eletronico.bancovw.com.br/logon/">Atendimento  Eletrônico</a></p>

            <p class="conteudotab3item-sub"><a target="_blank" href="http://atendimento-eletronico.bancovw.com.br/logon/">Banco Volkswagen</a></p>

        </div>



    </div>

</div>