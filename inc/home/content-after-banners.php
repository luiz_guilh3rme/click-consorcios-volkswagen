<div class="backgroung-azul-after-banners">

    <div class="container">

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

            <h1>Consórcio Nacional Volkswagen - CNVW</h1>

            <h2>Conquiste seu carro através do Consórcio Volkswagen com garantia da fábrica, planos sem juros, sem entrada ou taxa de adesão, você tem até 84 meses para pagar. Todos estes benefícios vem junto com a garantia, confiança e tradição de uma das maiores montadoras de veículos do mundo, a Volkswagen.</h2>
            <a href="planos.php">Conheça nossos planos!</a>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 img-after-banners">

                <img border="0" src="img/consorcio-volkswagen.png" alt="Ícone Volkswagen" width="50" height="50">

                <p><a href="sobre.php" title="Conheça mais sobre a Volkswagen">Conheça mais sobre a Volkswagen</a></p>

            </div>

            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 img-after-banners">

                <img border="0" src="img/consorcio-volkswagen-fale-conosco.png" alt="Ícone Fale Conosco" width="50" height="50">

                <p><a href="fale-conosco.php" title="Converse com um consultor CNVW">Converse com um consultor CNVW</a></p>

            </div>

            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 img-after-banners">

                <img border="0" src="img/consorcio-volkswagen-parceria.png" alt="Ícone Parceria" width="50" height="50">

                <p><a href="parceria.php" title="Torne-se um parceiro CNVW">Torne-se um parceiro CNVW</a></p>

            </div>

        </div>

    </div>

</div>