<!DOCTYPE html>
<html lang="pt-br">
<?php include 'includes/components/head.php' ?>
<?php wp_head(); ?>

   <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KWCH849" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <header>
        <div class="desktop telefoneContato">
            <img src="<?php echo get_template_directory_uri().'/images/phone.png'; ?>" class="img-phone" alt="telefone de contato com o consórcio volks">
             <a class="phone-head" href="tel:551121140010" target="_blank">
                <p class="phone-header">11 2114.0010</p>
             </a>
            <a class="cellphone-head" href="wpp">
                <p class="phone-header">11 96800.9002</p>
            </a>
        </div>
        <a href="<?php echo home_url(); ?>" class="barraBrancaelogo">
            <div class="desktop">
                <img class="primeiratlogoTopoBarraBranca" src="<?php echo get_template_directory_uri(). '/images/topoNacionalConsorcioVolkswagen5.png'; ?>" alt="Logotipos Porto Seguro e Super - Corretora de Seguros"
                    title="Porto Seguro e  Super Corretora">
            </div>
            <div class="mobile">
                <div id="Topo1">
                    <img src="<?php echo get_template_directory_uri(). '/images/logo_p1Topo.png'; ?>" alt="Logotipos Porto Seguro e Super - Corretora de Seguros" title="Porto Seguro e  Super Corretora">
                    <!---<img src="images/logo.png"  alt="Logotipos Porto Seguro e Super - Corretora de Seguros" title="Porto Seguro e  Super Corretora">--->
                </div>
                <div id="Topo2">
                    <img src="<?php echo get_template_directory_uri(). '/images/logo_p2Topo.png' ?>" alt="Logotipos Porto Seguro e Super - Corretora de Seguros" title="Porto Seguro e  Super Corretora">
                    <!---<img src="images/logo.png"  alt="Logotipos Porto Seguro e Super - Corretora de Seguros" title="Porto Seguro e  Super Corretora">--->
                </div>
            </div>
        </a>
    </header>
</head>
