<?php require_once './inc/compress-html.php'; ?>
<!DOCTYPE html>
    <html lang="pt-BR">

    <?php
        $title = "Perguntas Frequentes (FAQ) | Consórcio Nacional Volkswagen";
        $description = '<meta itemprop="description" name="description" content="Conheça as principais perguntas abordadas por participantes do Consórcio Volkswagen durante o processo de adesão. Tire suas dúvidas, Confira!">';
        $canonicalTag = '<link rel="canonical" href="'. get_home_url() .'/faq';">';
        $h1_da_pag = 'Perguntas Frequentes (FAQ)';
        $texto_after = 'Esclareça suas dúvidas através de nosso FAQ, conheça as questões mais frequentes e suas respostas.';
        $img_header = '/img/header/faq-consorcio-volkswagen.png';
        $alt = 'Banner Homem ao Telefone - Perguntas Frequentes (FAQ)';
        $title_alt = 'Perguntas Frequentes (FAQ)';
        require_once 'inc/head.php';
    ?>


    <body>

        <?php

            require_once 'inc/header.php';

            require_once 'inc/header-interna.php';

            require_once 'inc/consorcio/content-faq.php';

            require_once 'inc/home/servicos.php';

            require_once 'inc/footer.php';

            require_once 'inc/script.php';

        ?>

    </body>

</html>