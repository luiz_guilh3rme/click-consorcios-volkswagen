<?php include 'header.php'; ?>
<main class="landing">
    <section class="opening-contain-obrigado">
        <p class="thanks">Obrigado pelo seu contato!
            <br>
            Clique
            <a >AQUI</a>
            para retornar à página principal.
        </p>
    </section>
</main>
<?php include 'footer.php'; ?>
