<?php require_once '/inc/compress-html.php'; ?>
<!DOCTYPE html>
    <html lang="pt-BR">

    <?php
        $title = "Consórcio Nacional Volkswagen | Representante Autorizado";
        $description = '<meta itemprop="description" name="description" content="Consórcio Nacional Volkswagen - Conheça o maior e mais tradicional consórcio do País. Muito mais tranquilidade e segurança para você. Aproveite!">';
        $canonicalTag = '<link rel="canonical" href="http://www.consorciovolkswagen.com.br/sobre.php">';
        $h1_da_pag = 'Consórcio Nacional Volkswagen';
        $texto_after = 'Conheça o maior e mais tradicional consórcio do País';
        $img_header = $dir.'/img/header/frente-amarok-consorcio-volkswagen.png';
        $alt = 'Banner Amarok - Consórcio Nacional Volkswagen';
        $title_alt = 'Consórcio Nacional Volkswagen';
        require_once 'inc/head.php';
    ?>

    <body>
        <?php
            require_once 'inc/header.php';
            require_once 'inc/header-interna.php';
            require_once 'inc/consorcio/content-consorcio.php';
            require_once 'inc/home/servicos.php';
            require_once 'inc/footer.php';
            require_once 'inc/script.php';
            require_once 'inc/bulldesk.php';
        ?>
    </body>
</html>