<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KWCH849');</script>
<!-- End Google Tag Manager -->
    <meta charset="UTF-8">
    <title>
       Consórcio Volkswagen | Consórcio Nacional Volkswagen
    </title>
    <meta name="description" content="Consórcio Volkswagen - Compre seu Carro Volkswagen sem Financiamento, Juros e Burocracia. Escolha seu Veículo e Solicite uma Simulação.">
    <meta name="language" content="pt-br">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no">
    <meta name="author" content="Agência 3xceler">
    <meta name="language" content="pt-br" />
    
  
    <link rel="canonical" href="<?= $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] ?>" />
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(). '/favicon/favicon.ico'; ?>" />
       <!-- <meta name="theme-color" content="#FFFFFFF"> -->
 <!--<link rel="manifest" href="/favicon/site.webmanifest">-->
<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#18527a">
<meta name="msapplication-TileColor" content="#18527a">
<meta name="theme-color" content="#18527a">
	<script type="text/javascript">
    window.smartlook||(function(d) {
    var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
    var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
    c.charset='utf-8';c.src='https://rec.smartlook.com/recorder.js';h.appendChild(c);
    })(document);
    smartlook('init', '8391a08f310f81c590eb577164c076d9e8a8b1d5');
</script>
