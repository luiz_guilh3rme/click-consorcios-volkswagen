<div class="overlay">
    <button class="close-modal" aria-label="Fechar Modal">
        &times;
    </button>
    <div class="form-wrapper-all">
        <div class="form-picker">
            <button class="form-pickers" data-instance="00">
                <i class="fa fa-phone"></i>
                ME LIGUE AGORA
            </button>
            <button class="form-pickers" data-instance="01">
                <i class="fa fa-clock alt"></i>
                ME LIGUE DEPOIS
            </button>
            <button class="form-pickers active" data-instance="02">
                <i class="fa fa-comments alt"></i>
                DEIXE UMA MENSAGEM
            </button>
        </div>
        <div class="instance" data-instance="01">
            <form class="leave-message schedule-time" data-form="schedule">
                <legend class="leave-title">
                    Gostaria de agendar e receber uma
                    chamada em outro horário?
                </legend>
                <div class="fields cleared">
                    <input type="hidden" name="identifier" value="Agendamento de Chamada com Horário">
                    <input type="hidden" name="bulldesk-client" value="">
                    <label class="field-wrapper half">
                        <select name="dia" class="field-green"></select>
                    </label>
                    <label class="field-wrapper half">
                        <select name="horario" class="field-green"></select>
                    </label>
                    <label class="field-wrapper full">
                        <div class="icon"><i class="fa fa-phone"></i></div>
                        <input class="field-green has-icon" type="tel" placeholder="Informe seu Telefone" name="telefone">
                    </label>
                    <button class="call-me-later">
                        ME LIGUE DEPOIS
                    </button>
                    <p class="callers">Você já é a <span class="number">5</span> pessoa a solicitar uma ligação.</p>
                </div>
            </form>
        </div>
        <div class="instance active" data-instance="02">
            <form class="leave-message" data-form="leave-message">
                <legend class="leave-title">
                    Deixe sua mensagem! Entraremos em
                    contato o mais rápido possível.
                </legend>
                <div class="fields cleared">
                    <input type="hidden" name="identifier" value="Deixe sua Mensagem">
                    <input type="hidden" name="bulldesk-client" value="">
                    <label class="field-wrapper full">
                        <textarea class="field-green bigger" placeholder="Deixe sua Mensagem" name="mensagem"></textarea>
                    </label>
                    <label class="field-wrapper full">
                        <div class="icon"><i class="fa fa-phone"></i></div>
                        <input class="field-green has-icon" name="telefone" placeholder="Informe seu Telefone" type="text">
                    </label>
                    <label class="field-wrapper full">
                    <div class="icon"><i class="fa fa-envelope"></i></div>
                        <input class="field-green has-icon" type="text" name="email" placeholder="Informe seu E-mail">
                    </label>
                    <button class="call-me-later">
                        ME LIGUE DEPOIS
                    </button>
                    <p class="callers">Você já é a <span class="number">3</span> pessoa a deixar uma mensagem.</p>
                </div>
            </form>
        </div>
        <div class="instance" data-instance="00">
            <form class="leave-message" data-form="we-call">
                <legend class="leave-title">
                    <span class="variant">
                        NÓS TE LIGAMOS!
                    </span>
                    Informe seu telefone que entraremos em
                    contato o mais rápido possível.
                </legend>
                <div class="fields cleared">
                    <input type="hidden" name="identifier" value="Nós te Ligamos">
                    <input type="hidden" name="bulldesk-client" value="">
                    <label class="field-wrapper full">
                        <div class="icon"><i class="fa fa-phone"></i></div>
                        <input class="field-green has-icon" type="tel" placeholder="Informe seu Telefone" name="telefone">
                    </label>
                    <button class="call-me-later">
                        ME LIGUE AGORA
                    </button>
                    <p class="callers">Você já é a <span class="number">5</span> pessoa a solicitar uma ligação.</p>
                </div>
            </form>
        </div>
    </div>
</div>