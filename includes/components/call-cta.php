<div class="call-cta-wrapper">
    <div class="tooltip">
        <p class="tooltip-text">
            Vamos conversar sobre consórcio?</p>
        <button class="confirm tracked" data-category="CTA - Telefone" data-action="Click" data-label="Abriu CTA de Telefone">
            SIM
        </button>
    </div>
    <img src="<?php echo get_template_directory_uri(). '/images/callcta.png'; ?>" aria-hidden="true" class="call-cta">
</div>